package cz.benchmarktools.facedetection;

import cz.benchmarktools.facedetection.exception.TaskFailedException;
import cz.benchmarktools.facedetection.task.FaceDetectionTask;
import cz.sandor.dipl.benchmark.BenchmarkResult;
import cz.sandor.dipl.benchmark.IBenchmarkRunFlowController;
import cz.sandor.dipl.benchmark.MeasurableBenchmark;
import org.pmw.tinylog.Logger;

/**
 * Implements the interface and thus enables the measuring framework to run the face detection benchmark.
 */
public class Benchmark implements MeasurableBenchmark {
    private IBenchmarkRunFlowController runFlowController;
    private FaceDetectionTask task;

    /**
     * Initializes the benchmark.
     *
     * @param args The face detection algorithm's arguments.
     * @return The result of the initialization. Must be one of {@link BenchmarkResult#INIT_FINISHED}
     * {@link BenchmarkResult#INIT_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult init(String[] args) {
        if (args.length == 2) {
            task = new FaceDetectionTask(args[0], args[1]);
        } else {
            return BenchmarkResult.INIT_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.INIT_FINISHED;
        }
    }

    /**
     * Iteratively performs the task until it is enabled. Reports the measuring framework when the task started and
     * finished.
     *
     * @return The result of the benchmark. Must be one of {@link BenchmarkResult#RUN_FINISHED},
     * {@link BenchmarkResult#RUN_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult iterateAndMeasure() {
        try {
            while (runFlowController.canRunAgain()) {
                runFlowController.iterationStarted();

                task.detectFaces();

                runFlowController.iterationStopped();
                runFlowController.iterationCompleted();
            }
        } catch (TaskFailedException e) {
            if (runFlowController.isIterationRunning()) {
                runFlowController.iterationFailed();
            }
            return BenchmarkResult.RUN_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.RUN_FINISHED;
        }
    }

    /**
     * Cleans up the working environment created in the {@link #init(String[])} and the temporary files created in
     * {@link #iterateAndMeasure()}.
     *
     * @return The result of the cleanup. Must be one of {@link BenchmarkResult#CLEAN_UP_FINISHED},
     * {@link BenchmarkResult#CLEAN_UP_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult cleanup() {
        try {
            task.cleanup();
        } catch (Exception e) {
            Logger.error(e, "Cleanup failed.");
            return BenchmarkResult.CLEAN_UP_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.CLEAN_UP_FINISHED;
        }
    }

    /**
     * Initializes the run flow controller.
     *
     * @param benchmarkRunFlowController The measuring framework's run flow controller.
     */
    @Override
    public void setBenchmarkRunFlowController(IBenchmarkRunFlowController benchmarkRunFlowController) {
        this.runFlowController = benchmarkRunFlowController;
    }
}
