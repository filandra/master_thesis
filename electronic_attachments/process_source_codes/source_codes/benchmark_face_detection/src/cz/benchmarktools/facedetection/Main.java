package cz.benchmarktools.facedetection;

import cz.benchmarktools.facedetection.exception.TaskFailedException;
import cz.benchmarktools.facedetection.task.FaceDetectionTask;

/**
 * Use the Main class only for testing.
 * The source of the images: http://robotics.csie.ncku.edu.tw/Databases/FaceDetect_PoseEstimate.htm#Our_Database_
 */
public class Main {
    public static void main(String[] args) {
//        String inputDir = "./img_input/set_1";
//        String outputDir = "./img_out";

        if (args.length != 2) {
            System.out.println(
                    "There should be 2 command-line arguments: path to the input folder, path to the output folder.");
            System.exit(1);
        }

        // INITIALIZE
        System.out.println("Preparing the task started.");
        String inputDir = args[0];
        String outputDir = args[1];
        FaceDetectionTask faceDetectionTask = new FaceDetectionTask(inputDir, outputDir);
        System.out.println("Preparing the task finished.");

        // ITERATE AND MEASURE
        System.out.println("Performing the task started.");
        try {
            for (int i = 0; i < 10; i++) {
                System.out.println("Iteration " + i + " started");
                long start = System.currentTimeMillis();

                faceDetectionTask.detectFaces();

                long elapsed = System.currentTimeMillis() - start;
                System.out.println("Iteration " + i + " finished in " + elapsed);
            }
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Performing the task finished.");

        // CLEANUP
        System.out.println("Cleaning up the task started.");
        try {
            faceDetectionTask.cleanup();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Cleaning up the task finished.");
    }
}
