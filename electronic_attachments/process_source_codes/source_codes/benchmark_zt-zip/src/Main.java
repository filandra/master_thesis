import cz.benchmarktools.ztzip.tasks.*;
import cz.benchmarktools.ztzip.tasks.exception.TaskFailedException;

/**
 * Enables to execute the test microservices without attaching them to the measuring framework.
 */
public class Main {
    /**
     * The entry point, that enables to select from the available microservices.
     *
     * @param args Use 'unzip' as the command-line argument to run the Unzip task, e.g. 'unzip ./input_res/cpuset4.zip ./output_res/unzipped/'.
     *             Use 'zip' as the command-line argument to run the Zip task, e.g. 'zip ./input_res/to_zip ./output_res/zipped.zip'.
     *             Use 'exist' as the command-line argument to run the Zip task, e.g. 'exist ./input_res/cpuset4.zip xalan-single-cpuset4-heap-no-limit-gc.yaml'.
     *             Use 'compare' as the command-line argument to run the Zip task, e.g. 'compare ./input_res/cpuset4.zip ./output_res/zipped.zip'.
     */
    public static void main(String[] args) {
        AbstractTask action = null;

        if (args.length != 3) {
            System.out.println("There should be 3 command-line arguments. Please read the documentation.");
            System.exit(1);
        }

        switch (args[0]) {
            case Task.ZIP:
                action = new ZipTask(args);
                break;
            case Task.UNZIP:
                action = new UnzipTask(args);
                break;
            case Task.EXIST:
                action = new ExistsTask(args);
                break;
            case Task.COMPARE:
                action = new CompareTask(args);
                break;
            default:
                System.out.println("Unknown task. The application terminates.");
                System.exit(1);
        }

        // INITIALIZE
        System.out.println("Preparing the task started.");
        try {
            action.prepareTask();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Preparing the task finished.");

        // ITERATE AND MEASURE
        System.out.println("Performing the task started.");
        for (int i = 0; i < 15; i++) {
            System.out.println("started");
            long start = System.currentTimeMillis();

            try {
                action.performTask();
            } catch (TaskFailedException e) {
                e.printStackTrace();
            }

            long time = System.currentTimeMillis() - start;
            System.out.println("done " + i + " time: " + time);
        }
        System.out.println("Performing the task finished.");

        // CLEANUP
        System.out.println("Cleaning up the task started.");
        try {
            action.cleanupTask();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Cleaning up the task finished.");
    }
}
