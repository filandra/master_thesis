package cz.benchmarktools.ztzip.tasks;

import cz.benchmarktools.ztzip.tasks.exception.TaskFailedException;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;

/**
 * Checks whether the zip archive contains the specified file. The task's main goal is not to decide whether the archive
 * contains the file, but to generate resource demand. Therefore, the result of search in not used anywhere.
 */
public class ExistsTask extends AbstractTask {
    private static final int FILE_TO_FIND_INDEX = 2;

    private File zipArchive;
    private String fileToFind;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the task and the path of the zip archive and the file.
     */
    public ExistsTask(String... args) {
        super(args);
        this.zipArchive = new File(args[INPUT_FILE_INDEX]);
        this.fileToFind = args[FILE_TO_FIND_INDEX];
    }

    /**
     * Checks whether the zip archive exists.
     *
     * @return True if zip archive exists, otherwise false.
     */
    @Override
    public boolean isProperlyInitialized() {
        return zipArchive.exists();
    }

    @Override
    public void prepareTask() throws TaskFailedException {
        // do nothing here
    }

    /**
     * Calls the {@link ZipUtil#containsEntry(File, String)} (File, File)} to check whether the file is in the zip
     * archive.
     *
     * @throws TaskFailedException Thrown if failed to check whether the file is in the zip archive.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            ZipUtil.containsEntry(zipArchive, fileToFind);
        } catch (Exception e) {
            throw new TaskFailedException("ExistsTask failed", e);
        }
    }

    @Override
    public void cleanupTask() throws TaskFailedException {
        // do nothing here
    }
}
