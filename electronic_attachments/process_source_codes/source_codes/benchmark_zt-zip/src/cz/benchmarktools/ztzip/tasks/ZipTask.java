package cz.benchmarktools.ztzip.tasks;

import cz.benchmarktools.ztzip.tasks.exception.TaskFailedException;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;

/**
 * Zips the given files at the input path into a zip archive. The task's main goal is to generate resource demand.
 * Therefore, the resulting zip archive will not be used anywhere.
 */
public class ZipTask extends AbstractTask {
    private File srcDirectory;
    private File targetZipArchive;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the task, the source directory's path and the target zip archive's path.
     */
    public ZipTask(String[] args) {
        super(args);
        srcDirectory = new File(args[INPUT_FILE_INDEX]);
        targetZipArchive = new File(args[OUTPUT_FILE_INDEX]);
    }

    /**
     * Checks whether the source directory exists.
     *
     * @return True source directory exists, otherwise false.
     */
    @Override
    public boolean isProperlyInitialized() {
        return srcDirectory.exists();
    }

    /**
     * Deletes the target zip archive if it already exists.
     *
     * @throws TaskFailedException Thrown if failed to delete the existing zip archive.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        try {
            if (targetZipArchive.exists() && targetZipArchive.isFile()) {
                targetZipArchive.delete();
            }
        } catch (SecurityException e) {
            throw new TaskFailedException("ZipTask failed", e);
        }
    }

    /**
     * Calls the {@link ZipUtil#pack(File, File)} to zip the source directory.
     *
     * @throws TaskFailedException Thrown if the zipping failed.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            ZipUtil.pack(srcDirectory, targetZipArchive);
        } catch (Exception e) {
            throw new TaskFailedException("ZipTask failed", e);
        }
    }

    /**
     * Deletes the target zip archive if it still exists after the execution.
     *
     * @throws TaskFailedException Thrown if failed to delete the existing zip archive.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        try {
            if (targetZipArchive.exists() && targetZipArchive.isFile()) {
                targetZipArchive.delete();
            }
        } catch (SecurityException e) {
            throw new TaskFailedException("ZipTask failed", e);
        }
    }
}
