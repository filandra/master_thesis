package cz.benchmarktools.ztzip.tasks;

import cz.benchmarktools.ztzip.tasks.exception.TaskFailedException;

/**
 * Base class of Tasks used in the benchmark. Defines the base methods used by all tasks.
 */
public abstract class AbstractTask {
    final static int INPUT_FILE_INDEX = 1;
    final static int OUTPUT_FILE_INDEX = 2;

    /**
     * Creates a new instance.
     *
     * @param args The 0th argument is the task's name, and the following arguments are the task's arguments.
     */
    AbstractTask(String... args) {
    }

    /**
     * Checks whether the task is properly inited. This method should be used to check whether the resources exist
     * (i.e. the input files).
     *
     * @return True if everything is ready for the task execution. Otherwise false.
     */
    public abstract boolean isProperlyInitialized();

    /**
     * Prepares the task before the measurement. The code in this method will not be the part of the measurement.
     *
     * @throws TaskFailedException Thrown if the preparation has failed.
     */
    public abstract void prepareTask() throws TaskFailedException;

    /**
     * Performs the task that should be measured.
     *
     * @throws TaskFailedException Thrown if the task execution has failed.
     */
    public abstract void performTask() throws TaskFailedException;

    /**
     * Cleans up the task's working environment. The code in this method will not be the part of the measurement.
     *
     * @throws TaskFailedException Thrown if the cleanup has failed.
     */
    public abstract void cleanupTask() throws TaskFailedException;
}
