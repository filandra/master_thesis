package cz.benchmarktools.ztzip;

import cz.benchmarktools.ztzip.tasks.*;
import cz.benchmarktools.ztzip.tasks.exception.TaskFailedException;
import cz.sandor.dipl.benchmark.BenchmarkResult;
import cz.sandor.dipl.benchmark.IBenchmarkRunFlowController;
import cz.sandor.dipl.benchmark.MeasurableBenchmark;
import org.pmw.tinylog.Logger;

/**
 * Implements the interface and thus enables the framework to run the benchmark.
 */
public class Benchmark implements MeasurableBenchmark {
    private static final int TASK_INDEX = 0;
    private IBenchmarkRunFlowController runFlowController;
    private AbstractTask task;

    /**
     * Initializes the task.
     *
     * @param args The task's arguments. The 0th argument is tha task's name, and the following arguments are the task's
     *             arguments.
     * @return The result of the initialization. Must be one of {@link BenchmarkResult#INIT_FINISHED}
     * {@link BenchmarkResult#INIT_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult init(String[] args) {
        final int ARGS_REQUIRED_LENGTH = 3;
        if (args.length == ARGS_REQUIRED_LENGTH) {
            switch (args[TASK_INDEX]) {
                case Task.ZIP:
                    task = new ZipTask(args);
                    break;
                case Task.UNZIP:
                    task = new UnzipTask(args);
                    break;
                case Task.EXIST:
                    task = new ExistsTask(args);
                    break;
                case Task.COMPARE:
                    task = new CompareTask(args);
                    break;
                default:
                    return BenchmarkResult.INIT_FAILED;
            }

            try {
                task.prepareTask();
            } catch (TaskFailedException e) {
                return BenchmarkResult.INIT_FAILED;
            }

            if (runFlowController.isForceStopped()) {
                return BenchmarkResult.FORCE_STOPPED;
            } else if (task.isProperlyInitialized()) {
                return BenchmarkResult.INIT_FINISHED;
            } else {
                return BenchmarkResult.INIT_FAILED;
            }
        } else {
            return BenchmarkResult.INIT_FAILED;
        }
    }

    /**
     * Iteratively performs the task until it is enabled. Reports the measuring framework when the task started and
     * finished.
     *
     * @return The result of the benchmark. Must be one of {@link BenchmarkResult#RUN_FINISHED},
     * {@link BenchmarkResult#RUN_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult iterateAndMeasure() {
        try {
            while (runFlowController.canRunAgain()) {
                runFlowController.iterationStarted();

                task.performTask();

                runFlowController.iterationStopped();
                runFlowController.iterationCompleted();
            }
        } catch (TaskFailedException e) {
            if (runFlowController.isIterationRunning()) {
                runFlowController.iterationFailed();
            }
            return BenchmarkResult.RUN_FAILED;
        } catch (Exception e) {
            Logger.error(e);
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.RUN_FINISHED;
        }
    }

    /**
     * Cleans up the working environment created in the {@link #init(String[])} and the temporary files created in
     * {@link #iterateAndMeasure()}.
     *
     * @return The result of the cleanup. Must be one of {@link BenchmarkResult#CLEAN_UP_FINISHED},
     * {@link BenchmarkResult#CLEAN_UP_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult cleanup() {
        try {
            task.cleanupTask();
        } catch (Exception e) {
            Logger.error(e, "Cleanup failed.");
            return BenchmarkResult.CLEAN_UP_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.CLEAN_UP_FINISHED;
        }
    }

    /**
     * Initializes the run flow controller.
     *
     * @param benchmarkRunFlowController The measuring framework's run flow controller.
     */
    @Override
    public void setBenchmarkRunFlowController(IBenchmarkRunFlowController benchmarkRunFlowController) {
        this.runFlowController = benchmarkRunFlowController;
    }
}
