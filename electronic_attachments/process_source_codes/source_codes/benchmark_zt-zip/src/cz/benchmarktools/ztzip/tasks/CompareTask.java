package cz.benchmarktools.ztzip.tasks;

import cz.benchmarktools.ztzip.tasks.exception.TaskFailedException;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;

/**
 * Compares two zip archives. The archives equal if the contain the same files. The task's main goal is not to decide
 * whether the archives are equal or not, but to generate resource demand. Therefore, the result of comparison in not
 * used anywhere.
 */
public class CompareTask extends AbstractTask {
    private static final int ZIP_TO_COMPARE_A_INDEX = 1;
    private static final int ZIP_TO_COMPARE_B_INDEX = 2;

    private File zipA;
    private File zipB;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the task and the path of the zip archives that should be compared.
     */
    public CompareTask(String... args) {
        super(args);
        zipA = new File(args[ZIP_TO_COMPARE_A_INDEX]);
        zipB = new File(args[ZIP_TO_COMPARE_B_INDEX]);
    }

    /**
     * Checks whether the zip archives exist.
     *
     * @return True if both zip archives exist, otherwise false.
     */
    @Override
    public boolean isProperlyInitialized() {
        return zipA.exists() && zipB.exists();
    }

    @Override
    public void prepareTask() throws TaskFailedException {
        // do nothing here
    }

    /**
     * Calls the {@link ZipUtil#archiveEquals(File, File)} to compare the zip archives.
     *
     * @throws TaskFailedException Thrown if failed to compare the zip archives.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            ZipUtil.archiveEquals(zipA, zipB);
        } catch (Exception e) {
            throw new TaskFailedException("CompareTask failed", e);
        }
    }

    @Override
    public void cleanupTask() throws TaskFailedException {
        // do nothing here
    }
}
