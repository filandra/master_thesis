package cz.benchmarktools.ztzip.tasks;

/**
 * Defines the available tasks that the benchmark can execute.
 */
public class Task {
    /**
     * Compresses the source directory into a zip archive.
     */
    public static final String ZIP = "zip";
    /**
     * Extracts the zip archive into a directory.
     */
    public static final String UNZIP = "unzip";
    /**
     * Checks whether the zipp archive contains the given file.
     */
    public static final String EXIST = "exist";
    /**
     * Checks whether the two zip archives are equal.
     */
    public static final String COMPARE = "compare";
}
