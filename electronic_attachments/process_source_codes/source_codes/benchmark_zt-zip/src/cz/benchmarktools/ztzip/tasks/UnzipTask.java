package cz.benchmarktools.ztzip.tasks;

import cz.benchmarktools.ztzip.tasks.exception.TaskFailedException;
import org.zeroturnaround.zip.ZipUtil;
import org.zeroturnaround.zip.commons.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Extracts the given zip archive into the target directory. The task's main goal is to generate resource demand.
 * Therefore, the resulting extracted directory will not be used anywhere.
 */
public class UnzipTask extends AbstractTask {
    private File srcZipArchive;
    private File targetDirectory;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the task, the source archives's path and the target directory's path.
     */
    public UnzipTask(String[] args) {
        super(args);
        srcZipArchive = new File(args[INPUT_FILE_INDEX]);
        targetDirectory = new File(args[OUTPUT_FILE_INDEX]);
    }

    /**
     * Checks whether the source zip archive exists.
     *
     * @return True source zip archive exists, otherwise false.
     */
    @Override
    public boolean isProperlyInitialized() {
        return srcZipArchive.exists();
    }

    /**
     * Deletes the target directory if it already exists.
     *
     * @throws TaskFailedException Thrown if failed to delete the target directory.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        if (targetDirectory.exists() && targetDirectory.isDirectory()) {
            try {
                FileUtils.deleteDirectory(targetDirectory);
            } catch (IOException e) {
                throw new TaskFailedException("UnzipTask failed", e);
            }
        }
    }

    /**
     * Calls the {@link ZipUtil#unpack(File, File)} (File, File)} to extract the source zip archive.
     *
     * @throws TaskFailedException Thrown if failed to extract the zip archive.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            ZipUtil.unpack(srcZipArchive, targetDirectory);
        } catch (Exception e) {
            throw new TaskFailedException("UnzipTask failed", e);
        }
    }

    /**
     * Deletes the target directory if it still exists after the execution.
     *
     * @throws TaskFailedException Thrown if failed to delete the existing target directory.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        if (targetDirectory.exists() && targetDirectory.isDirectory()) {
            try {
                FileUtils.deleteDirectory(targetDirectory);
            } catch (IOException e) {
                throw new TaskFailedException("UnzipTask failed", e);
            }
        }
    }
}
