package org.dacapo.harness;

import cz.sandor.dipl.benchmark.BenchmarkResult;
import cz.sandor.dipl.benchmark.IBenchmarkRunFlowController;
import org.dacapo.parser.Config;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.Locale;

/**
 * The modified version of the {@link TestHarness} class from the original scalabench benchmark suite. The original file
 * was slightly modified in order to comply with the requirements of the microservice's library and
 * to be able to execute by the measurement framework.
 */
public class BRTestHarness {
    private Config config;
    private static CommandLineArgs commandLineArgs;

    private static boolean validBenchmark = true;
    private static String benchmarkName = "";

    private static File scratch;
    private static BRTestHarness harness;

    private IBenchmarkRunFlowController runFlowController;

    public BRTestHarness() {
        // allows the reflection to create a new instance
    }

    public BenchmarkResult init(String[] args) {
        // force the locale so that we don't have any character set issues
        // when generating output for the digests.
        Locale.setDefault(new Locale("en", "AU"));
        Logger.debug("EntryPoint started");

        /* All benchmarks run headless */
        System.setProperty("java.awt.headless", "true");

        try {
            commandLineArgs = new CommandLineArgs(args);

            scratch = new File(commandLineArgs.getScratchDir());
            makeCleanScratch();

            // this is not right
            Benchmark.setCommandLineOptions(commandLineArgs);
            try {
                Config.setThreadCountOverride(Integer.parseInt(commandLineArgs.getThreadCount()));
            } catch (RuntimeException re) {
                Logger.error(re, "Failed on RuntimeException while calling Config.setThreadCountOverride(...)");
                return BenchmarkResult.INIT_FAILED;
            }

            // now get the only benchmark name and run it
            if (commandLineArgs.benchmarks().iterator().hasNext()) {
                benchmarkName = commandLineArgs.benchmarks().iterator().next();
                // check if it is a benchmark name
                // name of file containing configurations
                InputStream ins = null;
                if (commandLineArgs.getCnfOverride() == null) {
                    String cnf = "cnf/" + benchmarkName + ".cnf";
                    ins = BRTestHarness.class.getClassLoader().getResourceAsStream(cnf);
                    if (ins == null) {
                        Logger.error("Unknown benchmark " + benchmarkName);
                        return BenchmarkResult.INIT_FAILED;
                    }
                } else {
                    String cnf = commandLineArgs.getCnfOverride();
                    try {
                        ins = new FileInputStream(cnf);
                    } catch (FileNotFoundException e) {
                        Logger.error("Count not find cnf file: '" + cnf + "'");
                        return BenchmarkResult.INIT_FAILED;
                    }
                }

                harness = new BRTestHarness(ins);

                String size = commandLineArgs.getSize();

                int factor = 0;
                int limit = harness.config.getThreadLimit(size);

                try {
                    factor = Integer.parseInt(commandLineArgs.getThreadFactor());
                    if (0 < factor && harness.config.getThreadModel() == Config.ThreadModel.PER_CPU) {
                        harness.config.setThreadFactor(size, factor);
                    }
                } catch (RuntimeException re) {
                    Logger.error(re,
                                 "Failed on RuntimeException while calling harness.config.setThreadFactor(size, factor);");
                    return BenchmarkResult.INIT_FAILED;
                }

                if (!harness.isValidSize(size)) {
                    Logger.error("No configuration size, " + size + ", for benchmark " + benchmarkName + ".");
                    return BenchmarkResult.INIT_FAILED;
                } else if (factor != 0 && harness.config.getThreadModel() != Config.ThreadModel.PER_CPU) {
                    Logger.error("Can only set the thread factor for per_cpu configurable benchmarks");
                    return BenchmarkResult.INIT_FAILED;
                } else if (!harness.isValidThreadCount(size) && (harness.config
                        .getThreadCountOverride() > 0 || factor > 0)) {
                    Logger.error("The specified number of threads (" + harness.config
                            .getThreadCount(size) + ") is outside the range [1,"
                                         + (limit == 0 ? "unlimited" : "" + limit) + "]");
                    return BenchmarkResult.INIT_FAILED;
                } else if (commandLineArgs.getInformation()) {
                    Logger.debug("harness.bminfo");
                    harness.bmInfo(size);
                    return BenchmarkResult.INIT_FAILED;
                } else {
                    if (!harness.isValidThreadCount(size)) {
                        Logger.error("The derived number of threads (" + harness.config
                                .getThreadCount(size) + ") is outside the range [1,"
                                             + (limit == 0 ? "unlimited" : "" + limit) + "]; rescaling to match thread limit.");
                        harness.config.setThreadCountOverride(harness.config.getThreadLimit(size));
                    }
                    harness.dump(commandLineArgs.getVerbose());
                }
            }
        } catch (Exception e) {
            Logger.error(e, "Uncaught exception");
            return BenchmarkResult.INIT_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.INIT_FINISHED;
        }
    }

    public void setBenchmarkRunFlowController(IBenchmarkRunFlowController benchmarkRunFlowController) {
        this.runFlowController = benchmarkRunFlowController;
    }

    private static void makeCleanScratch() {
        rmdir(scratch);
        scratch.mkdir();
    }

    private boolean isValidSize(String size) {
        return size != null && config.getSizes().contains(size);
    }

    private boolean isValidThreadCount(String size) {
        return config.getThreadLimit(size) == 0 || config.getThreadCount(size) <= config.getThreadLimit(size);
    }

    public BenchmarkResult iterateAndMeasure() {
        Logger.debug("Initialization started in iterateAndMeasure()");
        try {
            harness.config.printThreadModel(System.out, commandLineArgs.getSize(), commandLineArgs.getVerbose());

            Constructor<?> cons = harness.findClass().getConstructor(new Class[]{Config.class, File.class});
            Benchmark benchmark = (Benchmark) cons.newInstance(new Object[]{harness.config, scratch});

            Logger.debug("Initialization done in runAndMeasure()");

            runFlowController.resetTerminationCondition();
            while (runFlowController.canRunAgain()) {
                validBenchmark = benchmark.run(runFlowController, commandLineArgs.getSize()) && validBenchmark;
            }

            benchmark.cleanup();
        } catch (Exception e) {
            Logger.error(e, "Iteration failed.");
            if (runFlowController.isIterationRunning()) {
                runFlowController.iterationFailed();
            }
            return BenchmarkResult.RUN_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else if (isValid()) {
            return BenchmarkResult.RUN_FINISHED;
        } else {
            return BenchmarkResult.RUN_FAILED;
        }
    }

    private boolean isValid() {
        // Validation is skipped if the benchmark was stopped by the user.
        if (!runFlowController.isForceStopped() && !validBenchmark) {
            Logger.error("Validation FAILED for " + benchmarkName + " " + commandLineArgs.getSize());
            if (!commandLineArgs.getIgnoreValidation()) {
                Logger.error("Benchmark validation failed");
                return false;
            }
        }
        return true;
    }

    private static void rmdir(File dir) {
        String[] files = dir.list();
        if (files != null) {
            for (int f = 0; f < files.length; f++) {
                File file = new File(dir, files[f]);
                if (file.isDirectory()) {
                    rmdir(file);
                }
                if (!file.delete()) {
                    System.err.println("Could not delete " + files[f]);
                }
            }
        }
    }

    public void cleanup() {
        // do nothing here
    }

    private void bmInfo(String size) {
        config.describe(System.err, size);
    }

    private void dump(boolean verbose) {
        if (verbose) {
            System.err.println("Class name: " + config.className);

            System.err.println("Configurations:");
            config.describe(System.err, commandLineArgs.getSize());
        }
    }

    private BRTestHarness(InputStream stream) {
        this.config = Config.parse(stream);
        if (this.config == null) {
            System.exit(-1);
        }
    }

    private Class<?> findClass() {
        try {
            return Class.forName(config.className);
        } catch (ClassNotFoundException e) {
            System.err.println(e);
            e.printStackTrace();
            System.exit(-1);
            return null; // not reached
        }
    }
}
