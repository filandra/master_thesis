package cz.benchmarktools.scalabench;

import cz.sandor.dipl.benchmark.BenchmarkResult;
import cz.sandor.dipl.benchmark.IBenchmarkRunFlowController;
import cz.sandor.dipl.benchmark.MeasurableBenchmark;
import org.dacapo.harness.BRTestHarness;
import org.pmw.tinylog.Logger;

/**
 * Implements the interface and thus enables the measuring framework to run the benchmark.
 */
public class Benchmark implements MeasurableBenchmark {
    private BRTestHarness harness;
    private IBenchmarkRunFlowController runFlowController;

    /**
     * Creates a new instance. Initializes the instance of the {@link BRTestHarness}.
     */
    public Benchmark() {
        this.harness = new BRTestHarness();
    }

    /**
     * Initializes the scalabench.
     *
     * @param args The scalabench's arguments. Use the arguments accordingly to the scalabench's documentation.
     * @return The result of the initialization. Must be one of {@link BenchmarkResult#INIT_FINISHED}
     * {@link BenchmarkResult#INIT_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult init(String[] args) {
        return harness.init(args);
    }

    /**
     * Iteratively performs the scalabench's benchmark until it is enabled. Reports the measuring framework when
     * the benchmarks started and finished.
     *
     * @return The result of the benchmark. Must be one of {@link BenchmarkResult#RUN_FINISHED},
     * {@link BenchmarkResult#RUN_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult iterateAndMeasure() {
        try {
            return harness.iterateAndMeasure();
        } catch (Exception e) {
            return BenchmarkResult.RUN_FAILED;
        }
    }

    /**
     * Cleans up the working environment created in the {@link #init(String[])} and the temporary files created in
     * {@link #iterateAndMeasure()}.
     *
     * @return The result of the cleanup. Must be one of {@link BenchmarkResult#CLEAN_UP_FINISHED},
     * {@link BenchmarkResult#CLEAN_UP_FAILED} or {@link BenchmarkResult#FORCE_STOPPED}.
     */
    @Override
    public BenchmarkResult cleanup() {
        try {
            harness.cleanup();
        } catch (Exception e) {
            Logger.error(e, "Cleanup failed.");
            return BenchmarkResult.CLEAN_UP_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.CLEAN_UP_FINISHED;
        }
    }

    /**
     * Initializes the run flow controller.
     *
     * @param benchmarkRunFlowController The measuring framework's run flow controller.
     */
    @Override
    public void setBenchmarkRunFlowController(IBenchmarkRunFlowController benchmarkRunFlowController) {
        this.runFlowController = benchmarkRunFlowController;
        harness.setBenchmarkRunFlowController(runFlowController);
    }
}
