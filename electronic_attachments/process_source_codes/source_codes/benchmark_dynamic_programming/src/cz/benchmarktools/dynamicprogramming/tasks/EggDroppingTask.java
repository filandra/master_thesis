package cz.benchmarktools.dynamicprogramming.tasks;

import cz.benchmarktools.dynamicprogramming.tasks.exception.TaskFailedException;

/**
 * Implementation of the Egg dropping algorithm.
 * The source code is taken from <a href=https://github.com/TheAlgorithms/Java/blob/master/DynamicProgramming/EggDropping.java>TheAlgorithms GitHub page</a>
 * and might be slightly modified to meet our requirements.
 */
public class EggDroppingTask extends AbstractTask {
    private String eggsString;
    private String floorsString;
    private int eggs;
    private int floors;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the command-line arguments. However, in this case the arguments are not
     *             applicable.
     */
    public EggDroppingTask(String... args) {
        super(args);
        // eggs = 3; floors = 80000
        eggsString = "3";
        floorsString = "80000";
    }

    /**
     * Initializes the attributes that will be used in the algorithm.
     *
     * @throws TaskFailedException Thrown if failed to initialize the attributes.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        try {
            eggs = Integer.parseInt(eggsString);
            floors = Integer.parseInt(floorsString);
        } catch (NumberFormatException e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Executes the Egg dropping algorithm.
     *
     * @throws TaskFailedException Thrown if failed to successfully perform the task.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            minTrials();
            //System.gc();
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * The implementation of the Egg dropping algorithm.
     *
     * @return The result of the algorithm.
     */
    private int minTrials() {

        int[][] eggFloor = new int[eggs + 1][floors + 1];
        int result, x;

        for (int i = 1; i <= eggs; i++) {
            eggFloor[i][0] = 0;   // Zero trial for zero floor.
            eggFloor[i][1] = 1;   // One trial for one floor
        }

        // j trials for only 1 egg

        for (int j = 1; j <= floors; j++) {
            eggFloor[1][j] = j;
        }

        // Using bottom-up approach in DP

        for (int i = 2; i <= eggs; i++) {
            for (int j = 2; j <= floors; j++) {
                eggFloor[i][j] = Integer.MAX_VALUE;
                for (x = 1; x <= j; x++) {
                    result = 1 + Math.max(eggFloor[i - 1][x - 1], eggFloor[i][j - x]);

                    // choose min of all values for particular x
                    if (result < eggFloor[i][j]) {
                        eggFloor[i][j] = result;
                    }
                }
            }
        }

        return eggFloor[eggs][floors];
    }

    /**
     * Cleans up the attributes.
     *
     * @throws TaskFailedException Thrown if the clean up failed.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        eggsString = null;
        floorsString = null;
        eggs = 0;
        floors = 0;
    }
}
