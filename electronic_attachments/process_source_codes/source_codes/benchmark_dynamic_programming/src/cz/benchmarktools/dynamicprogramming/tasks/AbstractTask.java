package cz.benchmarktools.dynamicprogramming.tasks;

import cz.benchmarktools.dynamicprogramming.tasks.exception.TaskFailedException;

/**
 * Base class of Tasks used in the benchmark. Defines the base methods used by all tasks.
 */
public abstract class AbstractTask {
    /**
     * Creates a new instance.
     *
     * @param args The 0th argument is the task's name, and the following arguments are the task's arguments.
     */
    AbstractTask(String... args) {
    }

    /**
     * Prepares the task before the measurement. This method should be used to check whether the resources exist
     * (i.e. the input files). The code in this method will not be the part of the measurement.
     *
     * @throws TaskFailedException Thrown if the preparation has failed.
     */
    public abstract void prepareTask() throws TaskFailedException;

    /**
     * Performs the task that should be measured.
     *
     * @throws TaskFailedException Thrown if the task execution has failed.
     */
    public abstract void performTask() throws TaskFailedException;

    /**
     * Cleans up the task's working environment. The code in this method will not be the part of the measurement.
     *
     * @throws TaskFailedException Thrown if the cleanup has failed.
     */
    public abstract void cleanupTask() throws TaskFailedException;
}
