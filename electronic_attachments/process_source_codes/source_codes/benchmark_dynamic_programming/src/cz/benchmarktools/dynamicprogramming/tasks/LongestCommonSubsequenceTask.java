package cz.benchmarktools.dynamicprogramming.tasks;

import cz.benchmarktools.dynamicprogramming.Util;
import cz.benchmarktools.dynamicprogramming.tasks.exception.TaskFailedException;

/**
 * Implementation of the Longest Common Subsequence  algorithm.
 * The source code is taken from <a href=https://github.com/TheAlgorithms/Java/blob/master/DynamicProgramming/LongestCommonSubsequence.java>TheAlgorithms GitHub page</a>
 * and might be slightly modified to meet our requirements.
 */
public class LongestCommonSubsequenceTask extends AbstractTask {
    private static long LONGEST_COMMON_SUBSEQUENCE_SEED = 148954;
    private static int LONGEST_COMMON_SUBSEQUENCE_WORD_1_LENGTH = 30000;
    private static int LONGEST_COMMON_SUBSEQUENCE_WORD_2_LENGTH = 30000;

    private String wordOne;
    private String wordTwo;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the command-line arguments. However, in this case the arguments are not
     *             applicable.
     */
    public LongestCommonSubsequenceTask(String... args) {
        super(args);
    }

    /**
     * Initializes the attributes that will be used in the algorithm.
     *
     * @throws TaskFailedException Thrown if failed to initialize the attributes.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        wordOne = Util.generateRandomString(LONGEST_COMMON_SUBSEQUENCE_SEED, LONGEST_COMMON_SUBSEQUENCE_WORD_1_LENGTH);
        wordTwo = Util.generateRandomString(LONGEST_COMMON_SUBSEQUENCE_SEED, LONGEST_COMMON_SUBSEQUENCE_WORD_2_LENGTH);
    }

    /**
     * Executes the Longest Common Subsequence algorithm.
     *
     * @throws TaskFailedException Thrown if failed to successfully perform the task.
     */
    @Override
    public void performTask() throws TaskFailedException {
        getLCS();
        System.gc();
    }

    /**
     * The implementation of the Longest Common Subsequence algorithm.
     *
     * @return The result of the algorithm.
     */
    private String getLCS() {
        //At least one string is null
        if (wordOne == null || wordTwo == null) {
            return null;
        }

        //At least one string is empty
        if (wordOne.length() == 0 || wordTwo.length() == 0) {
            return "";
        }

        String[] arr1 = wordOne.split("");
        String[] arr2 = wordTwo.split("");

        //lcsMatrix[i][j]  = LCS of first i elements of arr1 and first j characters of arr2
        int[][] lcsMatrix = new int[arr1.length + 1][arr2.length + 1];

        for (int i = 0; i < arr1.length + 1; i++) {
            lcsMatrix[i][0] = 0;
        }
        for (int j = 1; j < arr2.length + 1; j++) {
            lcsMatrix[0][j] = 0;
        }
        for (int i = 1; i < arr1.length + 1; i++) {
            for (int j = 1; j < arr2.length + 1; j++) {
                if (arr1[i - 1].equals(arr2[j - 1])) {
                    lcsMatrix[i][j] = lcsMatrix[i - 1][j - 1] + 1;
                } else {
                    lcsMatrix[i][j] =
                            lcsMatrix[i - 1][j] > lcsMatrix[i][j - 1] ? lcsMatrix[i - 1][j] : lcsMatrix[i][j - 1];
                }
            }
        }
        return lcsString(wordOne, wordTwo, lcsMatrix);
    }

    /**
     * Part of the implementation of the Longest Common Subsequence algorithm.
     *
     * @return The result of the algorithm.
     */
    private String lcsString(String str1, String str2, int[][] lcsMatrix) {
        StringBuilder lcs = new StringBuilder();
        int i = str1.length(),
                j = str2.length();
        while (i > 0 && j > 0) {
            if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                lcs.append(str1.charAt(i - 1));
                i--;
                j--;
            } else if (lcsMatrix[i - 1][j] > lcsMatrix[i][j - 1]) {
                i--;
            } else {
                j--;
            }
        }
        return lcs.reverse().toString();
    }

    /**
     * Cleans up the attributes.
     *
     * @throws TaskFailedException Thrown if the clean up failed.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        wordOne = null;
        wordTwo = null;
    }
}
