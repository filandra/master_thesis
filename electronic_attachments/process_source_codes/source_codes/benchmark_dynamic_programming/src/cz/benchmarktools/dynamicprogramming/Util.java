package cz.benchmarktools.dynamicprogramming;

import java.nio.charset.Charset;
import java.util.Random;

/**
 * Contains utility method for generating input for the dynamic programming algorithms.
 */
public class Util {

    /**
     * Generates a random String using the given random generator seed and the given length.
     *
     * @param seed   Seed for the random number generator.
     * @param length The expected length of the String.
     * @return The generated String.
     */
    public static String generateRandomString(long seed, int length) {
        byte[] array = new byte[length];
        new Random(seed).nextBytes(array);
        return new String(array, Charset.forName("UTF-8"));
    }

    /**
     * Generates an array of integers using the given random generator seed and the given length.
     *
     * @param seed  Seed for the random number generator.
     * @param count The expected length of the integer array.
     * @return The generated integer array.
     */
    public static int[] generateValues(long seed, int count) {
        Random rnd = new Random(seed);
        int[] values = new int[count];

        for (int i = 0; i < count; i++) {
            values[i] = rnd.nextInt();
        }
        return values;
    }

    /**
     * Generates an array of integers between the given boundaries using the given random generator seed and the given length.
     *
     * @param seed       Seed for the random number generator.
     * @param count      The expected length of the integer array.
     * @param lowerBound The lower boundary.
     * @param upperBound The upper boundary.
     * @return The generated integer array.
     */
    public static int[] generateValues(long seed, int count, int lowerBound, int upperBound) {
        Random rnd = new Random(seed);
        int[] values = new int[count];

        for (int i = 0; i < count; i++) {
            values[i] = lowerBound + (rnd.nextInt(upperBound - lowerBound));
        }
        return values;
    }
}
