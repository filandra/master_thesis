package cz.benchmarktools.dynamicprogramming.tasks;

/**
 * Defines the available tasks that the benchmark can execute.
 */
public class Task {
    /**
     * Runs rod cutting problem.
     */
    public static final String ROD_CUTTING = "rod_cutting";

    /**
     * Runs egg dropping problem.
     */
    public static final String EGG_DROPPING = "egg_dropping";

    /**
     * Runs edit distance problem.
     */
    public static final String EDIT_DISTANCE = "edit_distance";

    /**
     * Runs longest common subsequence problem.
     */
    public static final String LSC = "lsc";
}
