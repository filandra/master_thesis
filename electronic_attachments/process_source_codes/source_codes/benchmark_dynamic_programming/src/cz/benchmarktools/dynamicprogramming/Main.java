package cz.benchmarktools.dynamicprogramming;

import cz.benchmarktools.dynamicprogramming.tasks.*;
import cz.benchmarktools.dynamicprogramming.tasks.exception.TaskFailedException;

/**
 * Enables to execute the test microservices without attaching them to the measuring framework.
 */
public class Main {
    /**
     * The entry point, that enables to select from the available microservices.
     *
     * @param args Use 'edit_distance' as the command-line argument to run the EditDistance task.
     *             Use 'egg_dropping' as the command-line argument to run the EggDropping task.
     *             Use 'rod_cutting' as the command-line argument to run the RodCutting task.
     *             Use 'lsc' as the command-line argument to run the LongestCommonSubsequence task.
     */
    public static void main(String[] args) {
        AbstractTask action = null;

        if (args.length != 1) {
            System.out.println("There should be 1 command-line argument. Please read the documentation.");
            System.exit(1);
        }

        switch (args[0]) {
            case Task.EDIT_DISTANCE:
                action = new EditDistanceTask();
                break;
            case Task.EGG_DROPPING:
                action = new EggDroppingTask();
                break;
            case Task.ROD_CUTTING:
                action = new RodCuttingTask();
                break;
            case Task.LSC:
                action = new LongestCommonSubsequenceTask();
                break;
            default:
                System.out.println("Unknown task. The application terminates.");
                System.exit(1);
        }

        // INITIALIZE
        System.out.println("Preparing the task started.");
        try {
            action.prepareTask();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Preparing the task finished.");

        // ITERATE AND MEASURE
        System.out.println("Performing the task started.");
        for (int i = 0; i < 15; i++) {
            System.out.println("started");
            long start = System.currentTimeMillis();

            try {
                action.performTask();
            } catch (TaskFailedException e) {
                e.printStackTrace();
            }

            long time = System.currentTimeMillis() - start;
            System.out.println("done " + i + " time: " + time);
        }
        System.out.println("Performing the task finished.");

        // CLEANUP
        System.out.println("Cleaning up the task started.");
        try {
            action.cleanupTask();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Cleaning up the task finished.");
    }
}
