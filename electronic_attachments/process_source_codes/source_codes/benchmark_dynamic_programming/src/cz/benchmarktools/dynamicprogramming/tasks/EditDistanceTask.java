package cz.benchmarktools.dynamicprogramming.tasks;

import cz.benchmarktools.dynamicprogramming.Util;
import cz.benchmarktools.dynamicprogramming.tasks.exception.TaskFailedException;

/**
 * Implementation of the Edit Distance algorithm.
 * The source code is taken from <a href=https://github.com/TheAlgorithms/Java/blob/master/DynamicProgramming/EditDistance.java>TheAlgorithms GitHub page</a>
 * and might be slightly modified to meet our requirements.
 */
public class EditDistanceTask extends AbstractTask {
    private static long EDIT_DISTANCE_SEED = 148954;
    private static int EDIT_DISTANCE_WORD_1_LENGTH = 30000;
    private static int EDIT_DISTANCE_WORD_2_LENGTH = 30000;

    private String wordOne;
    private String wordTwo;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the command-line arguments. However, in this case the arguments are not
     *             applicable.
     */
    public EditDistanceTask(String... args) {
        super(args);
    }

    /**
     * Initializes the attributes that will be used in the algorithm.
     *
     * @throws TaskFailedException Thrown if failed to initialize the attributes.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        wordOne = Util.generateRandomString(EDIT_DISTANCE_SEED, EDIT_DISTANCE_WORD_1_LENGTH);
        wordTwo = Util.generateRandomString(EDIT_DISTANCE_SEED, EDIT_DISTANCE_WORD_2_LENGTH);
    }

    /**
     * Executes the Edit Distance algorithm.
     *
     * @throws TaskFailedException Thrown if failed to successfully perform the task.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            minDistance();
            System.gc();
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * The implementation of the Edit Distance algorithm.
     *
     * @return The result of the algorithm.
     */
    private int minDistance() {
        int len1 = wordOne.length();
        int len2 = wordTwo.length();

        //len1+1, len2+1, because finally return dp[len1][len2]
        int[][] dp = new int[len1 + 1][len2 + 1];

        //If second string is empty, the only option is to
        //insert all characters of first string into second
        for (int i = 0; i <= len1; i++) {
            dp[i][0] = i;
        }

        //If first string is empty, the only option is to
        //insert all characters of second string into first
        for (int j = 0; j <= len2; j++) {
            dp[0][j] = j;
        }

        //iterate though, and check last char
        for (int i = 0; i < len1; i++) {
            char c1 = wordOne.charAt(i);
            for (int j = 0; j < len2; j++) {
                char c2 = wordTwo.charAt(j);

                //if last two chars equal
                if (c1 == c2) {
                    //update dp value for +1 length
                    dp[i + 1][j + 1] = dp[i][j];
                } else {
                    //if two characters are different,
                    //then take the minimum of the various operations(i.e insertion,removal,substitution)

                    int replace = dp[i][j] + 1;
                    int insert = dp[i][j + 1] + 1;
                    int delete = dp[i + 1][j] + 1;

                    int min = replace > insert ? insert : replace;
                    min = delete > min ? min : delete;
                    dp[i + 1][j + 1] = min;
                }
            }
        }
        //return the final answer , after traversing through both the strings
        return dp[len1][len2];
    }

    /**
     * Cleans up the attributes.
     *
     * @throws TaskFailedException Thrown if the clean up failed.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        wordOne = null;
        wordTwo = null;
    }
}
