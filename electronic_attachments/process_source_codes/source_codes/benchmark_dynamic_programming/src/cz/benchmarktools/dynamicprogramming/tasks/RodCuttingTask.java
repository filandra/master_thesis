package cz.benchmarktools.dynamicprogramming.tasks;

import cz.benchmarktools.dynamicprogramming.Util;
import cz.benchmarktools.dynamicprogramming.tasks.exception.TaskFailedException;

/**
 * Implementation of the Rod cutting algorithm.
 * The source code is taken from <a href=https://www.geeksforgeeks.org/cutting-a-rod-dp-13/>GeeksforGeeks</a>
 * and might be slightly modified to meet our requirements.
 */
public class RodCuttingTask extends AbstractTask {
    private static long ROD_CUTTING_SEED = 148954;
    private static int ROD_CUTTING_COUNT = 150000;
    private static int ROD_CUTTING_RANGE_FROM = 50;
    private static int ROD_CUTTING_RANGE_TO = 2000;

    private int[] prices;
    private int length;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the command-line arguments. However, in this case the arguments are not
     *             applicable.
     */
    public RodCuttingTask(String... args) {
        super(args);
    }

    /**
     * Initializes the attributes that will be used in the algorithm.
     *
     * @throws TaskFailedException Thrown if failed to initialize the attributes.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        prices = Util.generateValues(ROD_CUTTING_SEED, ROD_CUTTING_COUNT, ROD_CUTTING_RANGE_FROM, ROD_CUTTING_RANGE_TO);
        length = prices.length; // ROD_CUTTING_COUNT
    }

    /**
     * Executes the Rod cutting algorithm.
     *
     * @throws TaskFailedException Thrown if failed to successfully perform the task.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            cutRod();
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * The implementation of the Rod cutting algorithm.
     *
     * @return The result of the algorithm.
     */
    private int cutRod() {
        int[] val = new int[length + 1];
        val[0] = 0;

        for (int i = 1; i <= length; i++) {
            int max_val = Integer.MIN_VALUE;
            for (int j = 0; j < i; j++) {
                max_val = Math.max(max_val, prices[j] + val[i - j - 1]);
            }

            val[i] = max_val;
        }
        return val[length];
    }

    /**
     * Cleans up the attributes.
     *
     * @throws TaskFailedException Thrown if the clean up failed.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        prices = null;
    }
}
