package logic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.pmw.tinylog.Logger;

import java.io.*;


public class JSOND {

    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private String directory = "./data/";
    private String extension = ".json";

    public JSOND() {
    }

    public void init() {
        File outDir = new File(directory);
        if (!outDir.exists()) {
            outDir.mkdirs();
        }
    }

    public void run() throws IOException {
        long startTime = System.currentTimeMillis();

        Information inf;
        for (int i = 0; i < 100; i++) {
            inf = new Information();
            save(inf, i);
        }

        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(estimatedTime);
    }

    private void deleteRecursively(File file) throws IOException {
        if (file.isDirectory()) {
            File[] entries = file.listFiles();
            if (entries != null) {
                for (File entry : entries) {
                    deleteRecursively(entry);
                }
            }
        }
        if (!file.delete()) {
            throw new IOException("Failed to delete " + file);
        }
    }

    public void cleanup() throws IOException {
        File file = new File(directory);
        deleteRecursively(file);
    }

    private void save(Information inf, int number) throws IOException {
        try (Writer writer = new FileWriter(directory + String.valueOf(number) + extension)) {
            gson.toJson(inf, writer);
        } catch (IOException e) {
            Logger.error(e, "JSOND failed");
            throw new IOException(e);
        }
//        System.out.println("Saved json number " + number);
    }
}
