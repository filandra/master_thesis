package logic;

import com.google.gson.annotations.SerializedName;

import java.util.*;

class Information {

    private enum Enum {
        @SerializedName("1") LEFT,
        @SerializedName("0") UP,
        @SerializedName("1") RIGHT,
        @SerializedName("0") DOWN
    }

    private class Inner {
        private String name;
        private int number;

        public Inner(String name, int number) {
            this.name = name;
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return number;
        }

        public void setAge(int number) {
            this.number = number;
        }

        public String toString() {
            return "Inner [ name: " + name + ", num: " + number + " ]";
        }
    }

    private Map<String, Enum> dictionary = new HashMap<String, Enum>();
    private List<Inner> list = new LinkedList<Inner>();
    private double[][] array;

    Information() {
        Random rand = new Random(2);
        int max = 200;
        int min = 150;
        int random = rand.nextInt((max - min) + 1) + min;


        for (int i = 0; i < random * random; i++) {
            dictionary.put(String.valueOf(random), Enum.values()[i % 4]);
            list.add(new Inner(String.valueOf(random), random));
        }

        array = new double[random][random];

        for (int i = 0; i < random; i++) {
            for (int j = 0; j < random; j++) {
                array[i][j] = random + i + j;
            }
        }
    }
}
