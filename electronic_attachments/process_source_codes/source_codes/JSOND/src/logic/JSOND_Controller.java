package logic;

import java.io.IOException;

public class JSOND_Controller {

    public static void main(String[] args){
        JSOND jsond = new JSOND();
        try {
            jsond.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}