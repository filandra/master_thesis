package cz.benchmarktools.command.task;

import cz.benchmarktools.command.exception.TaskFailedException;

import java.io.IOException;

/**
 * Executes the command which represents the benchmark.
 */
public class CommandTask {
    private String[] command;

    /**
     * Creates a new instance of the task.
     *
     * @param command The command and its arguments.
     */
    public CommandTask(String[] command) {
        this.command = command;
    }

    public void prepareTask() {
        // do nothing here
    }

    /**
     * Executes the command as a process.
     *
     * @throws TaskFailedException Thrown if it failed to execute the process for some reason.
     */
    public void runCommand() throws TaskFailedException {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            Process process = processBuilder.start();
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Deletes the command.
     */
    public void cleanup() {
        command = null;
    }
}
