package cz.benchmarktools.graphs.tasks;

import cz.benchmarktools.graphs.tasks.algorithms.FloydWarshall;
import cz.benchmarktools.graphs.tasks.exception.TaskFailedException;

/**
 * Generates a graph that includes 2_200 vertices and runs the Floyd-Warshall algorithms on this graph. The task's main
 * goal is to generate resource demand. Therefore the result of the Floyd-Warshall algorithm will not be used anywhere.
 */
public class FWTask extends AbstractTask {
    private static final int VERTICES = 2_200;
    private FloydWarshall floydWarshall;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the command-line arguments. However, in this case the arguments are not
     *             applicable.
     */
    public FWTask(String... args) {
        super(args);
        this.floydWarshall = new FloydWarshall(VERTICES);
    }

    /**
     * Generates the graph that will be used in the Floyd-Warshall algorithm.
     *
     * @throws TaskFailedException Thrown if failed to generate the graph.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        try {
            floydWarshall.generateMatrix();
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Executes the Floyd-Warshall algorithm on the generated graph.
     *
     * @throws TaskFailedException Thrown if failed to successfully perform the task.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            floydWarshall.compute();
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Deletes the generated graph.
     *
     * @throws TaskFailedException Thrown if the clean up failed.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        floydWarshall = null;
    }
}
