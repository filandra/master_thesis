package cz.benchmarktools.graphs.tasks;

/**
 * Defines the available tasks that the benchmark can execute.
 */
public class Task {
    /**
     * Runs FLOYD_WARSHALL algorithm.
     */
    public static final String FLOYD_WARSHALL = "fw";

    /**
     * Runs PRIM's algorithm.
     */
    public static final String PRIM = "prim";

}
