package cz.benchmarktools.graphs.tasks.algorithms;

import java.util.Random;

/**
 * Implementation of the Prim algorithm.
 * The source code is taken from <a href=https://github.com/TheAlgorithms/Java/blob/master/DataStructures/Graphs/PrimMST.java>TheAlgorithms GitHub page</a>
 * and might be slightly modified to meet our requirements.
 */
public class Prim {
    private int[][] graph;
    private int vertices;

    public Prim(int vertices) {
        this.vertices = vertices;
    }

    // Function to construct and print MST for a graph represented
    //  using adjacency matrix representation
    public void compute() {
        // Array to store constructed MST
        int[] parent = new int[vertices];

        // Key values used to pick minimum weight edge in cut
        int[] key = new int[vertices];

        // To represent set of vertices not yet included in MST
        Boolean[] mstSet = new Boolean[vertices];

        // Initialize all keys as INFINITE
        for (int i = 0; i < vertices; i++) {
            key[i] = Integer.MAX_VALUE;
            mstSet[i] = false;
        }

        // Always include first 1st vertex in MST.
        key[0] = 0;     // Make key 0 so that this vertex is
        // picked as first vertex
        parent[0] = -1; // First node is always root of MST

        // The MST will have vertices vertices
        for (int count = 0; count < vertices - 1; count++) {
            // Pick thd minimum key vertex from the set of vertices
            // not yet included in MST
            int u = minKey(key, mstSet);

            // Add the picked vertex to the MST Set
            mstSet[u] = true;

            // Update key value and parent index of the adjacent
            // vertices of the picked vertex. Consider only those
            // vertices which are not yet included in MST
            for (int v = 0; v < vertices; v++)

            // graph[u][v] is non zero only for adjacent vertices of m
            // mstSet[v] is false for vertices not yet included in MST
            // Update the key only if graph[u][v] is smaller than key[v]
            {
                if (graph[u][v] != 0 && mstSet[v] == false &&
                        graph[u][v] < key[v]) {
                    parent[v] = u;
                    key[v] = graph[u][v];
                }
            }
        }
    }

    // A utility function to find the vertex with minimum key
    // value, from the set of vertices not yet included in MST
    private int minKey(int[] key, Boolean[] mstSet) {
        // Initialize min value
        int min = Integer.MAX_VALUE, min_index = -1;

        for (int v = 0; v < vertices; v++) {
            if (mstSet[v] == false && key[v] < min) {
                min = key[v];
                min_index = v;
            }
        }

        return min_index;
    }

    public void generateGraph() {
        final int MAX_WEIGHT = 1000;
        final int SEED = 148954;
        int maxEdges = vertices * (vertices - 1) / 2;
        int edgesCount = 0;

        graph = new int[vertices][vertices];
        Random random = new Random(SEED);

        for (int i = 0; i < vertices; i++) {
            for (int j = i + 1; j < vertices; j++) {
                int weight = random.nextInt(MAX_WEIGHT);

                if (weight % 5 == 0) {
                    continue;
                }

                if (weight > 0 && (edgesCount + 1) <= maxEdges) {
                    graph[i][j] = weight;
                    graph[j][i] = weight;
                    edgesCount++;
                }
            }
        }
    }
}
