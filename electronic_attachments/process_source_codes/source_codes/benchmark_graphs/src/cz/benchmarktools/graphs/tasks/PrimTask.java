package cz.benchmarktools.graphs.tasks;

import cz.benchmarktools.graphs.tasks.algorithms.Prim;
import cz.benchmarktools.graphs.tasks.exception.TaskFailedException;

/**
 * Generates a graph that includes 30_000 vertices and runs the Prim's algorithm twice on this graph. The task's main
 * goal is to generate resource demand. Therefore the result of the Prim's algorithm will not be used anywhere.
 */
public class PrimTask extends AbstractTask {
    private static final int VERTICES = 30_000;
    private Prim prim;

    /**
     * Creates a new instance.
     *
     * @param args String array containing the command-line arguments. However, in this case the arguments are not
     *             applicable.
     */
    public PrimTask(String... args) {
        super(args);
        this.prim = new Prim(VERTICES);
    }

    /**
     * Generates the graph that will be used in the Prim's algorithm.
     *
     * @throws TaskFailedException Thrown if failed to generate the graph.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        try {
            prim.generateGraph();
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Executes the Prim's algorithm twice on the generated graph. There is no other point of executing the algorithm
     * twice than to increase the execution time.
     *
     * @throws TaskFailedException Thrown if failed to successfully perform the task.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            prim.compute();
            prim.compute();
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Deletes the generated graph.
     *
     * @throws TaskFailedException Thrown if the clean up failed.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        prim = null;
    }
}
