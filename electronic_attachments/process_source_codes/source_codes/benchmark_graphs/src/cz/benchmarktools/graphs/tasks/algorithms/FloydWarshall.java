package cz.benchmarktools.graphs.tasks.algorithms;

import java.util.Random;

/**
 * Implementation of the Floyd-Warshall algorithm.
 * The source code is taken from <a href=https://www.geeksforgeeks.org/floyd-warshall-algorithm-dp-16/>GeeksforGeeks</a>
 * and might be slightly modified to meet our requirements.
 */
public class FloydWarshall {
    private final int INF = 99999;
    private int[][] matrix;
    private int vertices;

    public FloydWarshall(int vertices) {
        this.vertices = vertices;
    }

    public void compute() {
        int[][] dist = new int[vertices][vertices];
        int i, j, k;

        /* Initialize the solution matrix same as input graph matrix.
           Or we can say the initial values of shortest distances
           are based on shortest paths considering no intermediate
           vertex. */
        for (i = 0; i < vertices; i++) {
            for (j = 0; j < vertices; j++) {
                dist[i][j] = matrix[i][j];
            }
        }

        /* Add all vertices one by one to the set of intermediate
           vertices.
          ---> Before start of an iteration, we have shortest
               distances between all pairs of vertices such that
               the shortest distances consider only the vertices in
               set {0, 1, 2, .. k-1} as intermediate vertices.
          ----> After the end of an iteration, vertex no. k is added
                to the set of intermediate vertices and the set
                becomes {0, 1, 2, .. k} */
        for (k = 0; k < vertices; k++) {
            // Pick all vertices as source one by one
            for (i = 0; i < vertices; i++) {
                // Pick all vertices as destination for the
                // above picked source
                for (j = 0; j < vertices; j++) {
                    // If vertex k is on the shortest path from
                    // i to j, then update the value of dist[i][j]
                    if (dist[i][k] + dist[k][j] < dist[i][j]) {
                        dist[i][j] = dist[i][k] + dist[k][j];
                    }
                }
            }
        }
    }

    public void generateMatrix() {
        final int MAX_WEIGHT = 1000;
        final int SEED = 148954;
        matrix = new int[vertices][vertices];
        Random random = new Random(SEED);

        for (int i = 0; i < vertices; i++) {
            for (int j = i + 1; j < vertices; j++) {
                int weight = random.nextInt(MAX_WEIGHT);

                if (weight % 5 == 0) {
                    weight = INF;
                }

                matrix[i][j] = weight;
                matrix[j][i] = INF;
            }
        }
    }
}
