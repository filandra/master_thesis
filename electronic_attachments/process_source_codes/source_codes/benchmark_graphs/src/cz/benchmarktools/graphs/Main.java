package cz.benchmarktools.graphs;

import cz.benchmarktools.graphs.tasks.AbstractTask;
import cz.benchmarktools.graphs.tasks.FWTask;
import cz.benchmarktools.graphs.tasks.PrimTask;
import cz.benchmarktools.graphs.tasks.Task;
import cz.benchmarktools.graphs.tasks.exception.TaskFailedException;

/**
 * Enables to execute the test microservices without attaching them to the measuring framework.
 */
public class Main {
    /**
     * The entry point, that enables to select from the available microservices.
     *
     * @param args Use 'fw' as the command line argument to run the Floyd-Warshall, otherwise use 'prim' to run
     *             the Prim microservice.
     */
    public static void main(String[] args) {
        AbstractTask action = null;

        if (args.length != 1) {
            System.out.println("Unknown task. Use either 'fw' or 'prim'. The application terminates.");
            System.exit(1);
        }

        switch (args[0]) {
            case Task.FLOYD_WARSHALL:
                action = new FWTask();
                break;
            case Task.PRIM:
                action = new PrimTask(args);
                break;
            default:
                System.out.println("Unknown task. Use either 'fw' or 'prim'. The application terminates.");
                System.exit(-1);
        }

        // INITIALIZE
        System.out.println("Preparing the task started.");
        try {
            action.prepareTask();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Preparing the task finished.");

        // ITERATE AND MEASURE
        System.out.println("Performing the task started.");
        for (int i = 0; i < 15; i++) {
            long start = System.currentTimeMillis();

            try {
                action.performTask();
            } catch (TaskFailedException e) {
                e.printStackTrace();
            }

            long time = System.currentTimeMillis() - start;
            System.out.println("done " + i + " time: " + time);
        }
        System.out.println("Performing the task finished.");

        // CLEANUP
        System.out.println("Cleaning up the task started.");
        try {
            action.cleanupTask();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Cleaning up the task finished.");
    }
}

