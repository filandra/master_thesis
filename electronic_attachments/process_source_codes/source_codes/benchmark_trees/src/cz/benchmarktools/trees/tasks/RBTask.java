package cz.benchmarktools.trees.tasks;

import cz.benchmarktools.trees.tasks.exception.TaskFailedException;
import cz.benchmarktools.trees.tasks.tree.RBNode;
import cz.benchmarktools.trees.tasks.tree.RedBlackBST;

import java.util.Random;

/**
 * Inserts 5-million nodes into a Red-Black tree and removes these nodes afterwards. The task's main goal is to generate
 * resource demand. Therefore the created Red-Black tree will not be used anywhere.
 */
public class RBTask extends AbstractTask {
    private static final int SIZE = 5_000_000;
    private static final int SEED = 148954;
    private int[] items = new int[SIZE];

    /**
     * Creates a new instance.
     *
     * @param args String array containing the command-line arguments. However, in this case the arguments are not
     *             applicable.
     */
    public RBTask(String... args) {
        super(args);
    }

    /**
     * Generates the item that will be used to create the Red-Black tree.
     *
     * @throws TaskFailedException Thrown if failed to initialize the array of items.
     */
    @Override
    public void prepareTask() throws TaskFailedException {
        try {
            Random rnd = new Random(SEED);
            for (int i = 0; i < SIZE; i++) {
                items[i] = rnd.nextInt();
            }
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Builds a Red-Black tree by inserting the generated items one-by-one. Afterwards destroys the Red-Black tree by
     * deleting the added item one-by-one.
     *
     * @throws TaskFailedException Thrown if failed to successfully perform the task.
     */
    @Override
    public void performTask() throws TaskFailedException {
        try {
            RedBlackBST redBlackBST = new RedBlackBST();

            for (int element = 1; element < SIZE; element++) {
                redBlackBST.insert(new RBNode(items[element]));
            }

            for (int element = 1; element < SIZE; element++) {
                redBlackBST.delete(new RBNode(items[element]));
            }

            System.gc();
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Deletes the array of generated items.
     *
     * @throws TaskFailedException Thrown if the clean up failed.
     */
    @Override
    public void cleanupTask() throws TaskFailedException {
        items = null;
    }
}
