package cz.benchmarktools.trees.tasks;

/**
 * Defines the available tasks that the benchmark can execute.
 */
public class Task {
    /**
     * Runs AVL tree algorithm.
     */
    public static final String AVL = "avl";

    /**
     * Runs RB tree algorithm.
     */
    public static final String RB = "rb";

}
