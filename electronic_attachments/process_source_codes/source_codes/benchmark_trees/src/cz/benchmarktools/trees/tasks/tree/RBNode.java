package cz.benchmarktools.trees.tasks.tree;

/**
 * Represents a node for a Red-Black tree. The source code is taken from <a href=https://github.com/TheAlgorithms/Java/blob/master/DataStructures/Trees/RedBlackBST.java>TheAlgorithms GitHub page</a>
 * and might be slightly modified to meet our requirements.
 */
public class RBNode {

    private final int R = 0;
    private final int B = 1;

    private static RBNode nil;

    int key;
    int color;
    RBNode left;
    RBNode right;
    RBNode p;

    public RBNode(int key) {
        this.key = key;
        this.color = B;
        this.left = nil;
        this.right = nil;
        this.p = nil;
    }

    static RBNode getNilNode() {
        if (nil == null) {
            nil = new RBNode(-1);
        }
        return nil;
    }
}
