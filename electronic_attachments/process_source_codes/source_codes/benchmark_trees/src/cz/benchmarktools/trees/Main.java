package cz.benchmarktools.trees;

import cz.benchmarktools.trees.tasks.AVLTask;
import cz.benchmarktools.trees.tasks.AbstractTask;
import cz.benchmarktools.trees.tasks.RBTask;
import cz.benchmarktools.trees.tasks.Task;
import cz.benchmarktools.trees.tasks.exception.TaskFailedException;

/**
 * Enables to execute the test microservices without attaching them to the measuring framework.
 */
public class Main {
    /**
     * The entry point, that enables to select from the available microservices.
     *
     * @param args Use 'avl' as the command line argument to run the AVL tree, otherwise use 'rb' to run
     *             the Red-Black tree microservice.
     */
    public static void main(String[] args) {
        AbstractTask action = null;

        if (args.length != 1) {
            System.out.println("Unknown task. Use either 'avl' or 'rb'. The application terminates.");
            System.exit(1);
        }

        switch (args[0]) {
            case Task.AVL:
                action = new AVLTask();
                break;
            case Task.RB:
                action = new RBTask(args);
                break;
            default:
                System.out.println("Unknown task. Use either 'avl' or 'rb'. The application terminates.");
                System.exit(-1);
        }

        // INITIALIZE
        System.out.println("Preparing the task started.");
        try {
            action.prepareTask();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Preparing the task finished.");

        // ITERATE AND MEASURE
        System.out.println("Performing the task started.");
        for (int i = 0; i < 15; i++) {
            long start = System.currentTimeMillis();

            try {
                action.performTask();
            } catch (TaskFailedException e) {
                e.printStackTrace();
            }

            long time = System.currentTimeMillis() - start;
            System.out.println("done " + i + " time: " + time);
        }
        System.out.println("Performing the task finished.");

        // CLEANUP
        System.out.println("Cleaning up the task started.");
        try {
            action.cleanupTask();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
        System.out.println("Cleaning up the task finished.");
    }
}
