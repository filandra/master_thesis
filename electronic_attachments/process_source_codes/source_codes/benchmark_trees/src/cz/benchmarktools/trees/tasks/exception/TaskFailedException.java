package cz.benchmarktools.trees.tasks.exception;

import org.pmw.tinylog.Logger;

/**
 * Signals that the task execution has failed. The execution should be stopped, and the failure should be reported to
 * the agent.
 */
public class TaskFailedException extends Exception {
    /**
     * Creates a new instance containing the cause of the exception.
     *
     * @param cause The cause containing detailed information about the issue.
     */
    public TaskFailedException(Throwable cause) {
        super(cause);
        Logger.error(cause);
    }

    /**
     * Creates a new instance containing the message and the cause of the exception.
     *
     * @param message The error message describing the issue.
     * @param cause   The cause containing detailed information about the issue.
     */
    public TaskFailedException(String message, Throwable cause) {
        super(message, cause);
        Logger.error(cause, message);
    }
}
