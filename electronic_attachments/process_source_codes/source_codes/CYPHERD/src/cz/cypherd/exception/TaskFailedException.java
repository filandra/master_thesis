package cz.cypherd.exception;

import org.pmw.tinylog.Logger;

/**
 * Signals that the task execution has failed. The execution should be stopped, and the failure should be reported to
 * the agent.
 */
public class TaskFailedException extends Exception {
    /**
     * Creates a new instance containing the cause of the exception.
     *
     * @param cause The cause containing detailed information about the issue.
     */
    public TaskFailedException(Throwable cause) {
        super(cause);
        Logger.error(cause, "CYPHERD failed.");
    }

    /**
     * Creates a new instance containing the message of the exception.
     *
     * @param message The message containing detailed information about the issue.
     */
    public TaskFailedException(String message) {
        super(message);
        Logger.error(message);
    }
}
