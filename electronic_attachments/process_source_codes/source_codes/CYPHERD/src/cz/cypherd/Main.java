package cz.cypherd;

import cz.cypherd.exception.TaskFailedException;
import logic.CYPHERD;

/**
 * The main class used for testing purposes only.
 */
public class Main {
    public static void main(String[] args) {
        CYPHERD uls = new CYPHERD();
        uls.init();

        try {
            for (int i = 0; i < 1; i++) {
                uls.run();
            }
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
    }
}
