package logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;


public class SORTD {
    private Random rand = new Random(2);
    private String directory = "data";
    private String extension = ".out";
    private BubbleSort bubbleSort = new BubbleSort();
    private MergeSort mergeSort = new MergeSort();
    private QuickSort quickSort = new QuickSort();

    public void init() {
        File outDir = new File(directory);
        if (!outDir.exists()) {
            outDir.mkdirs();
        }
    }

    public void run() {
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < 3000; i++) {
            save(sort(), i);
        }

        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(estimatedTime);
    }

    private void save(Integer[] array, int number) {
        try (FileOutputStream fos = new FileOutputStream(directory + File.separatorChar + number + extension)) {
            byte[] byteArray = Arrays.toString(array).getBytes();
            fos.write(byteArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteRecursively(File file) throws IOException {
        if (file.isDirectory()) {
            File[] entries = file.listFiles();
            if (entries != null) {
                for (File entry : entries) {
                    deleteRecursively(entry);
                }
            }
        }
        if (!file.delete()) {
            throw new IOException("Failed to delete " + file);
        }
    }

    public void cleanup() throws IOException {
        File file = new File(directory);
        deleteRecursively(file);
    }

    private Integer[] sort() {
        // we statically set the sort algorithm to QuickSort: the fastest algorithms; we will constantly use the same algorithm
        Integer[] array = generate_input();

//        int random = rand.nextInt(3);
        int random = 2;

        switch (random) {
            case 0:
                bubbleSort.sort(array);
                break;
            case 1:
                mergeSort.sort(array);
                break;
            case 2:
                quickSort.sort(array);
                break;
        }
        return array;
    }

    private Integer[] generate_input() {
        int max = 9600;
        int min = 8600;

        Integer[] array = new Integer[max];

        for (int i = 0; i < max; i++) {
            array[i] = rand.nextInt();
        }

        return array;
    }
}
