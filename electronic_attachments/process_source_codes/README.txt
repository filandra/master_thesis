Processes
---------
This directory contains the source codes and JARs of processes, which were measured to produce data matrices for the predictor.

benchmark_scalabench:  
	Used for A, F, H, K and O process

benchmark_trees:  
	Used for AVL and RB processes

benchmark_zt-zip:  
	Used for ZB process

benchmark_facedetection:  
	Used for FACE process

benchmark_graphs:  
	Used for FLODY process

benchmark\_dynamic_programming:  
	Used for ROD and EGG

benchmark_command:  
	Used for SMATRIX process

CYPHERD, JSOND, PDFD, SORTD:  
	Used for their respective process

The processes CYPHERD, JSOND, PDFD and SORTD can be executed right away.  
The instructions on how to excute the rest of the processes can be found in the code comments.  
Other processes require input, usually in the form of a string to select the desired process and path to source files or output directory.  

Instructions to run the SMATRIX process can be found here:

https://kernel.ubuntu.com/~cking/stress-ng/?fbclid=IwAR34ozcjnfwl3dhnPMtR9mHTi6b8Ucro9p7hbHVQ7dTy6VxMxlnN1SUXU4Q

http://manpages.ubuntu.com/manpages/bionic/man1/stress-ng.1.html?fbclid=IwAR2JNARWvC2Ay26ozg36pPJUyIlMNRD09xANxZ1_7dkYqYctyzDRl0TTXi8

