package Logic;

import Blocks.Block;
import Planners.Planner;
import com.google.gson.Gson;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.lang.System.exit;

/**
 * The main class of the project
 * Loads the configuration file and executes the specified plan
 */
public class ULS {

    private Config config;
    private Plan plan;
    private boolean runnable = true;

    /**
     * The constructor first loads the configuration file from disk and checks the validity of the configuration.
     * Based on the configuration a plan is constructed using a specified Planner.
     *
     * @param args ULS expects a single string with the config file path as an input
     */
    public ULS(String[] args) {
        config = parseInput(args);
        checkConfig(config);
        Planner planner = config.distribution.getPlanner();
        plan = planner.generatePlan(config);
    }

    /**
     * Goes through the specified sequence of blocks in a plan and executes them.
     *
     * @param plan The plan containing a sequence of Blocks
     */
    private static void executePlan(Plan plan) {
        plan.init();
        for (Block item : plan.blocks) {
            item.runBlock();
        }
        plan.finish();
    }

    /**
     * The method, which starts the execution of plan.
     * Time is measured only to check, if the program is doing something.
     * it is not intended as official measurement of length of calculation - that measurement is handled by external
     * framework.
     */
    public void run() {
        long startTime = System.currentTimeMillis();

        if (runnable) {
            executePlan(plan);
        }

        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(estimatedTime);
    }

    /**
     * Parses the JSON input file into the Config class.
     *
     * @param args ULS expects a single string with the config file path as an input
     * @return The parsed Config class
     */
    private Config parseInput(String[] args) {
        try {
            String jsonConfig = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);
            Gson gson = new Gson();

            return gson.fromJson(jsonConfig, Config.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Checks if the configuration is specified correctly.
     * The combined percentage of resource usage can not exceed 100%.
     * If the combined resource usage does not meet 100%, the rest is fileed with IdleBlocks.
     *
     * @param config The configuration class
     */
    private void checkConfig(Config config) {

        if (config == null) {
            System.out.println("Config error");
            runnable = false;
            exit(0);
        }

        float total_percentage = config.cpu_percentage + config.mem_percentage + config.disk_percentage + config.idle_percentage;

        if (total_percentage > 100) {
            System.out.println("Specified percentages in config file is greater than 100%");
            runnable = false;
            exit(0);
        }

        if (total_percentage < 100) {
            config.idle_percentage += 100 - total_percentage;
        }
    }
}
