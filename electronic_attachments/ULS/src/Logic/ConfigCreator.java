package Logic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class used to create the default set of configurations
 */
public class ConfigCreator {

    private final static String cpuName = "C";
    private final static String diskName = "D";
    private final static String memName = "M";
    private final static String cpuDiskName = "CD";
    private final static String cpuMemName = "CM";
    private final static String diskMemName = "DM";
    private final static String cpuDiskMemName = "CDM";
    private final static String mainDir = "Configs/";
    private final static String extension = ".config";

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) {
        createDirectory(mainDir);
        createConfigs();
    }

    /**
     * Creates the needed directories and iterates through each intenstiy level (from 5 to 100 with 5% steps).
     * Creates a configuration file for each type of resource use.
     */
    private static void createConfigs() {
        createDirectory(mainDir + cpuName);
        createDirectory(mainDir + diskName);
        createDirectory(mainDir + memName);

        createDirectory(mainDir + cpuDiskName);
        createDirectory(mainDir + cpuMemName);
        createDirectory(mainDir + diskMemName);

        createDirectory(mainDir + cpuDiskMemName);

        for (int i = 5; i <= 100; i += 5) {
            configure(cpuName, i);
            configure(diskName, i);
            configure(memName, i);

            configure(cpuDiskName, i);
            configure(cpuMemName, i);
            configure(diskMemName, i);

            configure(cpuDiskMemName, i);
        }
    }

    /**
     * Creates a single configuration
     *
     * @param name      Name of the configuration
     * @param intensity The integrity percentage
     */
    private static void configure(String name, int intensity) {
        Config config = new Config();

        float half = (float) intensity / 2;
        // Round and truncate the intensity
        half = (float) Math.floor(half * 100) / 100;

        float third = (float) intensity / 3;
        // Round and truncate the intensity
        third = (float) Math.floor(third * 100) / 100;

        switch (name) {
            case cpuName:
                config.cpu_percentage = intensity;
                break;
            case diskName:
                config.disk_percentage = intensity;
                break;
            case memName:
                config.mem_percentage = intensity;
                break;
            case cpuDiskName:
                config.cpu_percentage = half;
                config.disk_percentage = half;
                break;
            case cpuMemName:
                config.cpu_percentage = half;
                config.mem_percentage = half;
                break;
            case diskMemName:
                config.disk_percentage = half;
                config.mem_percentage = half;
                break;
            case cpuDiskMemName:
                config.cpu_percentage = third;
                config.disk_percentage = third;
                config.mem_percentage = third;
                break;
            default:
        }
        save(config, name, intensity);
    }

    /**
     * Saves the configuration to prepared directories
     *
     * @param config    The Config class to save
     * @param name      Name of the configuration
     * @param intensity The level
     */
    private static void save(Config config, String name, int intensity) {
        String configText = gson.toJson(config);
        File outputFile = new File(mainDir + name + "/" + name + intensity + extension);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
            writer.write(configText);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates the specified directory
     *
     * @param name Directory name
     */
    private static void createDirectory(String name) {
        File directory = new File(name);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }
}
