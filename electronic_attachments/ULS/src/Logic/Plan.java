package Logic;

import Blocks.*;

/**
 * The Plan class contains all information related to the specified plan
 */
public class Plan {
    public CpuBlock cpuBlock;
    public MemBlock memBlock;
    public DiskBlock diskBlock;
    public IdleBlock idleBlock;

    public Block[] blocks;
    public int total_blocks;

    /**
     * Receives the configuration and creates an instance of Block for each type of Block.
     * These instances of Blocks are then referenced from the sequence of Blocks.
     *
     * @param config The configuration specified by the user
     */
    public Plan(Config config) {
        int cpu_block_num = (int) ((config.cpu_percentage / 100) * config.block_num);
        int mem_block_num = (int) ((config.mem_percentage / 100) * config.block_num);
        int disk_block_num = (int) ((config.disk_percentage / 100) * config.block_num);
        int idle_block_num = (int) ((config.idle_percentage / 100) * config.block_num);

        cpuBlock = new CpuBlock(cpu_block_num, config.seed, config.cpu_calculations);
        memBlock = new MemBlock(mem_block_num, config.seed, config.mem_used_bytes);
        diskBlock = new DiskBlock(disk_block_num, config.seed, config.bytes_per_diskBlock);
        idleBlock = new IdleBlock(idle_block_num, config.seed, config.idle_ms);

        total_blocks = cpuBlock.blockNum + memBlock.blockNum + diskBlock.blockNum + idleBlock.blockNum;
        blocks = new Block[total_blocks];
    }

    /**
     * For
     *
     * @param config The configuration specified by the user
     */
    public void fillBlockNum(Config config) {
        cpuBlock.blockNum = (int) ((config.cpu_percentage / 100) * config.block_num);
        memBlock.blockNum = (int) ((config.mem_percentage / 100) * config.block_num);
        diskBlock.blockNum = (int) ((config.disk_percentage / 100) * config.block_num);
        idleBlock.blockNum = (int) ((config.idle_percentage / 100) * config.block_num);
    }

    /**
     * Initialize all of the Blocks in the Plan
     */
    void init() {
        cpuBlock.init();
        memBlock.init();
        diskBlock.init();
        idleBlock.init();
    }

    /**
     * Close all of the Blocks in the Plan
     */
    void finish() {
        cpuBlock.finish();
        memBlock.finish();
        diskBlock.finish();
        idleBlock.finish();
    }
}
