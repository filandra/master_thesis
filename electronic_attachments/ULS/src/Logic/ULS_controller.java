package Logic;

/**
 * Just a controller for easy ULS demonstration
 */
public class ULS_controller {

    public static void main(String[] args) {
        ULS uls = new ULS(args);
        uls.run();
    }
}
