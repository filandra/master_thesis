package Logic;

import Planners.MixPlanner;
import Planners.Planner;
import Planners.RandomPlanner;
import com.google.gson.annotations.SerializedName;

/**
 * Class which holds all of the information about the configuration
 */
public class Config {
    /**
     * Number of blocks, which will be executed
     */
    public int block_num = 1000;

    public float cpu_percentage = 0;
    public float mem_percentage = 0;
    public float disk_percentage = 0;
    public float idle_percentage = 0;

    /**
     * Number of calculations in a CpuBlock
     */
    public int cpu_calculations = 100000;
    /**
     * Number of memory bytes used in the MemBlock calculation
     */
    public int mem_used_bytes = 40000000;
    /**
     * Number of bytes written in each DiskBlock
     */
    public int bytes_per_diskBlock = 1000000;
    /**
     * How long should the IdleBlock sleep
     */
    public int idle_ms = 8;

    public Distribution distribution = Distribution.Mix;

    public int seed = 0;

    /**
     * Used to extract the planner from the name of the planner in the configuration file
     */
    public enum Distribution {
        @SerializedName("Random") Random {
            @Override
            public Planner getPlanner() {
                return new RandomPlanner();
            }
        },
        @SerializedName("Mix") Mix {
            @Override
            public Planner getPlanner() {
                return new MixPlanner();
            }
        };

        abstract public Planner getPlanner();
    }
}
