package Blocks;

import java.util.concurrent.TimeUnit;

/**
 * Block, only waits for certain amount of time
 */
public class IdleBlock extends Block {

    private int waitTime = 0;

    public IdleBlock(int blockNum, int seed, int waitTime) {
        super(blockNum, seed);
        this.waitTime = waitTime;
    }

    /**
     * Sleep for a specified amount of miliseconds
     */
    @Override
    public void runBlock() {
        try {
            TimeUnit.MILLISECONDS.sleep(waitTime);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}