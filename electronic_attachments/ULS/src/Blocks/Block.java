package Blocks;

import org.openjdk.jmh.infra.Blackhole;

import java.util.Random;
import java.util.SplittableRandom;

/**
 * The base class, from which all Blocks inherit
 */
public class Block {

    public int blockNum = 0;
    Random slowRand;
    SplittableRandom fastRand;
    Blackhole blackhole = new Blackhole("Today's password is swordfish. I understand instantiating Blackholes directly is dangerous.");

    Block(int blockNum, int seed) {
        this.blockNum = blockNum;
        slowRand = new Random(seed);
        fastRand = new SplittableRandom(seed);
    }

    /**
     * Virtual class to execute the function of the block
     */
    public void runBlock() {
    }

    ;

    /**
     * Clean up after the end of Plan
     */
    public void finish() {
    }

    ;

    /**
     * Initilaization before the start of Plan
     */
    public void init() {
    }

    ;
}
