package Blocks;

/**
 * Block, which focuses on the use memory
 */
public class MemBlock extends Block {

    private int[][] mainArray;
    private int columns;
    private int rows = 200;
    private int usedBytesIntNum = 0;

    private int recursNum = 50;
    private int recursReduction = 190;
    private int memoryPlayIterations = 600;
    private int memoryReallocIterations = 2;

    public MemBlock(int blockNum, int seed, int mem_used_bytes) {
        super(blockNum, seed);
        usedBytesIntNum = mem_used_bytes / 4;
        columns = usedBytesIntNum / rows;
        mainArray = new int[rows][columns];
    }

    /**
     * For a specified number of iterations, randomly either user reucrsion, assign and reassign data in the memory
     * or reallocate some memory.
     */
    @Override
    public void runBlock() {
        int counter = 0;
        for (int i = 1; i < rows; i++) {

            switch (counter) {
                case 0:
                    recursion(0);
                    counter++;
                    break;
                case 1:
                    memoryPlay();
                    counter++;
                    break;
                case 2:
                    memoryRealloc();
                    counter = 0;
                    break;
            }
        }
    }

    /**
     * Recursively add new data on the stack
     *
     * @param depth Limits the depth of the recursion
     */
    private void recursion(int depth) {
        int[] recursiveArray = new int[usedBytesIntNum / (recursNum * recursReduction)];

        int content = 0;
        for (int i = 0; i < recursiveArray.length; i++) {
            recursiveArray[i] = content;
            content++;
        }

        if (depth++ < recursNum) {
            depth = depth + 1;
            recursion(depth);
        }
    }

    /**
     * For a specified number of iterations, select two random coordinates in a matrix a switch their contents
     */
    private void memoryPlay() {
        int fromRow = fastRand.nextInt(rows);
        int fromColumn = fastRand.nextInt(columns);
        int toRow = fastRand.nextInt(rows);
        int toColumn = fastRand.nextInt(columns);
        int fromArray = 0;

        for (int i = 0; i < memoryPlayIterations; i++) {
            fromArray = mainArray[fromRow][fromColumn];
            mainArray[fromRow][fromColumn] = mainArray[toRow][toColumn];
            mainArray[toRow][toColumn] = fromArray;

            fromRow = fastRand.nextInt(rows);
            fromColumn = fastRand.nextInt(columns);
            toRow = fastRand.nextInt(rows);
            toColumn = fastRand.nextInt(columns);
        }
    }

    /**
     * Reallocate parts of the matrix
     */
    private void memoryRealloc() {
        for (int i = 0; i < memoryReallocIterations; i++) {
            int toRow = fastRand.nextInt(rows);
            mainArray[toRow] = new int[columns];
            int content = 0;
            for (int j = 0; j < columns; j++) {
                mainArray[toRow][j] = content;
                content++;
            }
        }
    }
}

