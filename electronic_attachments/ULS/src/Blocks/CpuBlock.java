package Blocks;

/**
 * Block, which focuses on the use of CPU
 */
public class CpuBlock extends Block {

    private int cpu_calculations = 0;

    public CpuBlock(int blockNum, int seed, int cpu_calculations) {
        super(blockNum, seed);
        this.cpu_calculations = cpu_calculations;
    }

    /**
     * Calculate a random mathematical function and let it be consumed by blackhole for a specified number of times.
     * The blackhole is necessary, because without it the JVM could omit the calculation, as the results would not be
     * used.
     */
    @Override
    public void runBlock() {
        for (int i = 0; i < cpu_calculations; i++) {
            int rand_num = slowRand.nextInt(100);

            if ((rand_num % 2) == 0) {
                switch (rand_num % 4) {
                    case 0:
                        blackhole.consume(Math.sin(complication(rand_num)));
                        break;
                    case 1:
                        blackhole.consume(Math.acos(complication(rand_num)));
                        break;
                    case 2:
                        blackhole.consume(Math.tan(complication(rand_num)));
                        break;
                    case 3:
                        blackhole.consume(Math.atan(complication(rand_num)));
                        break;
                }
            } else {
                switch (rand_num % 4) {
                    case 0:
                        blackhole.consume(Math.log(complication(rand_num)));
                        break;
                    case 1:
                        blackhole.consume(Math.exp(complication(rand_num)));
                        break;
                    case 2:
                        blackhole.consume(Math.sqrt(complication(rand_num)));
                        break;
                    case 3:
                        blackhole.consume(Math.log10(complication(rand_num)));
                        break;
                }
            }
        }
    }

    /**
     * Used to add some more calculations
     *
     * @param number Input on which to operate
     * @return The resulting number
     */
    private int complication(int number) {
        int r1 = fastRand.nextInt(43);
        int r2 = fastRand.nextInt(13);
        return (number + r1) * (number - r2);
    }
}
