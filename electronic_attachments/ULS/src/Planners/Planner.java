package Planners;

import Logic.Config;
import Logic.Plan;

/**
 * The base class, from which the individual Planners inherit.
 */
public abstract class Planner {

    public abstract Plan generatePlan(Config config);
}
