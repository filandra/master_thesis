package Planners;

import Logic.Config;
import Logic.Plan;

/**
 * Planner, which mixes the Blocks perfetly in the Block sequence
 */
public class MixPlanner extends Planner {

    /**
     * Mixes all blocks perfetly.
     * For example switches between one CpuBlock and one IdleBlock, if the configuration was 50%/50% CpuBlock/IdleBlock.
     * @param config The specified configuration of the Plan.
     * @return Plan with perfectly mixed blocks.
     */
    @Override
    public Plan generatePlan(Config config) {
        Plan plan = new Plan(config);

        int counter = 0;

        double cpuRatio = config.cpu_percentage / 100;
        double diskRatio = config.disk_percentage / 100;
        double memRatio = config.mem_percentage / 100;
        double idleRatio = config.idle_percentage / 100;

        double cpuBank = 0;
        double diskBank = 0;
        double memBank = 0;
        double idleBank = 0;

        // Ratio is always <= 1, thus this cycle will create perfectly even distribution
        while (counter != plan.total_blocks) {
            cpuBank += cpuRatio;
            diskBank += diskRatio;
            memBank += memRatio;
            idleBank += idleRatio;

            if (diskBank >= 1 && counter != plan.total_blocks) {
                plan.blocks[counter] = plan.diskBlock;
                diskBank--;
                counter++;
            }

            if (cpuBank >= 1 && counter != plan.total_blocks) {
                plan.blocks[counter] = plan.cpuBlock;
                cpuBank--;
                counter++;
            }

            if (memBank >= 1 && counter != plan.total_blocks) {
                plan.blocks[counter] = plan.memBlock;
                memBank--;
                counter++;
            }

            if (idleBank >= 1 && counter != plan.total_blocks) {
                plan.blocks[counter] = plan.idleBlock;
                idleBank--;
                counter++;
            }
        }

        return plan;
    }
}
