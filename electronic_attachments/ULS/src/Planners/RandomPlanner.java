package Planners;

import Blocks.*;
import Logic.Config;
import Logic.Plan;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Planner, which mixes the Blocks randomly in the Block sequence.
 */
public class RandomPlanner extends Planner {

    /**
     * Randomly distributes the Blocks in the Block sequence.
     * @param config The specified configuration of the Plan.
     * @return Plan with randomly mixed blocks.
     */
    @Override
    public Plan generatePlan(Config config) {
        Plan plan = new Plan(config);

        List<Block> blockTypes = new LinkedList<Block>();
        if (plan.cpuBlock.blockNum != 0) {
            blockTypes.add(plan.cpuBlock);
        }
        if (plan.memBlock.blockNum != 0) {
            blockTypes.add(plan.memBlock);
        }
        if (plan.diskBlock.blockNum != 0) {
            blockTypes.add(plan.diskBlock);
        }
        if (plan.idleBlock.blockNum != 0) {
            blockTypes.add(plan.idleBlock);
        }

        Random rand = new Random(config.seed);
        int counter = 0;

        while (!blockTypes.isEmpty()) {
            int n = rand.nextInt(blockTypes.size());

            if (blockTypes.get(n).blockNum != 0) {
                plan.blocks[counter] = blockTypes.get(n);
                blockTypes.get(n).blockNum--;
                counter++;
            } else {
                blockTypes.remove(n);
            }
        }

        plan.fillBlockNum(config);
        return plan;
    }
}
