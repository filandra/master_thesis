package cz.uls;

import Logic.ULS;
import cz.sandor.dipl.benchmark.BenchmarkResult;
import cz.sandor.dipl.benchmark.IBenchmarkIterationController;
import cz.sandor.dipl.benchmark.MeasurableBenchmark;

/**
 * Class, which forms a benchmark that can be run in the software used to measure data matrices
 */
public class Benchmark implements MeasurableBenchmark {

    private ULS uls = null;
    private IBenchmarkIterationController benchmarkIterationController;

    /**
     * Prepare the ULS with the configuration file
     * @param strings Path to the configuration file
     * @return Benchamrk initialized
     */
    @Override
    public BenchmarkResult init(String[] strings) {
        uls = new ULS(strings);
        return BenchmarkResult.INITED;
    }

    /**
     * Execute one run of the ULS
     * @return Either benchmark interrupted or finished
     * @throws Exception
     */
    @Override
    public BenchmarkResult runAndMeasure() throws Exception {
        while (benchmarkIterationController.canRunAgain()) {
            benchmarkIterationController.iterationStarted();
            uls.run();
            benchmarkIterationController.iterationStopped();
            benchmarkIterationController.iterationCompleted();
        }

        if (benchmarkIterationController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.RUN_FINISHED;
        }
    }

    @Override
    public void setIterationController(IBenchmarkIterationController iBenchmarkIterationController) {
        this.benchmarkIterationController = iBenchmarkIterationController;
    }
}
