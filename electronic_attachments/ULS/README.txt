Universal Load Simulator
========================

The ULS is written in Java in order to be compatible with the software used to measure the data matrices.

The directory "src" contains the source code of the ULS together.
The directory "Configs" configuration files, which the ULS take as an input

Javadoc can be generated from the source code. The documentation is not pre-generated due to the restrictions on file extensions, when submiting electronic attachements to SIS.

Setup
=====

In order to run the ULS, the user has to provide one argument: the path to the configuration file.
Examples of the configuration files can be found in the Configs directory


Input file formats:
===================
The configuration file is a standard JSON file containing a dictionary, where the user can specify:

"block_num": Number of executed blocks
"cpu_percentage": Percentage of blocks, which focus on the CPU
"mem_percentage": Percentage of blocks, which focus on the memory
"disk_percentage": Percentage of blocks, which focus on the disk
"idle_percentage": Percentage of blocks, which only sleep
"cpu_calculations": The length of one CPU block
"mem_used_bytes": The size of the memory used during MemBlock (also affects the length of execution time)
"bytes_per_diskBlock": The number of bytes written on disk in each disk block (also affects the length of execution time)
"idle_ms": Number of milliseconds, for which the IdleBlock sleep
"distribution": "Mix" (perfect distribution of blocks - e.g., switches between one CpuBlock and one IdleBlock, if the configuration was 50%/50%) or "Random" (just randomly places Blocks in the Plan)
"seed": Seed used for the randomizer
