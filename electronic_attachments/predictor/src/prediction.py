"""
This module contains classes, which implement the calculation of prediction methods used in prediction calculation

Classes:
	Prediction: Base class, which is inherited by classes implementing the prediction methods
	Baseline: Class implementing the baseline prediction method
	ClosestFriend: Class implementing the closest-friend prediction method
	PrimaryCluster: Class implementing the primary-cluster prediction method
	ClusterCluster: Class implementing the cluster cluster prediction method
	PolynomialRegression: Class implementing the polynomial regression method
"""

import collections
import itertools
import logging
import os

import numpy as np
from sklearn.preprocessing import PolynomialFeatures

import paths
import utilities as util
from record import Record
from regression_analyser import RegressionAnalyser

logger = logging.getLogger("prediction")


class Prediction:
	"""Base class, which is inherited by classes implementing the prediction methods

	Subclasses of Prediction are expected to generate a prediction from provided data.
	New prediction methods can be easily added by inheriting from Prediction and overriding provided methods.

	Attributes:
		name (str): name of the distance measure
		_node (str): On which nodetype is the prediction method executed
		_perc (float): For which percentile is the prediction method executed
		_single_records (dict[str, record.Record]): Mapping of single process Records to their ID
		_comb_records (dict[str, record.Record]): Mapping of process combination Records to their ID
	"""

	def __init__(self, nodetype, percentile):
		self.name = ""
		self._node = nodetype
		self._perc = float(percentile)
		self._single_records = None
		self._comb_records = None

	def load_data(self, single_records, comb_records):
		"""A virtual method used to load data for the prediction method

		The subclasses are expected to override this method.

		Args:
			single_records (dict[str, record.Record]): Mapping of single process Records to their ID
			comb_records (dict[str, record.Record]): Mapping of process combination Records to their ID

		Returns:
			None
		"""
		pass

	def get_prediction(self, combination):
		"""A virtual method used to calculate the prediction method

		The subclasses are expected to override this method.

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			float: Value of the prediction
		"""
		return 0


class Baseline(Prediction):
	"""Class implementing the baseline prediction method

	Attributes:
		name (str): name of the prediction method
	"""

	def __init__(self, nodetype, percentile):
		super().__init__(nodetype, percentile)
		self.name = "Baseline"

	def load_data(self, single_records, comb_records):
		"""Loads data needed for to execute the prediction method

		Args:
			single_records (dict[str, record.Record]): Mapping of single process Records to their ID
			comb_records (dict[str, record.Record]): Unused, left for consistency

		Returns:
			None
		"""
		self._single_records = single_records

	def get_prediction(self, combination):
		"""The baseline prediction is the percentile of execution time of the primary process in a combination running
			alone

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			float: Value of the prediction
		"""
		return self._single_records[combination[0]].stats.percentile[util.stats_elapsed_col(self._node)]


class ClosestFriend(Prediction):
	"""Class implementing the closest-friend prediction method

	Attributes:
		name (str): name of the prediction method
		__most_similar_pairs (dict[str, utilities.ClosestId]): Maps ClosestID to process IDs
	"""

	def __init__(self, nodetype, percentile):
		super().__init__(nodetype, percentile)
		self.name = "ClosestFriend"
		self.__most_similar_pairs = None

	def load_data(self, single_records, comb_records):
		"""Loads data needed for to execute the prediction method

		Args:
			single_records (dict[str, record.Record]): Mapping of single process Records to their ID
			comb_records (dict[str, record.Record]): Mapping of process combination Records to their ID

		Returns:
			None
		"""
		self._single_records = single_records
		self._comb_records = comb_records
		self.__most_similar_pairs = self.__load_most_similar_pairs()

	def get_prediction(self, combination):
		"""The closest-friend prediction method predicts the percentile of execution time of a primary process using the
			percentile of execution time of the closest measured combination.

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			float: Value of the prediction
		"""
		logger.info("Calculating closest friend prediction")

		if len(combination) == 1:
			process_id = combination[0]
			if process_id not in self._single_records:
				return None
			else:
				return self._single_records[process_id].stats.percentile[util.stats_elapsed_col(self._node)]

		sorted_alternative_combinations = self.__get_sorted_alternative_combinations(combination)
		logger.info("Alternative combinations: " + str(sorted_alternative_combinations))

		result = None
		for alternative in sorted_alternative_combinations:
			alternative_id = "-".join(alternative)
			if alternative_id not in self._comb_records:
				logger.info("Do not have data to predict " + str(alternative) + " combination.")
			else:
				logger.info("Using " + str(alternative) + " for prediction.")

				result = self._comb_records[alternative_id].stats.percentile[
					util.stats_elapsed_col(self._node)]
				logger.info(result)
				break

		return result

	def __get_sorted_alternative_combinations(self, combination):
		"""Creates list of alternative process combinations sorted by their distance from the original combination

		Alternative process combinations consists of the same primary process, however the secondary processes are
		replaced by their most similar counterparts. All variations of secondary processes with either original or
		replaced processes is considered.

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			list of list of str: The sorted list of alternative combinations
		"""
		combination = util.sorted_combination(combination)
		index_combinations = self.__get_index_combinations(combination)
		alternatives_distance = self.__get_alternatives_distance(combination, index_combinations)

		sorted_alternative_combinations = sorted(alternatives_distance, key=alternatives_distance.get)
		sorted_alternative_combinations.insert(0, "-".join(combination))
		sorted_alternative_combinations = [comb.split("-") for comb in sorted_alternative_combinations]

		return sorted_alternative_combinations

	def __get_index_combinations(self, combination):
		"""Creates all combinations of indexes, which indicate what secondary processes in a process combination
		should be replaced by their most similar counterpart.

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			list of list of int: List of index combinations, which are used to create the alternative combinations
		"""
		indexes = range(1, len(combination))
		index_combinations = []

		for i in range(len(indexes)):
			index_combinations.extend(itertools.combinations(indexes, i + 1))

		return index_combinations

	def __get_alternatives_distance(self, combination, index_combinations):
		"""Calculates the distance of the alternative combination from the original one for each alternative combination

		Args:
			combination (list of str): List of processes in the combination
			index_combinations (list of list of int): List of index combinations, which are used to create the
				alternative combinations

		Returns:
			dict[str, float]: Alternative combination IDs mapped to their distance from the original combination
		"""
		all_alternatives = collections.OrderedDict()

		logger.info(index_combinations)

		for indexes in index_combinations:
			distance = 0
			new_combination = list(combination)
			for index in indexes:
				most_similar_process = self.__most_similar_pairs[combination[index]].closest_id
				new_combination[index] = most_similar_process
				distance += self.__most_similar_pairs[combination[index]].distance
			new_sorted_combination = util.sorted_combination(new_combination)
			all_alternatives["-".join(new_sorted_combination)] = distance

		return all_alternatives

	def __load_most_similar_pairs(self):
		"""Loads the most similar pairs information and converts it from JSON to utilities.ClosestId

		Returns:
			utilities.ClosestId: Information about most similar pairs
		"""
		return util.decode_dict(util.load_json(paths.MOST_SIMILAR(self._node, self._perc)), util.ClosestId)


class PrimaryCluster(Prediction):
	"""Class implementing the primary-cluster prediction method

	Attributes:
		name (str): name of the prediction method
		__primary_cluster_records (dict[str, record.Record]): Mapping of primary process IDs to their Records
		__id_to_cluster (dict[str, int]): Mapping of process IDs to their cluster number
	"""

	def __init__(self, nodetype, percentile):
		super().__init__(nodetype, percentile)
		self.name = "PrimaryCluster"
		self.__primary_cluster_records = None
		self.__id_to_cluster = None

	def load_data(self, single_records, comb_records):
		"""Loads data needed for to execute the prediction method

		Args:
			single_records (dict[str, record.Record]): Mapping of single process Records to their ID
			comb_records (dict[str, record.Record]): Mapping of process combination Records to their ID

		Returns:
			None
		"""
		self._single_records = single_records
		self._comb_records = comb_records
		self.__primary_cluster_records = Record.load_clusters_stats(self._node, self._perc, primary=True)
		self.__id_to_cluster = util.load_json(paths.ID_TO_CLUSTER(self._node, self._perc))

	def get_prediction(self, combination):
		"""The primary-cluster prediction method predicts the percentile of execution time of a primary process using
			the average slowdown of percentile of execution time inflicted by processes in secondary clusters

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			float: Value of the prediction
		"""
		logger.info("Calculating primary cluster prediction")
		if len(combination) < 2:
			logger.info("Insufficient length of question")
			return None

		primary_cluster_id = Record.get_cluster_id(combination, self.__id_to_cluster, primary=True)
		if primary_cluster_id not in self.__primary_cluster_records:
			logger.info("Do not have data to predict " + str(primary_cluster_id) + " cluster.")
			return None

		# Mean percentual change of percentile
		percentile_change = self.__primary_cluster_records[primary_cluster_id].stats.mean
		single_percentile = self._single_records[combination[0]].stats.percentile[
			util.stats_elapsed_col(self._node)]
		result = single_percentile + single_percentile * percentile_change
		logger.info(result)

		return result


class ClusterCluster(Prediction):
	"""Class implementing the cluster-cluster prediction method

	Attributes:
		name (str): name of the prediction method
		__cluster_cluster_records (dict[str, record.Record]): Mapping of process combination IDs to their Records
		__id_to_cluster (dict[str, int]): Mapping of process IDs to their cluster number
	"""

	def __init__(self, nodetype, percentile):
		super().__init__(nodetype, percentile)
		self.name = "ClusterCluster"
		self.__cluster_cluster_records = None
		self.__id_to_cluster = None

	def load_data(self, single_records, comb_records):
		"""Loads data needed for to execute the prediction method

		Args:
			single_records (dict[str, record.Record]): Mapping of single process Records to their ID
			comb_records (dict[str, record.Record]): Mapping of process combination Records to their ID

		Returns:
			None
		"""
		self._single_records = single_records
		self._comb_records = comb_records
		self.__cluster_cluster_records = Record.load_clusters_stats(self._node, self._perc, primary=False)
		self.__id_to_cluster = util.load_json(paths.ID_TO_CLUSTER(self._node, self._perc))

	def get_prediction(self, combination):
		"""The cluster-cluster prediction method predicts the percentile of execution time of a primary process using
			the average slowdown of percentile of execution time inflicted by processes in secondary clusters unto the
			primary cluster

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			float: Value of the prediction
		"""
		logger.info("Calculating cluster prediction")
		if len(combination) < 2:
			logger.info("Insufficient length of question")
			return None

		cluster_id = Record.get_cluster_id(combination, self.__id_to_cluster, primary=False)
		if cluster_id not in self.__cluster_cluster_records:
			logger.info("Do not have data to predict " + str(cluster_id) + " cluster.")
			return None

		# Mean percentual change of percentile
		percentile_change = self.__cluster_cluster_records[cluster_id].stats.mean
		cluster_percentile = self._single_records[combination[0]].stats.percentile[
			util.stats_elapsed_col(self._node)]
		result = cluster_percentile + cluster_percentile * percentile_change
		logger.info(result)

		return result


class PolynomialRegression(Prediction):
	"""Class implementing the polynomial-regression prediction method

	Attributes:
		name (str): name of the prediction method
		__reg_models (dict[str, list of float]): Mapping of model coefficients to primary process IDs
		__id_index_mapping dict[str, int]): Mapping between the process ID and the index in row in the first degree
		version of the model matrix
		__poly (sklearn.preprocessing.PolynomialFeatures): sklearn implementation used to transform linear vector
				into polynomial
		__upper_limit (dict[str, float]): Upper limit to the prediction, beyond which the prediction is considered
			unsuccessful
		__lower_limit (dict[str, float]): Lower limit to the prediction, beneath which the prediction is considered
			unsuccessful
		__degree (int): Degree of the polynomial of the regression model
	"""

	def __init__(self, nodetype, percentile, degree):
		super().__init__(nodetype, percentile)
		self.name = "PolynomialReg"
		self.__reg_models = {}
		self.__id_index_mapping = None
		self.__poly = PolynomialFeatures(degree=degree)
		self.__upper_limit = {}
		self.__lower_limit = {}
		self.__degree = degree

	def load_data(self, single_records, comb_records):
		"""Loads data needed for to execute the prediction method

		Args:
			single_records (dict[str, record.Record]): Mapping of single process Records to their ID
			comb_records (dict[str, record.Record]): Unused, left for consistency

		Returns:
			None
		"""
		self._single_records = single_records
		self.__reg_models = {}
		for primary in single_records:
			file = os.path.join(paths.POLYNOMIAL_REG_DIR(self._node, self._perc, self.__degree), primary)
			self.__reg_models[primary] = util.load_json_list(file)

			single_time = self._single_records[primary].stats.percentile[util.stats_elapsed_col(self._node)]
			self.__upper_limit[primary] = single_time * 10
			self.__lower_limit[primary] = single_time

		self.__id_index_mapping = RegressionAnalyser.build_id_index_mapping(self._node, self._perc, single_records)

	def get_prediction(self, combination):
		"""The polynomial regression prediction method predicts the percentile of execution time of a primary
			process using a regression model calculated for each primary process

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			float: Value of the prediction
		"""
		logger.info("Calculating polynomial regression prediction")
		primary = combination[0]
		polynomial_question = self.generate_question(combination)
		result = sum([a * b for a, b in zip(polynomial_question, self.__reg_models[primary])])

		if result < self.__lower_limit[primary] or result > self.__upper_limit[primary]:
			logger.info("Do not have data to predict " + str(combination) + " polynomial regression.")
			return None
		else:
			logger.info(result)
			return result

	def generate_question(self, combination):
		"""Creates question vector for the predicted combination of processes

		The question vector consists of the known values in the regression model, which are then multiplied by the
		calculated coefficients.

		Args:
			combination (list of str): List of processes in the combination

		Returns:
			list of int: The polynomial question vector
		"""
		question = np.zeros(len(self._single_records))
		# Skip the primary
		for id in combination[1:]:
			question[self.__id_index_mapping[id]] += 1

		polynomial_question = self.__poly.fit_transform([question])[0]
		logger.info("Polynomial question:")
		logger.info(polynomial_question)

		return polynomial_question
