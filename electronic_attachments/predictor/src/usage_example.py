"""
This module contains the usage examples of Predictor and Evaluator

The description of the setup and file formats is provided in the README.txt file.
The predictor si already pre-loaded with data matrices for the "nodetype_1". This is the data set used in the thesis.
"""

import logging

import clustering_alg as cl_alg
import clustering_score as cl_score
import distance
import normalizer
import optimizer
import utilities as utils
from evaluator import Evaluator, TestSetup, TestPlan
from predictor import Predictor


def predictor_example():
	"""An example of the Predictor use

	First we create an instance of the Predictor.
	Then we supply the predictor with headers, ground truth and the user boundary.
	An example on how to delete and provide data matrices follows, however the predictor is already pre-loaded with
	data matrices on the "nodetype_1".
	After that, we configure the predictor with certain algorithms and calculate the weights.
	En example on how to issue prediction requests follows.

	Returns:
		None
	"""
	# Just a clean-up of files, in case they got corrupted in other tests
	evaluator = Evaluator()
	evaluator.configure_evaluator(nodetype="nodetype_1", percentile=90, test_setup=None, test_plan=None)
	evaluator.clean_files("headers.json")

	predictor = Predictor(nodetype="nodetype_1", percentile=90)

	predictor.assign_headers("headers.json")
	predictor.assign_groundtruth("groundtruth.json")
	predictor.assign_user_boundary("user_boundary.json")

	# Example on how to remove and provide data matrices
	predictor.delete_combination(["A", "A"])
	predictor.provide_data_matrix("A.csv")
	predictor.provide_data_matrix("A-A.csv")
	print(predictor.get_registered_processes())

	predictor.configure(
		clustering_alg=cl_alg.MeanShift(),
		clustering_score=cl_score.VMeasure(),
		distance=distance.AveragePairCorrelation(),
		normalizer=normalizer.MinMax(),
		optimizer=optimizer.SimAnnealing(),
		boundary_percentage=140)
	print(predictor.get_configuration_id())
	predictor.calculate_weights()

	# Predict a specific combination with a specific primary process
	# An execution time limit is provided for the primary process F
	verdict, prediction = predictor.predict_combination(
		comb=["F", "A", "H"],
		time_limit=15000,
		distance_latency=200)
	print(verdict)
	print(prediction.combined)
	# Predict, if this set of processes can be collocated.
	# Each process is assigned with an execution time limit for the specified percentile
	print(predictor.predict(
		assignment={"A": 10000, "F": 15000, "H": 12000},
		distance_latency=200))


def evaluator_example_1():
	"""An example of the Evaluator use
	The predictor is already pre-loaded with data matrices on the "nodetype_1".

	First test is to calculate the mean-shift clustering, APCorrelation distance and MinMax normalization. For all
		integrity levels from 95 to 5 with (5% steps) with 10 iterations.
	This test will provide the charts used in thesis in Section 6.5 and it runs for several hours.
	We use all five arities to calculate the predictions and use every arity with the exception of the first one as the
		targets of prediction.
	The processes can be slowed down by 10% (110% of the original execution time) - the treshold is 10%

	Returns:
		None
	"""
	evaluator = Evaluator()
	plan = TestPlan()
	plan.clustering_algs = [cl_alg.MeanShift()]
	plan.clustering_scores = [cl_score.VMeasure()]
	plan.distance = [distance.AveragePairCorrelation()]
	plan.normalizers = [normalizer.MinMax()]
	plan.optimizers = [optimizer.SimAnnealing()]

	# The TestSetup parameters are described in the class documentation
	setup = TestSetup(
		integrities=range(95, 0, -5),
		iterations=10,
		prediction_arities=[1, 2, 3, 4, 5],
		target_arities=[2, 3, 4, 5],
		treshold_percentage=110)

	evaluator.configure_evaluator(nodetype="nodetype_1", percentile=90, test_setup=setup, test_plan=plan)
	# Just a clean-up of files, in case they got corrupted in other tests
	evaluator.clean_files("headers.json")
	evaluator.predictor.assign_headers("headers.json")
	setup.weights = utils.empty_weights("nodetype_1")
	evaluator.predictor.assign_groundtruth("groundtruth.json")
	evaluator.predictor.assign_user_boundary("user_boundary.json")

	evaluator.execute_plan()


def evaluator_example_2():
	"""An example of the Evaluator use
	The predictor is already pre-loaded with data matrices on the "nodetype_1".

	In the second test we want to examine the effects of weights on the corrupted dataset.
	This test will provide all of the charts used in thesis in section 6.6 and runs for a day or two.
	We calculate only the 50% arity with 5 iteration and execute 3-fold cross validation
	We also use the default plan, which contains all combinations of algorithms.

	Returns:
		None
	"""
	evaluator = Evaluator()
	# The default plan contains all combinations of algorithms
	plan = TestPlan()
	setup = TestSetup(
		integrities=[50],
		iterations=5,
		prediction_arities=[1, 2, 3, 4, 5],
		target_arities=[2, 3, 4, 5],
		treshold_percentage=110,
		xfold=3)
	evaluator.configure_evaluator(nodetype="nodetype_1", percentile=90, test_setup=setup, test_plan=plan)
	evaluator.predictor.assign_groundtruth("groundtruth.json")
	evaluator.predictor.assign_user_boundary("user_boundary.json")

	evaluator.weights_evaluation_corrupted(seed=0, clean_headers="headers.json")


if __name__ == "__main__":
	# It is possible to enable the logging of the progress
	logging.basicConfig(level=logging.INFO)

	# Optionally the log can be saved
	# logging.basicConfig(filename='example.log', level=logging.INFO)

	predictor_example()
	evaluator_example_1()
	evaluator_example_2()
