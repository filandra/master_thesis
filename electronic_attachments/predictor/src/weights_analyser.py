import logging
from collections import OrderedDict

from sklearn.model_selection import KFold

import paths
import utilities as util

logger = logging.getLogger("weights_analyser")


class WeightsAnalyser:
	"""Class used for the calculation of weights and their cross-validation

	Attributes:
		optimizer (optimizer.Optimizer): Optimizer used during the training
		__predictor (predictor.Predictor): Reference to the predictor, which uses this weights analyzer
		__node (str): On which nodetype are the weights analyzed
		__perc (float): For which percentile the weights are trained
	"""

	def __init__(self, nodetype, percentile, predictor):
		self.optimizer = None
		self.__predictor = predictor
		self.__node = nodetype
		self.__perc = float(percentile)

	def cross_validate(self, cross_validation_splits=3):
		"""Cross-validate the effects of trained weights in order to confirm their usefulness

		First random splits of ground truth are selected. For each split the clustering score with and without weights
		is calculated for the training and the testing part of split. Finally the results are saved to the disk.

		Args:
			cross_validation_splits (int): The number of splits (the k in the k-fold cross validation)

		Returns:
			None
		"""
		logger.info("Cross-validating")

		groundtruth_files = util.load_groundtruth_files(self.__node)
		separator = KFold(n_splits=cross_validation_splits, random_state=util.SEED, shuffle=True)
		separator.get_n_splits(groundtruth_files)

		split_results = OrderedDict()
		counter = 0
		for train_index, test_index in separator.split(groundtruth_files):
			logger.info("Cross validating " + str(counter) + ". split")
			train_files, test_files = self.__select_cross_validation_files(train_index, test_index, groundtruth_files)
			original_train, new_train, weights = self.__train(train_files)
			original_test, new_test = self.__test(test_files, weights)

			split_results[counter] = self.__get_split_result(original_train, new_train, original_test, new_test)
			counter += 1

		self.__save_cross_results(split_results)

	def calculate_groundtruth_weights(self):
		"""Calculates weights using all of the groundtruth data

		Loads all of the ground truth and uses it to calculate weights, which are then stored on disk.

		Returns:
			None
		"""
		logger.info("Calculating best weights")
		groundtruth_files = util.load_groundtruth_files(self.__node)
		util.save_json(paths.TRAIN_RECORDS(self.__node), groundtruth_files.tolist())
		original_score, max_score, max_weights = self.__train(paths.TRAIN_RECORDS(self.__node), write_score=True)

		result = {"original_score": original_score, "max_score": max_score, "max_weights": max_weights}
		util.save_json(paths.BEST_WEIGHTS(self.__node, self.__perc), result)

		return max_weights

	def __train(self, used_records_path, write_score=False):
		"""Trains weights on provided ground truth processes

		While the optimizer did not converge, calculate the clustering score for latest weights and compare it to
		the previous best. If better, store the clustering score and weights.

		Args:
			used_records_path (str): Path to file containing processes which are used for training
			write_score (bool, optional): Whether to write the training data on disk or not, False by default

		Returns:
			float: Clustering score before training
			float: Clustering score after training
			numpy.ndarray of float: Vector of weights
		"""
		logger.info("Training")
		self.__predictor._preload_cluster_data(used_records_path)

		weights = util.empty_weights(self.__node)
		weights = [val - 2 / 3 for val in weights]

		max_score = 0
		original_score = None
		max_weights = []
		score_list = []

		self.optimizer.init()
		while self.optimizer.next():
			score = self.__predictor._get_clusters_score(weights)
			if score is None:
				raise Exception("Can not calculate weights - likely missing ground truth")

			if original_score is None:
				original_score = score

			if score > max_score or self.optimizer.name == "Random":
				max_score = score
				max_weights = weights.copy()

			weights = self.optimizer.generate_weights(weights, score)

			if write_score:
				score_list.append(score)

		if write_score:
			util.save_json(paths.TRAINING_SCORES(self.__node, self.__perc), score_list)
		logger.info("Finished training")
		return original_score, max_score, max_weights

	def __test(self, used_records_path, weights):
		"""Tests weights on provided ground truth processes

		Calculates the clustering score with and without the use of weights on provided ground truth processes.

		Args:
			used_records_path (str): Path to file containing processes which are used for testing
			weights (numpy.ndarray of float): Vector of process property weights

		Returns:
			float: Clustering score without weights
			float: Clustering score with weights
		"""
		logger.info("Testing")
		self.__predictor._preload_cluster_data(used_records_path)

		no_weights = util.empty_weights(self.__node)
		original_score = self.__predictor._get_clusters_score(no_weights)
		new_score = self.__predictor._get_clusters_score(weights)

		logger.info("Finished testing")
		return original_score, new_score

	def __select_cross_validation_files(self, train_index, test_index, groundtruth_files):
		"""Selects cross-validation files for given split and saves them to disk

		Args:
			train_index (ndarray): Indexes of processes used in training for a given split
			test_index (ndarray): Indexes of processes used in testing for a given split
			groundtruth_files (ndarray): IDs of all ground truth processes

		Returns:
			None
		"""
		logger.info("Selecting cross validation files")
		train_files, test_files = groundtruth_files[train_index], groundtruth_files[test_index]
		util.save_json(paths.TRAIN_RECORDS(self.__node), train_files.tolist())
		util.save_json(paths.TEST_RECORDS(self.__node), test_files.tolist())
		return paths.TRAIN_RECORDS(self.__node), paths.TEST_RECORDS(self.__node)

	def __get_split_result(self, original_train, new_train, original_test, new_test):
		"""Creates a dictionary from provided clustering score values of a given cross-validation split

		Args:
			original_train: Clustering score on training data without weights
			new_train: Clustering score on training data with weights
			original_test: Clustering score on test data without weights
			new_test: Clustering score on test data with weights

		Returns:
			dict[str, float]: Mapping of clustering score to str
		"""
		split = OrderedDict()
		split["original_train"] = original_train
		split["new_train"] = new_train
		split["change_train"] = new_train - original_train

		split["original_test"] = original_test
		split["new_test"] = new_test
		split["change_test"] = new_test - original_test

		return split

	def __save_cross_results(self, split_results):
		"""Saves results of cross-validation on disk

		Args:
			split_results (dict[int, dict[str, float]]): Mapping of the cross-validation split results to the split
				number

		Returns:
			None
		"""
		# Adds the average results into the dictionary containing results for each split
		split_results["average"] = self.__average_split_results(split_results)
		util.save_json(paths.CROSS_VALIDATION(self.__node, self.__perc), split_results)

		short_result = [str(split_results["average"]["average_change_test"])]
		info = ["average_change"] + util.get_configuration_info()
		util.save_csv_line(info, short_result, self.__predictor.get_configuration_id(),
						   paths.CROSS_VALIDATION_TABLE(self.__node, self.__perc))

	def __average_split_results(self, split_results):
		"""Averages the results of all cross-validation splits

		Args:
			split_results (dict[int, dict[str, float]]): Mapping of the cross-validation split results to the split
				number

		Returns:
			dict[str, float]: The average values of training and testing clustering score, with and without the weights
		"""
		average = OrderedDict()
		average["average_original_train"] = 0
		average["average_new_train"] = 0
		average["average_change_train"] = 0

		average["average_original_test"] = 0
		average["average_new_test"] = 0
		average["average_change_test"] = 0

		for split_num in split_results:
			for value in split_results[split_num]:
				average["average_" + value] += split_results[split_num][value]

		for value in average:
			average[value] = average[value] / len(split_results)

		return average
