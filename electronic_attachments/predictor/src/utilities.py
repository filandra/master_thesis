"""
This module contains classes and methods, which are used across the whole project and do not fit into any other module

Classes:
	Stats: Used to store statistical information about the data matrices and also normalized statistical information
	ClosestId: Used to store the ID of the most similar process together with the distance value
	PredictionResult: Used to store the results of the predictions
	Metrics: Used to store metrics used to evaluate the prediction methods in a given integrity level
	ConfusionMatrix: Used to store confusion matrix in a given integrity level
"""

import json
import os
import random
import re
import shutil
from collections import OrderedDict
from distutils.dir_util import copy_tree as cp_tree

import numpy as np

import paths


class Stats:
	"""Class used to store statistical information about the data matrices and also normalized statistical information

	Attributes:
		mean (numpy.ndarray): Mean values for all measured properties in the data matrix
		median (numpy.ndarray): Median values for all measured properties in the data matrix
		deviation (numpy.ndarray): Deviation values for all measured properties in the data matrix
		percentile (numpy.ndarray): Percentile values for all measured properties in the data matrix
		min (numpy.ndarray): Min values for all measured properties in the data matrix
		max  (numpy.ndarray): Max values for all measured properties in the data matrix
		slowdown_rel (numpy.ndarray): Relative slowdown of percentile of execution time - numpy.ndarray for consistency
	"""

	def __init__(self):
		self.mean = np.array([])
		self.median = np.array([])
		self.deviation = np.array([])
		self.percentile = np.array([])
		self.min = np.array([])
		self.max = np.array([])
		self.slowdown_rel = np.array([])


class ClosestId:
	""" Class used to store the ID of the most similar process together with the distance value

	Attributes:
		closest_id (str): ID of the process with smallest distance
		distance (float): Value of the distance
	"""

	def __init__(self, closest_id=None, distance=None):
		self.closest_id = closest_id
		self.distance = distance


class PredictionResult:
	"""Class used to store the results of the predictions

	Attributes:
		closest_friend (float): Value of the closest-friend prediction
		primary_cluster (float): Value of the primary-cluster prediction
		cluster_cluster (float): Value of the cluster-cluster prediction
		polynomial_reg (float): Value of the polynomial regression prediction
		combined (float): Value of the combined prediction
		baseline (float): Value of the baseline - the percentile of execution time of the primary process running alone
		exceeded_boundary (bool): Whether the predicted combination exceeded the boundary or not
	"""

	def __init__(self):
		self.closest_friend = None
		self.primary_cluster = None
		self.cluster_cluster = None
		self.polynomial_reg = None
		self.combined = None
		self.exceeded_boundary = False
		self.baseline = None


class Metrics:
	"""Class used to store metrics used to evaluate the prediction methods in a given integrity level

	Attributes:
		in_average_error (float): Average difference between the predicted and real value as the
			percentage of the real value inside the boundary
		in_average_improvement (float): Average improvement over baseline of the average error inside the boundary
		in_unable_predict (int): Number of combinations which the method was unable to predict inside the boundary
		in_able_predict (int): Number of combinations which the method was able to predict inside the boundary
		out_average_error (float): Average difference between the predicted and real value as the
			percentage of the real value outside the boundary
		out_average_improvement (float): Average improvement over baseline of the average error outside the boundary
		out_unable_predict (int): Number of combinations which the method was unable to predict outside the boundary
		out_able_predict (int): Number of combinations which the method was able to predict outside the boundary
	"""

	def __init__(self):
		self.in_average_error = 0.0
		self.in_average_improvement = 0.0
		self.in_unable_predict = 0.0
		self.in_able_predict = 0.0
		self.out_average_error = 0.0
		self.out_average_improvement = 0.0
		self.out_unable_predict = 0.0
		self.out_able_predict = 0.0

	def finalize(self):
		"""Averages the values stored in variables by the number of predictions

		Returns:
			None
		"""
		in_succ = self.in_able_predict
		out_succ = self.out_able_predict
		in_total = self.in_able_predict + self.in_unable_predict
		out_total = self.out_able_predict + self.out_unable_predict

		in_succ = in_succ if in_succ > 0 else 1
		out_succ = out_succ if out_succ > 0 else 1
		in_total = in_total if in_total > 0 else 1
		out_total = out_total if out_total > 0 else 1

		self.in_average_improvement = (self.in_average_improvement / in_succ) * 100
		self.in_average_error = (self.in_average_error / in_succ) * 100
		self.in_unable_predict = (self.in_unable_predict / in_total) * 100
		self.in_able_predict = (self.in_able_predict / in_total) * 100

		self.out_average_improvement = (self.out_average_improvement / out_succ) * 100
		self.out_average_error = (self.out_average_error / out_succ) * 100
		self.out_unable_predict = (self.out_unable_predict / out_total) * 100
		self.out_able_predict = (self.out_able_predict / out_total) * 100


class ConfusionMatrix:
	"""Class used to store confusion matrix in a given integrity level

	Attributes:
		in_TP (float): Percentage of true positive cases inside the boundary
		in_TN (float): Percentage of true negative cases inside the boundary
		in_FP (float): Percentage of false positive cases inside the boundary
		in_FN (float): Percentage of false negative cases inside the boundary
		out_TP (float): Percentage of true positive cases outside the boundary
		out_TN (float): Percentage of true negative cases outside the boundary
		out_FP (float): Percentage of false positive cases outside the boundary
		out_FN (float): Percentage of false negative cases outside the boundary
		__inside_num (int): Number of predictions inside the boundary
		__outside_num (int): Number of predictions outside the boundary

	"""

	def __init__(self):
		self.in_TP = 0.0
		self.in_TN = 0.0
		self.in_FP = 0.0
		self.in_FN = 0.0
		self.out_TP = 0.0
		self.out_TN = 0.0
		self.out_FP = 0.0
		self.out_FN = 0.0
		self.__inside_num = 0
		self.__outside_num = 0

	def add_verdict(self, verdict):
		"""Increments the appropriate variable based on the situation

		Args:
			verdict (dict[str, bool]): Mapping of prediction verdict to string (predicted or real)

		Returns:
			None
		"""
		if not verdict["exceeded_boundary"]:
			self.__inside_num += 1
			if verdict["predicted"] is True and verdict["real"] is True:
				self.in_TP += 1
			if verdict["predicted"] is True and verdict["real"] is False:
				self.in_FP += 1
			if verdict["predicted"] is False and verdict["real"] is True:
				self.in_FN += 1
			if verdict["predicted"] is False and verdict["real"] is False:
				self.in_TN += 1
		else:
			self.__outside_num += 1
			if verdict["predicted"] is True and verdict["real"] is True:
				self.out_TP += 1
			if verdict["predicted"] is True and verdict["real"] is False:
				self.out_FP += 1
			if verdict["predicted"] is False and verdict["real"] is True:
				self.out_FN += 1
			if verdict["predicted"] is False and verdict["real"] is False:
				self.out_TN += 1

	def finalize(self):
		"""Divides the variables by the number of all recorded cases
			in order to transform them from case count into a percentage of all cases

		Returns:
			None
		"""
		self.__inside_num = self.__inside_num if self.__inside_num > 0 else 1
		self.__outside_num = self.__outside_num if self.__outside_num > 0 else 1

		self.in_TP = self.in_TP / self.__inside_num
		self.in_FP = self.in_FP / self.__inside_num
		self.in_FN = self.in_FN / self.__inside_num
		self.in_TN = self.in_TN / self.__inside_num
		self.out_TP = self.out_TP / self.__outside_num
		self.out_FP = self.out_FP / self.__outside_num
		self.out_FN = self.out_FN / self.__outside_num
		self.out_TN = self.out_TN / self.__outside_num

	@staticmethod
	def get_sets():
		"""Returns dictionary with sets of variables to be plotter together in a chart

		Returns:
			dict[str, list of str]: The sets of variables
		"""
		return {"in": ["in_TP", "in_TN", "in_FP", "in_FN"], "out": ["out_TP", "out_TN", "out_FP", "out_FN"]}


PREDICTIONS = ["baseline", "polynomial_reg", "cluster_cluster", "combined", "closest_friend", "primary_cluster"]
WEIGHTED_PREDICTIONS = ["closest_friend", "primary_cluster", "cluster_cluster"]
EXTENSION = ".csv"
JSON = ".json"
PNG = ".png"
SINGLE_ARITY = "1"
SEED = 0
ELAPSED = "elapsed"
SCALE = "scale"
SKIP_METHOD = ["exceeded_boundary"]
INPUT_REGEXP = r'[A-Za-z]*[0-9]*(-[A-Za-z]*[0-9]*)*'


# Random related
# ----------------------------------------------------------------------------------------------------------------------
def get_random():
	return random.random()


def set_seed(seed):
	global SEED
	SEED = seed
	random.seed(seed)


def get_rand_int(range):
	return random.randrange(range)


def get_rand_bool():
	return bool(random.getrandbits(1))


def get_rand_choice(items):
	return random.choice(items)


# ----------------------------------------------------------------------------------------------------------------------


# Sorting related
# ----------------------------------------------------------------------------------------------------------------------
def natural_keys(text):
	return [text_to_int(c) for c in re.split(r'(\d+)', text)]


def text_to_int(text):
	return int(text) if text.isdigit() else text


def sorted_combination(combination):
	# Sorts the secondary processes of a combination
	if len(combination) == 0:
		return []

	if len(combination) == 1:
		return combination
	else:
		sorted_combination = [combination[0]]
		secondary = combination[1:]
		secondary.sort(key=natural_keys)
		sorted_combination.extend(secondary)
		return sorted_combination


def sorted_name(combination):
	if len(combination) == 0:
		return ""

	if len(combination) == 1:
		return combination[0] + EXTENSION
	else:
		primary = [combination[0]]
		secondary = combination[1:]
		secondary.sort(key=natural_keys)
		primary.extend(secondary)

		file_name = "-".join(primary) + EXTENSION

	return file_name


# ----------------------------------------------------------------------------------------------------------------------

# Dictionary related
# ----------------------------------------------------------------------------------------------------------------------
def encode_dict(dict):
	encoded_dict = OrderedDict()
	for key in dict:
		encoded_dict[key] = to_dict(dict[key])
	return encoded_dict


def decode_dict(encoded_dict, type):
	dict = OrderedDict()
	if encoded_dict == None:
		return dict

	for key in encoded_dict:
		dict[key] = from_dict(encoded_dict[key], type)
	return dict


def to_dict(object):
	return vars(object)


def from_dict(dict, type):
	instance = type()
	for attr in dict:
		setattr(instance, attr, dict[attr])
	return instance


# ----------------------------------------------------------------------------------------------------------------------

# JSON related
# ----------------------------------------------------------------------------------------------------------------------
def save_json(file_path, content):
	with open(file_path, 'w') as file:
		json.dump(content, file, indent=4)


def load_json(file_path):
	try:
		with open(file_path, 'r') as file:
			return json.load(file)
	except:
		open(file_path, 'a').close()


def load_json_list(file_path):
	try:
		with open(file_path, "r") as file:
			list = json.load(file)
			if list is None:
				list = []
		return list
	except:
		open(file_path, 'a').close()
		return []


def load_json_OrderedDict(file_path):
	try:
		with open(file_path, 'r') as f:
			return json.load(f, object_pairs_hook=OrderedDict)
	except:
		open(file_path, 'a').close()
		return OrderedDict()


# ----------------------------------------------------------------------------------------------------------------------


# Directory related
# ----------------------------------------------------------------------------------------------------------------------
def list_dir(dir):
	try:
		return [file for file in os.listdir(dir)]
	except:
		return []


def get_dirs(path, dirs=True):
	if dirs is False:
		return [file for file in os.listdir(path) if os.path.isdir(os.path.join(path, file)) is False]
	else:
		return [os.path.join(path, dir) for dir in os.listdir(path) if os.path.isdir(os.path.join(path, dir)) is True]


def clean_dir(path):
	try:
		shutil.rmtree(path)
	except:
		pass
	os.makedirs(path, exist_ok=True)


def copy_eval(nodetype, percentile, configuration_id):
	cp_tree(paths.EVAL_DIR(nodetype, percentile),
			paths.AGGREGATE_DIR(nodetype, percentile, configuration_id))


# ----------------------------------------------------------------------------------------------------------------------


# Headers, ground truth and weights related
# ----------------------------------------------------------------------------------------------------------------------
headers_info = {}


def empty_weights(nodetype):
	check_headers_info(nodetype)
	return np.ones(headers_info[nodetype]["weights_num"])


def stats_elapsed_col(nodetype):
	check_headers_info(nodetype)
	return headers_info[nodetype]["stats_elapsed_column"]


def original_elapsed_col(nodetype):
	check_headers_info(nodetype)
	return headers_info[nodetype]["elapsed_column"]


def scale_column(nodetype):
	check_headers_info(nodetype)
	return headers_info[nodetype]["scale_column"]


def data_columns_num(nodetype):
	check_headers_info(nodetype)
	return headers_info[nodetype]["data_columns_num"]


def check_headers_info(nodetype):
	headers_info[nodetype] = load_json(paths.HEADERS_INFO(nodetype))


def load_groundtruth_files(nodetype):
	groundtruth = load_json_OrderedDict(paths.GROUNDTRUTH(nodetype))
	groundtruth_files = [id + JSON for id in groundtruth]
	return np.array(groundtruth_files)


def cointains_groundtruth(file, groundtruth_files):
	combination = file.replace(JSON, "").split("-")
	for groundtruth_file in groundtruth_files:
		groundtruth_id = groundtruth_file.replace(JSON, "")
		if groundtruth_id in combination:
			return True
	return False


def load_best_weights(nodetype, percentile):
	try:
		weights = load_json(paths.BEST_WEIGHTS(nodetype, percentile))
		if weights == [] or weights is None:
			result = empty_weights(nodetype)
		else:
			result = weights["max_weights"]
	except:
		result = empty_weights(nodetype)

	return result


# ----------------------------------------------------------------------------------------------------------------------


# Evaluation related
# ----------------------------------------------------------------------------------------------------------------------
def get_empty_evaluation():
	evaluation = OrderedDict()
	for prediction_method in to_dict(PredictionResult()):
		evaluation[prediction_method] = Metrics()

	return evaluation


def load_eval(nodetype, percentile, id):
	evaluations = OrderedDict()
	integrities = sorted([int(file.replace(JSON, "").replace(id + "_", "")) for file in
						  os.listdir(paths.EVAL_EVALUATIONS_DIR(nodetype, percentile)) if id in file], reverse=True)
	for integrity in integrities:
		with open(os.path.join(paths.EVAL_EVALUATIONS_DIR(nodetype, percentile), id + "_" + str(integrity) + JSON),
				  'r') as f:
			evaluations[str(integrity)] = json.load(f)

	return evaluations


def load_confusion(nodetype, percentile, id):
	confusion = OrderedDict()
	integrities = sorted([int(file.replace(JSON, "").replace(id + "_", "")) for file in
						  os.listdir(paths.EVAL_CONFUSION_DIR(nodetype, percentile)) if id in file], reverse=True)
	for integrity in integrities:
		with open(os.path.join(paths.EVAL_CONFUSION_DIR(nodetype, percentile), id + "_" + str(integrity) + JSON),
				  'r') as f:
			confusion[str(integrity)] = json.load(f)

	return confusion


def save_csv_line(info, result, configuration_id, path):
	if not os.path.exists(path):
		with open(path, "a") as table:
			print(";".join(info), file=table)

	result += configuration_id
	with open(path, "a") as table:
		print(";".join(result), file=table)


def get_configuration_info():
	return ["scoring", "clustering", "distance", "normalizer", "optimizer", "boundary_percentage"]
