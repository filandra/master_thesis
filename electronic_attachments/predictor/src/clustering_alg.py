"""
This module contains classes, which implement the algorithms for the calculation of clusters.

Classes:
	ClusteringAlg: Base class, which is inherited by classes implementing algorithms for the clustering of processes
	AffinityPropagation: Class implementing the affinity propagation clustering algorithm
	MeanShift: Class implementing the mean-shift clustering algorithm
"""

import numpy as np
import sklearn.cluster


class ClusteringAlg:
	"""Base class, which is inherited by classes implementing algorithms for the clustering of processes

	Subclasses of ClusteringAlg are expected to implement clustering algorithms of processes from a distance matrix.
	The distance matrix contains calculated distance between each pair of processes
	New clustering algorithms can be easily added by inheriting from ClusteringAlg and overriding provided methods.

	Attributes:
		name (str): name of the clustering algorithm
	"""

	def __init__(self):
		self.name = ""

	def cluster(self, distance_matrix):
		"""A virtual method for calculating clusters from provided distance matrix

		The subclasses are expected to override this method.

		Args:
			distance_matrix (numpy.ndarray of float): A matrix containing calculated distance for each pair of processes

		Returns:
			numpy.ndarray of int: A vector containing the cluster numebr for each process in the distance matrix.
		"""
		return []


class AffinityPropagation(ClusteringAlg):
	"""Class implementing the affinity propagation clustering algorithm

		Attributes:
			name (str): name of the clustering algorithm
			__alg (sklearn.cluster.AffinityPropagation): algorithm provided by sklearn
	"""

	def __init__(self):
		super().__init__()
		self.__alg = sklearn.cluster.AffinityPropagation()
		self.name = "Affinity"

	def cluster(self, distance_matrix):
		"""Calculates clusters from provided distance matrix using affinity propagation

		Args:
			distance_matrix (list numpy.ndarray of float): A matrix containing calculated distance for each pair of processes

		Returns:
			numpy.ndarray of int: A vector containing the cluster number for each process in the distance matrix.
				If the calculation of clusters in unsuccessful, returns list containing only -1 values
		"""
		self.__alg.fit(distance_matrix)
		clusters_list = self.__alg.predict(distance_matrix)
		return clusters_list


class MeanShift(ClusteringAlg):
	"""Class implementing the mean-shift clustering algorithm

		Attributes:
			name (str): name of the clustering algorithm
	"""

	def __init__(self):
		super().__init__()
		self.name = "MeanShift"

	def cluster(self, distance_matrix):
		"""Calculates clusters from provided distance matrix using mean-shift

		Args:
			distance_matrix (list numpy.ndarray of float): A matrix containing calculated distance for each pair of processes

		Returns:
			numpy.ndarray of int: A vector containing the cluster number for each process in the distance matrix.
				If the calculation of clusters in unsuccessful, returns list containing only -1 values
		"""
		try:
			quantile = 0.1
			bandwidth = sklearn.cluster.estimate_bandwidth(distance_matrix, quantile=quantile)
			while bandwidth <= 0:
				quantile += 0.01
				bandwidth = sklearn.cluster.estimate_bandwidth(distance_matrix, quantile=quantile)

			meanshift = sklearn.cluster.MeanShift(bandwidth=bandwidth, n_jobs=1)
			meanshift.fit(distance_matrix)
		except Exception:
			try:
				meanshift = sklearn.cluster.MeanShift(n_jobs=1)
				meanshift.fit(distance_matrix)
			except Exception:
				return np.full(len(distance_matrix), -1)

		clusters_list = meanshift.labels_.astype(np.int)
		return clusters_list
