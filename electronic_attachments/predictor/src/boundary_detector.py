import logging
import sys

import numpy as np

import paths
import utilities as util
from record import Record

logger = logging.getLogger("boundary_detector")


class BoundaryDetector:
	"""Class used for the calculation of the boundary and detection of the boundary transgression

		Attributes:
			boundary_percentage (float): Percentage of the computer capacity, which the combination of processes can not
				exceed
			__node (str): On which nodetype is the boundary analyzed
			__perc (float): For which percentile is the boundary analyzed
			__single_stats (dict[str, record.Record]): Mapping of single process Records to process IDs
			__boundary (list of float): List of maximal values for each measured property
		"""

	def __init__(self, nodetype, percentile):
		self.boundary_percentage = 100
		self.__node = nodetype
		self.__perc = float(percentile)
		self.__single_stats = None
		self.__boundary = None

	def calculate_boundary(self):
		"""Calculates the maximal values for each measured property

		Acquires the maximal values from measured single data matrices and the user-specified maximal values.
		For each measured property finds the greater maximal value and multiplies it by the boundary percentage.
		Saves the resulting maximal values (boundary) on disk.

		Returns:
			None
		"""
		values = self.__get_single_max()
		values.append(self.__get_user_boundary())

		multiplier = self.boundary_percentage / 100
		self.__boundary = [float(multiplier * max_value) for max_value in list(np.maximum.reduce(values))]
		util.save_json(paths.BOUNDARY(self.__node), self.__boundary)

	def exceeds_boundary(self, combination):
		"""Detects, if a combination of processes exceeds the boundary

		If missing, loads the boundary and scaled single data matrices.
		Sums the average value of the measured properties of all processes in a combination.
		Compares the combined values of measured properties to the specified percentage of maximal values of
		those properties (the boundary).
		Because the measured properties indicate the use of a certain computer resource either per second or in absolute
		values, this comparison can be interpreted as comparing the cumulative resource usage of the processes in a
		given combination to a specified percentage of the maximal values, which the computer is able to provide.

		Args:
			combination (list of str): List of process IDs

		Returns:
			bool: True if the combination exceeds the boundary, False otherwise
		"""
		if self.__boundary is None or self.__single_stats is None:
			self.__boundary = util.load_json_list(paths.BOUNDARY(self.__node))
			if not self.__boundary:
				raise Exception("Boundary not calculated")

			self.__single_stats = Record.load_single_stats(self.__node, self.__perc)

		cumulative_usage = []
		for process in combination:
			cumulative_usage.append(self.__single_stats[process].stats.mean)

		cumulative_usage = np.sum(cumulative_usage, axis=0)

		# Excluding the elapsed time from comparison (first column)
		if all(i < j for (i, j) in zip(cumulative_usage[1:], self.__boundary[1:])):
			return False
		else:
			return True

	def __get_single_max(self):
		"""Loads the maximal values from single data matrices for each measured property

		Returns:
			list of numpy.ndarray of float: The maximal values for each measured property
		"""
		self.__single_stats = Record.load_single_stats(self.__node, self.__perc)

		max_stats = []
		for process in self.__single_stats:
			max_stats.append(np.array(self.__single_stats[process].stats.max, dtype=np.float32))

		return max_stats

	def __get_user_boundary(self):
		"""Loads the user-specified maximal values for each measured property

		Returns:
			numpy.ndarray of float: The user-specified maximal values for each measured property. If these values are
				not specified or are specified incorrectly, returns sys.maxsize instead (practically infinite value)
		"""
		try:
			values = util.load_json_list(paths.USER_BOUNDARY(self.__node))
			user_max = self.__trim_user_boundary(values)
			if len(user_max) == util.data_columns_num(self.__node):
				logger.info("Using user boundary")
				return np.array(user_max, dtype=np.float32)
			else:
				logger.info("Wrong user boundary input")
				return np.full(util.data_columns_num(self.__node), sys.maxsize)
		except Exception:
			logger.info("Wrong user boundary input")
			return np.full(util.data_columns_num(self.__node), sys.maxsize)

	def __trim_user_boundary(self, original):
		"""Deletes useless columns from user-specified boundary

		The user specifies maximal value for each header in the data matrix headers file. Some of the headers are not
		used during the boundary calculation and are deleted as a result.
		All values preceding the "elapsed" header and the value for the "scale" header are deleted.

		Args:
			original (list of float): Original user-specified boundary with values for all headers

		Returns:
			list of float: User-specified boundary with useless values deleted
		"""
		# throw away columns before elapsed column - contains only metadata
		delete_columns = [i for i in range(0, util.original_elapsed_col(self.__node))]
		trimmed = np.delete(original, delete_columns)

		# throw away the scaled header column
		scale_position = util.scale_column(self.__node) - util.original_elapsed_col(self.__node)
		trimmed = np.delete(trimmed, scale_position)

		return trimmed
