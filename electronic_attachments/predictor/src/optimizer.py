"""
This module contains classes, which implement optimizers used to calculate the weights

Classes:
	Optimizer: Base class, which is inherited by classes implementing the optimization algorithms
	SimAnnealing: Class implementing the simulated annealing optimization algorithm
	Climb: Class implementing simple hill climbing optimization algorithm
	Random: Class implementing random generation of weights
"""

import logging
import math

import numpy as np

import utilities as util

logger = logging.getLogger("optimizer")


class Optimizer:
	"""Base class, which is inherited by classes implementing the optimization algorithms

	Subclasses of Optimizer are expected to iteratively generate weights until convergence.
	New optimization algorithms can be easily added by inheriting from Optimizer and overriding provided methods.

	Attributes:
		name (str): name of the distance measure
		_temperature (float): Temperature value used in simulated annealing and in convergence estimation
		_cooling (float): Cooling value used in simulated annealing and in convergence estimation
		_current_score (float): The value of last accepted clustering score
		_current_weights (numpy.ndarray): The last accepted vector of weights
		_change_distance (float): The value by which are the weights changed in each iteration
	"""

	def __init__(self):
		self.name = ""
		self._temperature = 0
		self._cooling = 0
		self._current_score = 0
		self._current_weights = []
		self._change_distance = 1 / 3

	def generate_weights(self, new_weights, new_score):
		"""A virtual method used to iteratively change the weights until convergence

		The method is provided with clustering score and weights used to obtain this score.
		The subclasses are expected to evaluate the clustering score and then generate another iteration of weights
		based on the provided weights or based on saved weights from previous iterations.
		The subclasses are expected to override this method.

		Args:
			new_weights (numpy.ndarray): Weights which result in the provided clustering score
			new_score (float): Value of the clustering score

		Returns:
			numpy.ndarray: Vector of weights
		"""
		return np.ndarray([])

	def _change_weights(self, weights, distance):
		"""Default way of generating new weights

		Changes a random value in the vector by the specified distance in the bounds of 0 and 1.

		Args:
			weights (numpy.ndarray): The weights to change
			distance (float): The value by which to change one weight

		Returns:
			numpy.ndarray: The changed weights
		"""
		self.index = util.get_rand_int(len(weights))
		new_value = (weights[self.index] + distance)

		if new_value > 1:
			new_value = 1
		if new_value < 0:
			new_value = 0

		weights[self.index] = new_value
		return weights

	def next(self):
		"""The default way of determining convergence

		Returns:
			None
		"""
		if self._temperature > 1:
			return True
		else:
			return False


class SimAnnealing(Optimizer):
	"""Class implementing the simulated annealing optimization algorithm

	Attributes:
		name (str): name of the optimization algorithm
	"""

	def __init__(self):
		super().__init__()
		self.name = "Annealing"

	def init(self):
		"""Initalization of the algorithm

		Returns:
			None
		"""
		self._temperature = 100000
		self._cooling = 0.99

	def generate_weights(self, new_weights, new_score):
		"""Generate next iteration of weights based on the simulated annealing algorithm

		Args:
			new_weights (numpy.ndarray): Weights which result in the provided clustering score
			new_score (float): Value of the clustering score

		Returns:
			numpy.ndarray: The generated weights
		"""
		delta = new_score - self._current_score

		if delta >= 0:
			chance_to_move = 1
		else:
			chance_to_move = math.exp((delta * 10000) / self._temperature)

		if chance_to_move > util.get_random():
			# move to the new point
			self._current_weights = new_weights.copy()
			result = new_weights.copy()
			self._current_score = new_score
		else:
			# stay in the old point
			result = self._current_weights.copy()

		if util.get_rand_bool():
			result = self._change_weights(result, self._change_distance)
		else:
			result = self._change_weights(result, -self._change_distance)

		self._temperature *= self._cooling
		return result


class Climb(Optimizer):
	"""Class implementing simple hill climbing optimization algorithm

	Attributes:
		name (str): name of the optimization algorithm
	"""

	def __init__(self):
		super().__init__()
		self.name = "Climb"

	def init(self):
		"""Initalization of the algorithm

		Returns:

		"""
		self._temperature = 1000
		self._cooling = 0.99

	def generate_weights(self, new_weights, new_score):
		"""Generate next iteration of weights based on the hill climbing algorithm

		Args:
			new_weights (numpy.ndarray): Weights which result in the provided clustering score
			new_score (float): Value of the clustering score

		Returns:
			numpy.ndarray: The generated weights
		"""
		delta = new_score - self._current_score

		if delta >= 0:
			# move to the new point
			self._current_weights = new_weights.copy()
			result = new_weights.copy()
			self._current_score = new_score
		else:
			# stay in the old point
			result = self._current_weights.copy()

		if util.get_rand_bool():
			result = self._change_weights(result, self._change_distance)
		else:
			result = self._change_weights(result, -self._change_distance)

		self._temperature *= self._cooling
		return result


class Random(Optimizer):
	"""Class implementing random generation of weights

	Attributes:
		name (str): name of the optimization algorithm
		__counter (int): Counter used to determine convergence
	"""

	def __init__(self):
		super().__init__()
		self.name = "Random"
		self.__counter = 0

	def init(self):
		"""Initalization of the algorithm

		Returns:
			None
		"""
		self.__counter = 2

	def next(self):
		"""The random weights are generated two times in order to maintain consistency in the weights generation process

		Returns:
			None
		"""
		if self.__counter > 0:
			self.__counter -= 1
			return True
		else:
			return False

	def generate_weights(self, new_weights, new_score):
		"""Simple random generation of weights

		Args:
			new_weights (numpy.ndarray): Weights which result in the provided clustering score
			new_score (float): Value of the clustering score

		Returns:
			numpy.ndarray: The generated weights
		"""
		result = new_weights.copy()
		for i in range(0, len(result)):
			result[i] = util.get_random()
		return result
