import collections
import json
import os

import numpy as np

import paths
import utilities as util


class Record:
	"""Universal class used to store original information from which statistics are calculated, together with the
		ID of primary process and ID of the combination to which the original data belongs.

	The Record is used three ways thorough the project:
	1) To store preprocessed data matrices:
		contains: ID of the primary process and the combination of processes, original data matrix and the statistics
	2) To store primary-cluster data:
		contains: ID of the primary process and the primary-process ID, vector of slowdowns and the statistics
	3) To store the cluster-cluster data:
		contains: ID of the primary process and the primary cluster, vector of slowdowns and the statistics

	The class also enables to load original and statistical data to Records from disk and to save them.

	Attributes:
		primary_id (str): ID of the primary process of a data matrix, or the primary process in
			primary-cluster or primary cluster in cluster-cluster data.
		combination_id (str): ID of the combination data matrix (IDs of processes separated by dash),
			or the primary-cluster or cluster-cluster ID.
		original (numpy.ndarray): The original data, from which statistics are calculated (a matrix or a vector)
		stats (utilities.Stats): The statistical information extracted from the original data, can be normalized
	"""

	def __init__(self):
		self.primary_id = ""
		self.combination_id = ""
		self.original = np.array([])
		self.stats = util.Stats()

	def save_stats(self, directory):
		"""Saves the statistical data contained in utilities.Stats to disk

		Args:
			directory (str): Path to the directory, where the data should be stored

		Returns:
			None
		"""
		arity = str(len(self.combination_id))
		os.makedirs(os.path.join(directory, arity), exist_ok=True)

		# numpy array cannot be saved into json - converting all numpy arrays into lists
		stats_list = collections.OrderedDict()
		for stat in util.to_dict(self.stats):
			try:
				stats_list[stat] = (getattr(self.stats, stat)).tolist()
			except Exception:
				stats_list[stat] = (getattr(self.stats, stat))

		with open(os.path.join(directory, arity, "-".join(self.combination_id) + util.JSON), 'w') as file:
			json.dump(stats_list, file, indent=4)

	def save_original(self, directory, csv=False):
		"""Saves the original values, from which statistics are calculated

		Args:
			directory (str): Path to the directory, where the data should be stored
			csv (bool, optional): Whether the file is in csv format, by default stores json format

		Returns:
			None
		"""
		arity = str(len(self.combination_id))
		os.makedirs(os.path.join(directory, arity), exist_ok=True)

		if csv == False:
			with open(os.path.join(directory, arity, "-".join(self.combination_id) + util.JSON), 'w') as file:
				json.dump(self.original, file, indent=4)
		else:
			with open(os.path.join(directory, arity, "-".join(self.combination_id) + util.EXTENSION), 'w') as file:
				for row in self.original:
					row = list(row)
					for i in range(0, len(row)):
						row[i] = str(int(row[i]))
					print(";".join(row), file=file)

	def load_data_matrix(self, directory, file):
		"""Loads data matrix from disk and fills in the primary and combination ID from the name of the file

		Args:
			directory (str): Path to the directory, from where the data should be loaded
			file (str): Name of the file containing the data matrix

		Returns:
			None
		"""
		combination = file.replace(util.EXTENSION, "").split("-")

		self.primary_id = combination[0]
		self.combination_id = combination
		self.original = np.genfromtxt(os.path.join(directory, file), delimiter=";")

	@staticmethod
	def load_single_data_matrices(nodetype):
		"""Loads all single data matrices from disk

		Args:
			nodetype (str): Load data matrices belonging to a specified nodetype

		Returns:
			dict[str, Record]: Mapping between the primary process IDs and the Records
		"""
		single_records = collections.OrderedDict()

		if not os.path.isdir(os.path.join(paths.DATA_MATRICES_DIR(nodetype), util.SINGLE_ARITY)):
			# There are no single files - we cannot calculate stats
			return None

		original_single_files = util.list_dir(os.path.join(paths.DATA_MATRICES_DIR(nodetype), util.SINGLE_ARITY))
		if len(original_single_files) == 0:
			# There are no single files - we cannot calculate stats
			return None

		# Load single records from disk
		for file in original_single_files:
			id = file.replace(util.EXTENSION, "")
			record = Record()
			record.load_data_matrix(os.path.join(paths.DATA_MATRICES_DIR(nodetype), util.SINGLE_ARITY), file)
			single_records[id] = record

		return single_records

	@staticmethod
	def load_comb_data_matrices(nodetype, single_records):
		"""Loads all combination data matrices from disk, which have a Record of their primary process available

		Args:
			nodetype (str): Load data matrices belonging to a specified nodetype
			single_records dict[str, Record]: Records for single process data matrices

		Returns:
			dict[str, Record]: Mapping between the process combination IDs and the Records
		"""
		comb_originals = collections.OrderedDict()
		arity_paths = [os.path.join(paths.DATA_MATRICES_DIR(nodetype), arity) for arity in
					   os.listdir(paths.DATA_MATRICES_DIR(nodetype)) if arity != util.SINGLE_ARITY]

		for arity in arity_paths:
			comb_files = util.list_dir(arity)

			for file in comb_files:
				id = file.replace(util.EXTENSION, "")
				processes = id.split("-")

				for process in processes:
					if process not in single_records:
						# There is a process without a single data matrix
						continue

				record = Record()
				record.load_data_matrix(arity, file)
				record.primary_id = processes[0]
				comb_originals[id] = record

		return comb_originals

	@staticmethod
	def load_single_stats(nodetype, percentile, used_records_path=None):
		"""Loads statistical information for specified single process data matrices

		Args:
			nodetype (str): Load data matrices belonging to a specified nodetype
			percentile (float): Load statistics specific for specified percentile
			used_records_path: Path to the file containing IDs of processes to load

		Returns:
			dict[str, Record]: Mapping of loaded Records to the process IDs
		"""
		if not os.path.exists(os.path.join(paths.STATS_DIR(nodetype, percentile), util.SINGLE_ARITY)):
			return collections.OrderedDict()

		used_files = []
		if used_records_path is not None:
			with open(used_records_path, 'r') as file:
				used_files = json.load(file)

		single_records = collections.OrderedDict()
		single_record_files = util.list_dir(os.path.join(paths.STATS_DIR(nodetype, percentile), util.SINGLE_ARITY))
		single_record_files.sort()

		for file in single_record_files:
			# Throw away files, which are not used
			if used_records_path is not None and file not in used_files:
				continue
			record = Record()
			record.__load_stats(os.path.join(paths.STATS_DIR(nodetype, percentile), util.SINGLE_ARITY), file)
			single_records["-".join(record.combination_id)] = record

		return single_records

	@staticmethod
	def load_normalized_stats(nodetype, percentile, used_records_path=None):
		"""Loads normalized statistical information for specified single process data matrices

		Args:
			nodetype (str): Load data matrices belonging to a specified nodetype
			percentile (float): Load statistics specific for specified percentile
			used_records_path: Path to the file containing IDs of processes to load

		Returns:
			dict[str, Record]: Mapping of loaded Records to the process IDs
		"""
		if not os.path.exists(os.path.join(paths.NORMALIZED_DIR(nodetype, percentile), util.SINGLE_ARITY)):
			return collections.OrderedDict()

		used_files = []
		if used_records_path is not None:
			with open(used_records_path, 'r') as file:
				used_files = json.load(file)

		normalized_records = collections.OrderedDict()

		record_files = util.list_dir(os.path.join(paths.NORMALIZED_DIR(nodetype, percentile), util.SINGLE_ARITY))
		record_files.sort(key=util.natural_keys)

		for file in record_files:
			if used_records_path is not None and file not in used_files:
				continue
			record = Record()
			record.__load_stats(os.path.join(paths.NORMALIZED_DIR(nodetype, percentile), util.SINGLE_ARITY), file)
			normalized_records["-".join(record.combination_id)] = record

		return normalized_records

	@staticmethod
	def load_comb_stats(nodetype, percentile, used_records_path=None):
		"""Loads statistical information for specified combination data matrices

		Args:
			nodetype (str): Load data matrices belonging to a specified nodetype
			percentile (float): Load statistics specific for specified percentile
			used_records_path: Path to the file containing IDs of process combinations to load

		Returns:
			dict[str, Record]: Mapping of loaded Records to the combination process IDs
		"""
		used_files = []
		if used_records_path is not None:
			with open(used_records_path, 'r') as file:
				used_files = json.load(file)

		comb_records = collections.OrderedDict()

		arity_dirs = util.get_dirs(paths.STATS_DIR(nodetype, percentile))
		arity_dirs.sort()

		# Throw away the single arity, if present
		if os.path.join(paths.STATS_DIR(nodetype, percentile), util.SINGLE_ARITY) in arity_dirs:
			arity_dirs.remove(os.path.join(paths.STATS_DIR(nodetype, percentile), util.SINGLE_ARITY))

		for arity in arity_dirs:
			for file in util.list_dir(arity):
				# Throw away files, which are not used
				if used_records_path is not None and file not in used_files:
					continue
				record = Record()
				record.__load_stats(arity, file)
				comb_records["-".join(record.combination_id)] = record

		return comb_records

	@staticmethod
	def load_record_names(directory):
		"""Loads process combination IDs of files in a directory

		Args:
			directory (str): Path to the directory, from which to load the Record names

		Returns:
			list of str: List of the names
		"""
		names = []
		arity_dirs = util.get_dirs(directory)
		arity_dirs.sort()

		for arity in arity_dirs:
			for file in sorted(util.list_dir(arity)):
				names.append(file.replace(util.EXTENSION, "").replace(util.JSON, ""))

		return names

	@staticmethod
	def get_cluster_id(combination, id_to_cluster, primary):
		"""Returns primary-cluster or cluster-cluster ID of a combination of processes

		Args:
			combination (list of str): List of process IDs in a combination
			id_to_cluster (dict[str, int]): Mapping of process ID to their cluster number
			primary (bool): Whether to calculate primary-cluster or cluster-cluster ID

		Returns:
			str: The primary-cluster or cluster-cluster id
		"""
		influencers_list = combination[1:]
		influencers_list = sorted([id_to_cluster[id] for id in influencers_list])
		influencers_list = [str(cluster_num) for cluster_num in influencers_list]

		if primary:
			cluster_id = combination[0]
		else:
			cluster_id = str(id_to_cluster[combination[0]])

		cluster_id += "-" + "-".join(influencers_list)

		return cluster_id

	@staticmethod
	def load_clusters_stats(nodetype, percentile, primary):
		"""Loads all primary-cluster or cluster-cluster data

		Args:
			nodetype (str): Load data matrices belonging to a specified nodetype
			percentile (float): Load statistics specific for specified percentile
			primary (bool): Whether to load primary-cluster or cluster-cluster data

		Returns:
			dict[str, Record]: Mapping of the Records to the primary-cluster or cluster-cluster IDs
		"""
		if not os.path.exists(paths.PRIMARY_CLUSTER_STATS_DIR(nodetype, percentile)):
			return collections.OrderedDict()

		if not os.path.exists(paths.CLUSTER_CLUSTER_STATS_DIR(nodetype, percentile)):
			return collections.OrderedDict()

		cluster_records = collections.OrderedDict()
		# If we do not choose specific arities to load
		if primary:
			arity_dirs = util.get_dirs(paths.PRIMARY_CLUSTER_STATS_DIR(nodetype, percentile))
		else:
			arity_dirs = util.get_dirs(paths.CLUSTER_CLUSTER_STATS_DIR(nodetype, percentile))
		arity_dirs.sort()

		for arity in arity_dirs:
			for file in util.list_dir(arity):
				record = Record()
				record.__load_stats(arity, file)
				cluster_records["-".join(record.combination_id)] = record

		return cluster_records

	def __load_stats(self, directory, file):
		"""Loads statistical information from specified file and fills primary and combination ID from the file name

		Args:
			directory (str): Path to the directory, from which to load the statistical data
			file (str): Name of the file

		Returns:
			None
		"""
		combination = file.replace(util.JSON, "").split("-")

		self.primary_id = combination[0]
		self.combination_id = combination

		self.stats = util.from_dict(util.load_json(os.path.join(directory, file)), util.Stats)

		for stat in util.to_dict(self.stats):
			setattr(self.stats, stat, np.array(getattr(self.stats, stat)))
