import logging

import prediction as prediction
import utilities as util
from boundary_detector import BoundaryDetector
from record import Record

logger = logging.getLogger("prediction_calculator")


class PredictionCalculator:
	"""Class used to calculate the prediction from various prediction methods

	Attributes:
		__node (str): On which nodetype is the prediction formulated
		__perc (float): For which percentile is the prediction formulated
		__boundary_detector (boundary_detector.BoundaryDetector): Boundary detector used for the prediction
		__baseline (prediction.Baseline): Baseline prediction method
		__closest_friend (prediction.ClosestFriend): Closest-friend prediction method
		__primary_cluster (prediction.PrimaryCluster): Primary-cluster prediction method
		__cluster_cluster (prediction.ClusterCluster): Cluster-cluster prediction method
		__polynomial_reg (prediction.PolynomialRegression): Polynomial regression prediction method
	"""

	def __init__(self, nodetype, percentile):
		self.__node = nodetype
		self.__perc = float(percentile)
		self.__boundary_detector = BoundaryDetector(nodetype, float(percentile))
		self.__baseline = prediction.Baseline(nodetype, float(percentile))
		self.__closest_friend = prediction.ClosestFriend(nodetype, float(percentile))
		self.__primary_cluster = prediction.PrimaryCluster(nodetype, float(percentile))
		self.__cluster_cluster = prediction.ClusterCluster(nodetype, float(percentile))
		self.__polynomial_reg = prediction.PolynomialRegression(nodetype, float(percentile), 3)

	def load_data(self, used_records_path=None):
		"""Loads data for each one of the prediction methods

		Args:
			used_records_path (str): Path to file containing processes which are used for testing

		Returns:
			None
		"""
		logger.info("Loading prediction calculator data")
		single_records = Record.load_single_stats(self.__node, self.__perc, used_records_path)
		comb_records = Record.load_comb_stats(self.__node, self.__perc, used_records_path)

		self.__baseline.load_data(single_records, {})
		self.__closest_friend.load_data(single_records, comb_records)
		self.__primary_cluster.load_data(single_records, comb_records)
		self.__cluster_cluster.load_data(single_records, comb_records)
		self.__polynomial_reg.load_data(single_records, {})

	def calculate_prediction(self, task_combination, evaluation=False):
		"""Calculates either the prediction used for evaluation or prediction intended for the Solver

		The prediction intended for evaluation calculates every prediction method.
		The prediction intended for production calculates only the combined prediction, which consists of the best
		performing prediction method capable of forming a prediction with provided data.

		Args:
			task_combination (list of str): List of the process IDs in the combination
			evaluation (bool, optional): Whether is the prediction intended for evaluation or production

		Returns:
			utilities.PredictionResult: The result of the prediction
		"""
		logger.info("Predicting " + str(task_combination) + "; " + str(self.__node) + "; " + str(self.__perc))
		if evaluation:
			return self.__evaluation_prediction(task_combination)
		else:
			return self.__production_prediction(task_combination)

	def set_boundary(self, boundary_percentage=None):
		"""Sets the boundary used during prediction

		Args:
			boundary_percentage (float): The percentage of maximal values of computer resources which set as boundary

		Returns:
			None
		"""
		if boundary_percentage is not None:
			self.__boundary_detector.boundary_percentage = float(boundary_percentage)
		self.__boundary_detector.calculate_boundary()

	def __evaluation_prediction(self, task_combination):
		"""Calculates prediction used during evaluation

		This prediction calculates every prediction method regardless, if it is necessary for the combined prediction

		Args:
			task_combination (list of str): List of the process IDs in the combination

		Returns:
			utilities.PredictionResult: The result of the prediction
		"""
		prediction = util.PredictionResult()

		prediction.baseline = self.__baseline.get_prediction(task_combination)
		prediction.closest_friend = self.__closest_friend.get_prediction(task_combination)
		prediction.primary_cluster = self.__primary_cluster.get_prediction(task_combination)
		prediction.cluster_cluster = self.__cluster_cluster.get_prediction(task_combination)
		prediction.polynomial_reg = self.__polynomial_reg.get_prediction(task_combination)
		prediction.exceeded_boundary = self.__boundary_detector.exceeds_boundary(task_combination)

		prediction.combined = prediction.closest_friend
		if prediction.combined is None:
			prediction.combined = prediction.primary_cluster
			if prediction.combined is None:
				prediction.combined = prediction.polynomial_reg
				if prediction.combined is None and prediction.exceeded_boundary:
					prediction.combined = prediction.cluster_cluster

		return prediction

	def __production_prediction(self, task_combination):
		"""Calculates prediction used during the production (intended for the use by Solver)

		This prediction calculates only prediction methods necessary for the combined prediction

		Args:
			task_combination (list of str): List of the process IDs in the combination

		Returns:
			utilities.PredictionResult: The result of the prediction
		"""
		prediction = util.PredictionResult()
		prediction.baseline = self.__baseline.get_prediction(task_combination)
		prediction.exceeded_boundary = self.__boundary_detector.exceeds_boundary(task_combination)

		prediction.combined = self.__closest_friend.get_prediction(task_combination)
		if prediction.combined is None:
			prediction.combined = self.__primary_cluster.get_prediction(task_combination)
			if prediction.combined is None:
				prediction.combined = self.__polynomial_reg.get_prediction(task_combination)
				if prediction.combined is None and prediction.exceeded_boundary:
					prediction.combined = self.__cluster_cluster.get_prediction(task_combination)

		return prediction
