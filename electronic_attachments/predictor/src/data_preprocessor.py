import logging
import os
import re
import shutil

import numpy as np

import paths
import utilities as util
from record import Record

logger = logging.getLogger("data_preprocessor")


class DataPreprocessor:
	"""Class used to preprocess the data matrices

	Attributes:
		normalizer (normalizer.Normalizer): Normalizer used during the preprocessing
		__node (str): On which nodetype are data matrices preprocessed
		__perc (float): For which percentile are the data matrices preprocessed
	"""

	def __init__(self, nodetype, percentile):
		self.normalizer = None
		self.__node = nodetype
		self.__perc = float(percentile)

	def preprocess_stats(self):
		"""Calculates statistical values of the measured properties, such as mean, mediean etc. from data matrices
			For each data matrix a instance of utilities.Stats is created and saved on disk as a dictionary.

		First new files, which might have been provided to the predictor are saved to the proper directory.
		A check is then executed to see if every data matrix has a corresponding utilities.Stats saved on disk.
		If not, the statistical values are recalculated.

		Returns:
			None
		"""
		self.save_new_files()
		if self.__stats_calculated():
			return

		util.clean_dir(paths.STATS_DIR(self.__node, self.__perc))
		single_records = self.__process_single_records()
		if single_records is None:
			logger.info("No single process files - stats calculation interrupted")
			return
		self.__process_combination_records(single_records)
		logger.info("Stats preprocessed")

	def normalize_stats(self, used_records_path=None):
		"""Normalize the previously calculated statistical values of measured properties of data matrices

		Args:
			used_records_path: Path to a file containing which single processes should be normalized

		Returns:
			None
		"""
		logger.info("Normalizing data")
		util.clean_dir(paths.NORMALIZED_DIR(self.__node, self.__perc))
		single_records = Record.load_single_stats(self.__node, self.__perc, used_records_path)

		if not single_records:
			logger.info("Nothing to normalize")
			return
		self.normalizer.normalize(util.data_columns_num(self.__node), single_records)

		for record in single_records:
			single_records[record].save_stats(paths.NORMALIZED_DIR(self.__node, self.__perc))

	def save_new_files(self):
		"""Saves new files, which were to the proper directory and applies the naming convention

		The name of the file consists of process IDs from the measured combination seperated by dash. The name starts
		with the primary process and has the secondary processes sorted alphabeticaly.

		Returns:
			list of str: List of IDs of added data matrices
		"""
		new_files = util.get_dirs(paths.NODETYPE_DIR(self.__node), dirs=False)
		logger.info("New files: " + str(new_files))
		added_files = []

		for file_name in new_files:
			combination = self.__get_id(file_name).split("-")
			# Sort the secondary processes in a combination
			file_name = util.sorted_name(combination)
			logger.info(file_name)

			# Create the arity directory if missing
			arity = len(file_name.split("-"))
			os.makedirs(os.path.join(paths.DATA_MATRICES_DIR(self.__node), str(arity)), exist_ok=True)

			# Rename and move the file into the directory with unpreprocessed files
			data_matrix_dir = os.path.join(paths.DATA_MATRICES_DIR(self.__node), str(arity), file_name)
			shutil.move(os.path.join(paths.NODETYPE_DIR(self.__node), file_name), data_matrix_dir)
			added_files.append(file_name)

		return added_files

	def __stats_calculated(self):
		"""Returns True if every data matrix has a corresponding utilities.Stats saved on disk, otherwise False

		Returns:
			bool: Whether the original data matrices have stats already calculated
		"""
		try:
			stats_names = Record.load_record_names(paths.STATS_DIR(self.__node, self.__perc))
			originals_names = Record.load_record_names(paths.DATA_MATRICES_DIR(self.__node))

			diff_stats = [item for item in stats_names if item not in originals_names]
			diff_originals = [item for item in originals_names if item not in stats_names]
			if len(diff_stats) != 0 or len(diff_originals) != 0:
				logger.info("Calculating stats again due to different records")
				return False
		except:
			logger.info("Some files regarding stats do not exist - calculating")
			return False

		logger.info("Stats already calculated - nodetype:" + self.__node + ", percentile:" + str(self.__perc))
		return True

	def __process_single_records(self):
		"""Calculates statistics for the single process data matrices and saves them on the disk

		Returns:
			dict[str, utilities.Stats]: A mapping of Stats classes to process IDs
		"""
		single_records = Record.load_single_data_matrices(self.__node)

		if single_records is None:
			return None

		for id in single_records:
			logger.info("Preprocessing file: " + id + util.EXTENSION)
			single_records[id].stats = self.__calculate_single_stats(single_records[id].original)
			single_records[id].save_stats(paths.STATS_DIR(self.__node, self.__perc))

		return single_records

	def __process_combination_records(self, single_records):
		"""Loads data matrices of process combinations and calculates the relative slowdown of percentile of execution
			time and saves them on the disk

		Args:
			single_records: A mapping of Stats classes to process IDs

		Returns:
			None
		"""
		comb_originals = Record.load_comb_data_matrices(self.__node, single_records)
		for id in comb_originals:
			logger.info("Preprocessing file: " + id + util.EXTENSION)
			record = comb_originals[id]
			record.stats = self.__calculate_comb_stats(record.original, single_records[record.primary_id].stats)
			record.save_stats(paths.STATS_DIR(self.__node, self.__perc))

	def __calculate_single_stats(self, original):
		"""Fills the utilities.Stats class with information from provided data matrix

		Args:
			original (numpy.ndarray): The original data matrix of a process

		Returns:
			utilities.Stats: Calculated statistical information
		"""
		stats = util.Stats()
		scaled = self.__trim_scale_data(original)

		stats.mean = np.mean(scaled, axis=0)
		stats.median = np.median(scaled, axis=0)
		stats.deviation = np.std(scaled, axis=0)
		stats.percentile = np.percentile(scaled, self.__perc, axis=0)
		stats.min = np.min(scaled, axis=0)
		stats.max = np.max(scaled, axis=0)

		# Only zeroes for these values since they require a combination
		stats.slowdown_rel = np.zeros(np.size(stats.percentile, 0))

		return stats

	def __calculate_comb_stats(self, comb_original, primary_stats):
		"""Calculates the relative slowdown of percentile of execution time in a combination of processes

		When calculating the slowdown, the utilities.Stats class is used to contain this information. This is done in
		order to achieve consistency, even though the rest of statistic in this class is unused. This way all
		of the information in regards to statistical analysis of both the single and combination data matrix is
		contained in the same class.

		Args:
			comb_original: The original combination data matrix
			primary_stats: The utilities.Stats class of the primary process in the combination

		Returns:
			utilities.Stats: Contains only the calculated slowdown, the rest of statistical values are not calculated
		"""
		comb_stats = util.Stats()
		trimmed = self.__trim_scale_data(comb_original, only_trim=True)

		percentile = np.percentile(trimmed, self.__perc, axis=0)

		# Only zeroes for these values since they require a combination
		comb_stats.slowdown_rel = np.zeros(np.size(percentile, 0))

		slowdown_rel = np.zeros(np.size(percentile, 0))

		for j in range(0, np.size(percentile, 0)):
			if (abs(primary_stats.percentile[j])) > 1e-10:
				slowdown_rel[j] = (percentile[j] - primary_stats.percentile[j]) / primary_stats.percentile[j]
			else:
				slowdown_rel[j] = 0

		comb_stats.percentile = percentile
		comb_stats.slowdown_rel = slowdown_rel

		return comb_stats

	def __trim_scale_data(self, original, only_trim=False):
		"""Deletes useless columns from the data matrix and scales the specified columns

		Some of the columns in the single and combination data matrix are not used during the preprocessing phase
		and are deleted as a result. All values preceding the "elapsed" header are deleted (contains only metadata).

		Args:
			original (numpy.ndarray): The original data matrix
			only_trim: (bool, optional): If True, only trims the data without scaling. Used for combination data
				matrice, where the scaling is not used necessary.

		Returns:
			numpy.ndarray: The trimmed and (optionally) scaled data matrix
		"""
		# throw away columns before elapsed column - only metadata
		delete_columns = [i for i in range(0, util.original_elapsed_col(self.__node))]
		trimmed = np.delete(original, delete_columns, axis=1)

		scale_position = util.scale_column(self.__node) - util.original_elapsed_col(self.__node)
		if not only_trim:
			# scale only data between elapsed and scale headers
			for i in range(0, len(trimmed)):
				trimmed[i, 1:scale_position] = trimmed[i, 1:scale_position] / trimmed[i, 0]

		return trimmed

	def __get_id(self, file_name):
		"""Get the ID of the data matrix from the name of the file, in which the data matrix is saved

		Args:
			file_name (str): Name of the file containing the data matrix

		Returns:
			str: ID of the data matrix (which process or combination of process does it belong to)
		"""
		pattern = re.compile(util.INPUT_REGEXP)
		return re.search(pattern, file_name).group()
