"""
This module contains classes, which implement the calculation of various normalization algorithms

Classes:
	Normalizer: Base class, which is inherited by classes implementing the normalization algorithms
	MinMax: Class implementing the min-max normalization
	ZValue: Class implementing the z-value normalization (also called z-score or standard normalization)
"""

import numpy as np

import utilities as util


class Normalizer:
	"""Base class, which is inherited by classes implementing the normalization algorithms

	Subclasses of Normalizer are expected to normalize the statistical information about process properties
	New normalization algorithms can be easily added by inheriting from Normalizer and overriding provided methods.

	Attributes:
		name (str): name of the normalization algorithm
	"""

	def __init__(self):
		self.name = ""

	def normalize(self, data_cols, single_records):
		"""A virtual method used to normalize statistical information of single process data matrices

		This method is provided with record.Record classes of all processes.
		The Record classes contain statistical information (utilities.Stats class) of process properties.
		This information is normalized and saved into the same utilities.Stats classes.
		The subclasses are expected to override this method.

		Args:
			data_cols (int): The number of columns containing information about process properties in the data matrix
			single_records (dict[str, record.Record]): Mapping of Records to process ID

		Returns:
			None
		"""
		pass


class MinMax(Normalizer):
	"""Class implementing the min-max normalization

	Attributes:
		name (str): name of the normalization algorithm
	"""

	def __init__(self):
		super().__init__()
		self.name = "MinMax"

	def normalize(self, data_cols, single_records):
		"""Normalizes statistical information in provided record.Record classes using min-max normalization

		This method is provided with record.Record classes of all processes.
		The Record classes contain statistical information (utilities.Stats class) of process properties.
		This information is normalized and saved into the same utilities.Stats classes.

		Args:
			data_cols (int): The number of columns containing information about process properties in the data matrix
			single_records (dict[str, record.Record]): Mapping of Records to process ID

		Returns:
			None
		"""
		for stat in util.to_dict(util.Stats()):
			for column_num in range(0, data_cols):
				records_values = []
				# Collect data from each record
				for record in single_records:
					records_values.append(getattr(single_records[record].stats, stat)[column_num])

				min = np.min(records_values)
				max = np.max(records_values)

				# normalize the value for each record
				for record in single_records:
					getattr(single_records[record].stats, stat)[column_num] = self.__min_max_formula(
						getattr(single_records[record].stats, stat)[column_num], min, max)

	def __min_max_formula(self, value, min, max):
		"""Performs the min-max normalization formula

		Args:
			value (float): The original statistical value of a given property of a given process
			min (float): The min of statistical values of given property
			max (float): The max of statistical values of given property

		Returns:
			float: The normalized value
		"""
		if max == min:
			return 0

		return (value - min) / (max - min)


class ZValue(Normalizer):
	"""Class implementing the z-value normalization

	Attributes:
		name (str): name of the normalization algorithm
	"""

	def __init__(self):
		super().__init__()
		self.name = "ZValue"

	def normalize(self, data_cols, single_records):
		"""Normalizes statistical information in provided record.Record classes using z-value normalization

		This method is provided with record.Record classes of all processes.
		The Record classes contain statistical information (utilities.Stats class) of process properties.
		This information is normalized and saved into the same utilities.Stats classes.

		Args:
			data_cols (int): The number of columns containing information about process properties in the data matrix
			single_records (dict[str, record.Record]): Mapping of Records to process ID

		Returns:
			None
		"""
		for stat in util.to_dict(util.Stats()):
			for column_num in range(0, data_cols):
				records_values = []
				# Collect data from each record
				for record in single_records:
					records_values.append(getattr(single_records[record].stats, stat)[column_num])

				mean = float(np.mean(records_values))
				deviation = float(np.std(records_values))

				# normalize the value for each record
				for record in single_records:
					getattr(single_records[record].stats, stat)[column_num] = self.__z_value_formula(
						getattr(single_records[record].stats, stat)[column_num], mean, deviation)

	def __z_value_formula(self, value, mean, deviation):
		"""Performs the z-value normalization formula

		Args:
			value (float): The original statistical value of a given property of a given process
			mean (float): The mean of statistical values of given property
			deviation (float): The deviation of statistical values of given property

		Returns:
			float: The normalized value
		"""
		if deviation == 0:
			return 0
		else:
			return (value - mean) / deviation
