"""
This module contains classes used to evaluate the predictor.

Classes:
	Evaluator: Class containing methods to evaluate the predictor
	TestSetup: Class specifying the the parameters of the test
	TestPlan: Class containing algorithms, which should be evaluated in the test
"""

import collections
import json
import logging
import os
import shutil
from copy import deepcopy

import clustering_alg as cl_alg
import clustering_score as cl_score
import distance
import normalizer
import optimizer
import paths
import utilities as util
from plotter import Plotter
from predictor import Predictor
from record import Record

logger = logging.getLogger("tester")


class Evaluator:
	"""Class containing methods to evaluate the predictor

	Attributes:
		predictor (predictor.Predictor): Instance of the predictor used during evaluation
		__node (str): For which nodetype should be the predictor evaluated
		__perc (float): For which percentile should be the predicotr evaluated
		__plotter (plotter.Plotter): Instance of the plotter used for visualization
		__setup (TestSetup): Parameters of the executed tests
		__plan (TestPlan): Algorithms used during the tests
	"""

	def __init__(self):
		self.predictor = None
		self.__node = None
		self.__perc = None
		self.__plotter = None
		self.__setup = None
		self.__plan = None

	def configure_evaluator(self, nodetype, percentile, test_setup, test_plan):
		"""Used to specify for which nodetype and percentile should be the predictor evaluated. Also specifies the
			test setup and test plan.

		Args:
			nodetype (str): For which nodetype should be the predictor evaluated
			percentile (float): For which percentile should be the predicotr evaluated
			test_setup (TestSetup): Parameters of the executed tests
			test_plan (TestPlan): Algorithms used during the tests

		Returns:
			None
		"""
		self.__node = nodetype
		self.__perc = float(percentile)
		self.__setup = test_setup
		self.__plan = test_plan
		self.predictor = Predictor(nodetype, float(percentile))
		self.__plotter = Plotter(nodetype, float(percentile))

	def weights_evaluation_corrupted(self, seed, clean_headers):
		"""Compares the the predictions with and without the use the weights on corrupted dataset

		The result of the comparison is a table containing the prediction scores and a table containing
		the cross-validation improvement for each combination of algorithms in the plan.
		Also visualizes the results of tests.

		Args:
			seed (int): Seed used during the calculation
			clean_headers (str): Path to the uncorrupted headers file in "input" directory

		Returns:
			None
		"""
		self.clean_files(clean_headers)
		corrupted_path = self.__corrupt_headers(paths.INPUT(clean_headers))
		self.predictor.assign_headers(corrupted_path)

		self.__corrupt_files()
		self.execute_plan(seed, weights_comparison=True)
		self.clean_files(clean_headers)

	def weights_evaluation_uncorrupted(self, seed, clean_headers):
		"""Compares the the predictions with and without the use the weights on uncorrupted dataset

		The result of the comparison is a table containing the prediction scores and a table containing
		the cross-validation improvement for each combination of algorithms in the plan.
		Also visualizes the results of tests.

		Args:
			seed (int): Seed used during the calculation
			clean_headers (str): Path to the uncorrupted headers file in "input" directory

		Returns:
			None
		"""
		self.predictor.assign_headers(clean_headers)
		self.clean_files(clean_headers)

		self.execute_plan(seed, weights_comparison=True)

	def execute_plan(self, seed=None, weights_comparison=False):
		"""Goes through all combinations of algorithms provided in the plan and performs either integrity test or
			weights comparison

		Args:
			seed (int): Seed used for the calculation
			weights_comparison (bool, optional): True if weights comparison should be executed instead of the integrity
				test

		Returns:
			None
		"""
		if seed is not None:
			util.set_seed(seed)

		for optimizer in self.__plan.optimizers:
			for score in self.__plan.clustering_scores:
				for alg in self.__plan.clustering_algs:
					for distance in self.__plan.distance:
						for normalizer in self.__plan.normalizers:
							for boundary in self.__plan.boundary:
								self.predictor.configure(alg, score, distance, normalizer, optimizer, boundary)
								if weights_comparison:
									self.__weights_comparions()
								else:
									self.__integrity_test()
								util.copy_eval(self.__node, self.__perc, self.predictor.get_configuration_id())

	def __weights_comparions(self):
		"""Calculates the cross validation and prediction score with and without weights

		Returns:
			None
		"""
		if not self.__duplicate_test(paths.CROSS_VALIDATION_TABLE(self.__node, self.__perc),
									 self.predictor.get_configuration_id()):
			self.predictor._calculate_cross_validation(self.__setup.xfold)

		if not self.__duplicate_test(paths.PREDICTION_SCORE_TABLE(self.__node, self.__perc),
									 self.predictor.get_configuration_id()):
			max_weights = self.predictor.calculate_weights()
			self.__visualize_weights(max_weights)
			self.__weights_test(max_weights)

	def __weights_test(self, weights):
		"""Executes integrity test with and without the weights, combines the results for visualization

		Args:
			weights (numpy.ndarray): The vector of weights to be tested

		Returns:
			None
		"""
		logger.info("Starting weights test")
		original_setup = deepcopy(self.__setup)

		# Execute integrity test with weights
		weighted_id = "weighted "
		self.__setup.weights = weights
		self.__setup.name = weighted_id
		self.__setup.headline = weighted_id
		self.__integrity_test("weights_evaluation")

		# Execute integrity test without weights
		orig_id = "no_weights "
		self.__setup.weights = util.empty_weights(self.__node)
		self.__setup.name = orig_id
		self.__setup.headline = orig_id
		self.__setup.boxplot = []
		self.__integrity_test("weights_evaluation")

		self.__setup = original_setup
		self.__combine_integrity_tests(orig_id, weighted_id, "weights_evaluation")
		self.__save_prediction_score(orig_id, weighted_id)

	def __integrity_test(self, dir=""):
		"""Executes the integrity test using the provided test setup

		The test setup specifies which integrities should be calculated and how many iterations of each integrity
		should be calculated.
		For each integrity saves the results on the disk.
		In the end, visualizes the results of the integrity test

		Args:
			dir (str): Directory name to save the generated charts

		Returns:
			None
		"""
		for integrity in self.__setup.integrities:
			logger.info("Calculating " + str(integrity) + "% integrity")

			iter_evaluations = []
			iter_verdicts = []
			for i in range(0, self.__setup.iterations):
				evaluations, predictions, verdicts = self.__calculate_integrity(integrity)
				iter_evaluations.append(evaluations)
				iter_verdicts.append(verdicts)
				logger.info("Finished " + str(integrity) + "% integrity")
			self.__finalize_evaluation(iter_evaluations, integrity)
			self.__finalize_verdicts(iter_verdicts, integrity)
			# Save only the latest set of predictions
			self.__save_predictions(predictions, integrity)

			if integrity in self.__setup.boxplot:
				self.__plotter.draw_boxplots(integrity, self.__setup.target_arities, self.__setup.name)
		self.__plotter.draw_metrics(self.__setup.name, self.__setup.headline, dir)
		self.__plotter.draw_confusion(self.__setup.name, self.__setup.headline, dir)

	def __calculate_integrity(self, integrity):
		"""Calculates a prediction for each target process in a given integrity level and evaluates the predictions

		Args:
			integrity (int): Which level of integrity is calculated

		Returns:
			list of dict[str, utilities.Metrics]: List of mappings of Metric to the names of prediction methods
			dict[str, utilities.PredictionResult]: Mapping of PredictionResult to process IDs
			list of dict[str, bool]: List of predicted and real verdict, whether the primary process satisfies the
				return trip time
		"""
		prediction_files = self.__select_integrity_prediction_files(self.__setup.prediction_arities, integrity)
		target_files = self.__select_target_files()
		self.predictor.prepare_predictor(preprocess_stats=False, weights=self.__setup.weights,
										 used_records_path=prediction_files)

		prediction_records_comb = Record.load_comb_stats(self.__node, self.__perc, prediction_files)
		target_records = Record.load_comb_stats(self.__node, self.__perc, target_files)
		single_records = Record.load_single_stats(self.__node, self.__perc)

		predictions = collections.OrderedDict()
		evaluations = []
		compared_verdicts = []
		for id in target_records:
			if id in prediction_records_comb:
				continue
			baseline = single_records[id.split("-")[0]].stats.percentile[util.stats_elapsed_col(self.__node)]
			treshold = baseline * self.__setup.treshold_percentage
			verdict, predictions[id] = self.predictor.predict_combination(id.split("-"), treshold, evaluation=True)
			predictions[id].real_value = target_records[id].stats.percentile[util.stats_elapsed_col(self.__node)]

			evaluations.append(self.__evaluate(predictions[id]))

			if verdict is not None:
				compared_verdicts.append(
					self.__compare_verdicts(verdict, predictions[id].real_value, treshold, predictions[id]))

		return evaluations, predictions, compared_verdicts

	def __compare_verdicts(self, predicted_verdict, real_value, treshold, prediction):
		"""Creates mapping of the verdict to the identifiers

		Args:
			predicted_verdict (bool): True if the predictor considers, that the primary process satisfies the time limit
			treshold (float): Value, which the real percentile can not exceed
			real_value (float): Real value of the percentile of the execution time of the primary process
			prediction (utilities.PredictionResult): The prediction provided by the predictor

		Returns:
			dict[str, bool]: The mapping of the verdict to the identifiers
		"""
		if real_value <= treshold:
			real_verdict = True
		else:
			real_verdict = False

		return {"predicted": predicted_verdict, "real": real_verdict, "exceeded_boundary": prediction.exceeded_boundary}

	def __evaluate(self, prediction):
		"""Creates an evaluation of single prediction

		For each prediction method, creates a utilities.Metrics class and fills the attributes

		Args:
			prediction (utilities.PredictionResult): The result of a single prediction

		Returns:
			dict[str, utilities.Metrics]: Mapping between the Metrics and prediction method names
		"""
		eval = util.get_empty_evaluation()
		real_value = getattr(prediction, "real_value")
		baseline = getattr(prediction, "baseline")

		for prediction_method in util.to_dict(util.PredictionResult()):
			predicted_value = getattr(prediction, prediction_method)
			if predicted_value is None:
				if prediction.exceeded_boundary:
					eval[prediction_method].out_unable_predict = 1
				else:
					eval[prediction_method].in_unable_predict = 1
			else:
				prediction_error = abs(1 - predicted_value / real_value)
				baseline_error = abs(1 - baseline / real_value)
				prediction_improvement = baseline_error - prediction_error

				if prediction.exceeded_boundary:
					eval[prediction_method].out_able_predict = 1
					eval[prediction_method].out_average_error = prediction_error
					eval[prediction_method].out_average_improvement = prediction_improvement
				else:
					eval[prediction_method].in_able_predict = 1
					eval[prediction_method].in_average_error = prediction_error
					eval[prediction_method].in_average_improvement = prediction_improvement

		return eval

	def __finalize_evaluation(self, iter_evaluation, integrity):
		"""Combined the results of all evaluations and averages them. In the end, saves the averaged evaluation on disk

		Args:
			iter_evaluation (list of list of dict[str, utilities.Metrics]): Contains the list of evaluations for each
				iteration of the integrity test on a given integrity
			integrity (int): The calculated integrity level

		Returns:
			None
		"""
		final_eval = util.get_empty_evaluation()
		prediction_num = 0
		for iter in iter_evaluation:
			prediction_num += len(iter)
			for eval in iter:
				for method in eval:
					for metric in util.to_dict(util.Metrics()):
						value = getattr(eval[method], metric)
						if not value:
							value = 0
						setattr(final_eval[method], metric, getattr(final_eval[method], metric) + value)

		for method in final_eval:
			final_eval[method].finalize()

		util.save_json(os.path.join(paths.EVAL_EVALUATIONS_DIR(self.__node, self.__perc),
									self.__setup.name + "_" + str(integrity) + util.JSON), util.encode_dict(final_eval))

	def __finalize_verdicts(self, iter_verdicts, integrity):
		"""Adds verdicts to the confusion matrix and converts the confusion categories from number of cases into
			the percentage of all cases

		Args:
			iter_verdicts (list of list of dict[str, bool]): List containing verdicts from all iterations of the
				integrity level
			integrity (int): The integrity level calculated

		Returns:
			None
		"""
		confusion = util.ConfusionMatrix()
		for iter in iter_verdicts:
			for verdict in iter:
				confusion.add_verdict(verdict)
		confusion.finalize()

		util.save_json(os.path.join(paths.EVAL_CONFUSION_DIR(self.__node, self.__perc),
									self.__setup.name + "_" + str(integrity) + util.JSON), util.to_dict(confusion))

	def __select_integrity_prediction_files(self, prediction_arities, integrity):
		"""Selects files used to calculate the predictions for given integrity and specified arities of data matrices

		Selects only files, which do not contain ground truth and lie in the specified set of arities.
		A percentage of processes is thrown away according to the level of integrity.

		Args:
			prediction_arities (list of int): Which arities should be used for the calculation
			integrity (int): Which level of integrity is calculated

		Returns:
			str: Path to the file containing the IDs of process combinations used to calculate the predictions
		"""
		logger.info("Selecting integrity prediction files")
		groundtruth_files = util.load_groundtruth_files(self.__node)
		selected_files = []

		arities_dir = util.get_dirs(paths.STATS_DIR(self.__node, self.__perc))
		for arity_dir in arities_dir:
			arity_num = os.path.basename(os.path.normpath(arity_dir))
			if int(arity_num) not in prediction_arities:
				continue
			files = os.listdir(arity_dir)
			# Do not use records containing the groundtruth (training) data
			for file in files:
				if util.cointains_groundtruth(file, groundtruth_files):
					continue
				# Throw away random files with the rate according to the integrity
				# Keep all of the single arity files
				r = util.get_random()
				if arity_dir == os.path.join(paths.STATS_DIR(self.__node, self.__perc),
											 util.SINGLE_ARITY) or r < integrity / 100:
					selected_files.append(file)

		with open(paths.INTEGRITY_PREDICTION_RECORDS(self.__node), 'w') as file:
			json.dump(selected_files, file, indent=4)
		return paths.INTEGRITY_PREDICTION_RECORDS(self.__node)

	def __select_target_files(self):
		"""Selects files used as targets of predictions. The accuracy of predictor will be compared to this set of
			process combinations.

		Selects only files, which do not contain ground truth and lie in the specified set of arities.

		Args:

		Returns:
			str: Path to the file containing the IDs of process combinations used as prediction targets
		"""
		logger.info("Selecting integrity target files")
		groundtruth_files = util.load_groundtruth_files(self.__node)
		selected_files = []

		arities_dir = util.get_dirs(paths.STATS_DIR(self.__node, self.__perc))
		for arity_dir in arities_dir:
			arity_num = os.path.basename(os.path.normpath(arity_dir))
			if int(arity_num) not in self.__setup.target_arities:
				continue
			files = os.listdir(arity_dir)
			# Do not use records containing the groundtruth (training) data
			for file in files:
				if util.cointains_groundtruth(file, groundtruth_files):
					continue
				selected_files.append(file)

		with open(paths.INTEGRITY_TARGET_RECORDS(self.__node), 'w') as file:
			json.dump(selected_files, file, indent=4)

		return paths.INTEGRITY_TARGET_RECORDS(self.__node)

	def __corrupt_headers(self, headers_file):
		"""Creates new headers file from the provided one, inserts noise properties

		Args:
			headers_file (str): Path to the headers file

		Returns:
			str: Name of the corrupted headers file in "input"
		"""
		headers = util.load_json_list(headers_file)
		for i in range(0, 9):
			headers.append("noise")

		util.save_json(os.path.join(paths.INPUT_DIR(), "corrupted_headers.json"), headers)

		return "corrupted_headers.json"

	def __corrupt_files(self):
		"""Adds noise into all data matrices

		Each data matrix is corrupted by one of three possible vectors. The vector is selected randomly and inserted
		into each one of the rows of the data matrix.
		Removes previously calculated preprocessed data matrices

		Returns:
			None
		"""
		single_records = Record.load_single_data_matrices(self.__node)
		comb_records = Record.load_comb_data_matrices(self.__node, single_records)
		comb_records.update(single_records)
		corruption = [
			[1, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 1, 1, 1, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 1, 1]
		]

		for id in comb_records:
			logger.info("Corrupting file: " + id + util.EXTENSION)
			rand = util.get_rand_int(3)
			new_matrix = []

			for row in comb_records[id].original:
				new_row = list(row) + corruption[rand]
				new_matrix.append(new_row)
			comb_records[id].original = new_matrix
			comb_records[id].save_original(paths.DATA_MATRICES_DIR(self.__node), csv=True)

		shutil.rmtree(paths.NORMALIZED_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.STATS_DIR(self.__node, self.__perc), ignore_errors=True)

	def clean_files(self, headers_path):
		"""Uses the original headers file to clean the corrupted data matrices

		Args:
			headers_path (str): Path to the file containing headers before the corruption

		Returns:
			None
		"""
		headers_path = paths.INPUT(headers_path)
		single_records = Record.load_single_data_matrices(self.__node)
		comb_records = Record.load_comb_data_matrices(self.__node, single_records)
		comb_records.update(single_records)

		# Without the scale column
		headers_num = len(util.load_json_list(headers_path)) - 1

		for id in comb_records:
			logger.info("Cleaning file: " + id + util.EXTENSION)
			new_matrix = []
			for row in comb_records[id].original:
				new_row = row[:headers_num]
				new_matrix.append(new_row)
			comb_records[id].original = new_matrix
			comb_records[id].save_original(paths.DATA_MATRICES_DIR(self.__node), csv=True)

		shutil.rmtree(paths.NORMALIZED_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.STATS_DIR(self.__node, self.__perc), ignore_errors=True)

	def __combine_integrity_tests(self, orig_id, weighted_id, dir=""):
		"""Combines the results of integrity tests with and without the weights for visualization

		For each calculated integrity, aggregate the average improvement over baseline for each prediction method
		with and without the weights, outside and inside the boundary.

		Args:
			orig_id (str): ID of the evaluation file without weights
			weighted_id (str): ID of the evaluation file with weights
			dir (str): Directory name to save the generated charts

		Returns:
			None
		"""
		orig_evals = util.load_eval(self.__node, self.__perc, orig_id)
		weighted_evals = util.load_eval(self.__node, self.__perc, weighted_id)

		for integrity in orig_evals:
			comb = {}
			orig = orig_evals[integrity]
			weighted = weighted_evals[integrity]

			for method in util.to_dict(util.PredictionResult()):
				comb[method] = {}
				comb[method]["in_orig_average_improvement"] = orig[method]["in_average_improvement"]
				comb[method]["in_weighted_average_improvement"] = weighted[method]["in_average_improvement"]
				comb[method]["out_orig_average_improvement"] = orig[method]["out_average_improvement"]
				comb[method]["out_weighted_average_improvement"] = weighted[method]["out_average_improvement"]

			util.save_json(os.path.join(paths.EVAL_EVALUATIONS_DIR(self.__node, self.__perc),
										"combined" + "_" + str(integrity) + util.JSON), comb)

		self.__plotter.draw_combined(dir)

	def __save_prediction_score(self, orig_id, weighted_id):
		"""Add a line into the table of prediction scores with and without the use of weights

		Calculate the mean of the prediction score for each prediction method across all the integrity levels,
		with and without the weights, outside and inside the boundary. Add the result into the table of prediction
		scores.

		Args:
			orig_id:
			weighted_id:

		Returns:

		"""
		orig_evals = util.load_eval(self.__node, self.__perc, orig_id)
		weighted_evals = util.load_eval(self.__node, self.__perc, weighted_id)

		in_orig = 0
		in_after = 0
		out_orig = 0
		out_after = 0

		for integrity in orig_evals:
			orig = orig_evals[integrity]
			weighted = weighted_evals[integrity]
			for method in util.WEIGHTED_PREDICTIONS:
				in_orig += orig[method]["in_average_improvement"]
				in_after += weighted[method]["in_average_improvement"]
				out_orig += orig[method]["out_average_improvement"]
				out_after += weighted[method]["out_average_improvement"]

		in_orig = (in_orig / len(orig_evals)) / len(util.WEIGHTED_PREDICTIONS)
		in_after = (in_after / len(orig_evals)) / len(util.WEIGHTED_PREDICTIONS)
		out_orig = (out_orig / len(orig_evals)) / len(util.WEIGHTED_PREDICTIONS)
		out_after = (out_after / len(orig_evals)) / len(util.WEIGHTED_PREDICTIONS)
		in_improvement = in_after - in_orig
		out_improvement = out_after - out_orig

		result = [str(in_after), str(in_orig), str(in_improvement), str(out_after), str(out_orig), str(out_improvement)]
		info = ["in_after", "in_orig", "in_improvement", "out_after", "out_orig", "out_improvement"]
		info += util.get_configuration_info()
		util.save_csv_line(info, result, self.predictor.get_configuration_id(),
						   paths.PREDICTION_SCORE_TABLE(self.__node, self.__perc))

	def __visualize_weights(self, weights):
		"""Draw the clusters of processes and ground truth with and without the weights. Also draw the weights and
			the evolution of clustering score during training.

		Args:
			weights (numpy.ndarray): The weights to draw

		Returns:
			None
		"""
		groundtruth_files = util.load_groundtruth_files(self.__node)
		util.save_json(paths.TRAIN_RECORDS(self.__node), groundtruth_files.tolist())

		# Draw groundtruth clusters before and after the use of weights
		self.__draw_weights_effect(weights, paths.TRAIN_RECORDS(self.__node), "Ground truth ")

		# Draw real processes clusters before and after the use of weights
		real_processes = self.__select_integrity_prediction_files([1], 100)
		self.__draw_weights_effect(weights, real_processes, "Processes ")

		self.__plotter.draw_weights()
		self.__plotter.draw_training_scores()

	def __draw_weights_effect(self, weights, used_records_path, name=""):
		"""Draw the effects of weights on the clustering of specified set of processes

		Args:
			weights (numpy.ndarray): The weights to use
			used_records_path (str): Path to the file containing IDs of processes to cluster
			name (str): Name used for the resulting png files

		Returns:
			None
		"""
		logger.info("Drawing effect without weights")
		self.predictor._calculate_clusters(util.empty_weights(self.__node), used_records_path)
		self.__plotter.draw_clusters(title=name + "without weights")

		logger.info("Drawing effect with weights")
		self.predictor._calculate_clusters(weights, used_records_path)
		self.__plotter.draw_clusters(title=name + "with weights")

		logger.info("Finished drawing weights effect")

	def __duplicate_test(self, table_path, configuration_id):
		"""Check whether this combination of algorithms already has a line in a table

		Args:
			table_path (str): Path to the table
			configuration_id (str): Configuration ID of the predictor (names of used algorithms)

		Returns:
			bool: True, if the test has already been calculated, otherwise False
		"""
		if not os.path.exists(table_path):
			return False

		substr = ";".join(configuration_id)
		with open(table_path) as table:
			for row in table:
				if substr in row:
					logger.info("Skipping: " + substr + " already in " + table_path)
					return True
		return False

	def __save_predictions(self, predictions, integrity):
		"""Saves a dictionary of predictions on the disk, used for boxplot drawing

		Args:
			predictions (dict[str, utilities.PredictionResult]): Mapping of combination IDs to the PredictionResults
			integrity (int): Integrity level of the predictions

		Returns:
			None
		"""
		util.save_json(os.path.join(paths.EVAL_PREDICTIONS_DIR(self.__node, self.__perc),
									self.__setup.name + "_" + str(integrity) + util.JSON),
					   util.encode_dict(predictions))


class TestSetup:
	"""Class containing the setup used for the tests

	Attributes:
		integrities (list of int): List of integrity percentages to calculate
		iterations (int): Number of iterations to be calculated
		prediction_arities (list of int): List of process combination arities to be used for the prediction calculation
		target_arities (list of int): List of process combination arities to be used as the targets of prediction
		treshold_percentage (float): Percentage of the percentile of a execution time of a process, which a
			combination of processes can not exceed
		boxplot (list of int): List of integrity levels, for which we want to draw boxplots
		weights (numpy.ndarray): Vector of weights used during the calculation
		name (str): Name of the test
		headline (str): Headline used in the charts
		xfold (int, optional): Sets the k in the k-fold cross-validation, default=3
	"""

	def __init__(self, integrities, iterations, prediction_arities, target_arities, treshold_percentage, xfold=3):
		self.integrities = integrities
		self.iterations = iterations
		self.prediction_arities = prediction_arities
		self.target_arities = target_arities
		self.treshold_percentage = float(treshold_percentage) / 100
		self.boxplot = []
		self.weights = None
		self.name = "default"
		self.headline = ""
		self.xfold = xfold


class TestPlan:
	"""Class containing the algorithms, which are intended to be tested

	__clustering_alg (list of clustering_alg.ClusteringAlg): Clustering algorithms
	__clustering_score (list of clustering_score.ClusteringScore): Clustering scores
	__distance (list of distance.Distance): Distance measures
	__normalizer (list of normalizer.Normalizer): Normalization algorithms
	__optimizer (list of optimizer.Optimizer): Optimization algorithms used to generate weights
	__boundary_percentage (list of float): Percentages of the boundary, which a combination of processes can not exceed
	"""

	def __init__(self):
		self.clustering_algs = [cl_alg.AffinityPropagation(), cl_alg.MeanShift()]
		self.clustering_scores = [cl_score.FowlkesMallows(), cl_score.VMeasure()]
		self.distance = [distance.AveragePairCorrelation(), distance.Frobenius()]
		self.normalizers = [normalizer.MinMax(), normalizer.ZValue()]
		self.optimizers = [optimizer.SimAnnealing(), optimizer.Random()]
		self.boundary = [140]
