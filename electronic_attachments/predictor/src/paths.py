"""
This module contains paths for all important dictionaries and files.
By returning paths from dedicated methods, it is possible to easily modify the names of directories and files,
or change the directory system.
We can also check the existence of all required directories each time a path is accessed.
"""

import os

import utilities as util


def DATA_DIR():
	os.makedirs(os.path.dirname(__file__) + "/../data", exist_ok=True)
	return os.path.dirname(__file__) + "/../data"


def NODETYPE_DIR(nodetype):
	os.makedirs(os.path.join(DATA_DIR(), nodetype), exist_ok=True)
	return os.path.join(DATA_DIR(), nodetype)


def DATA_MATRICES_DIR(nodetype):
	os.makedirs(os.path.join(NODETYPE_DIR(nodetype), "data_matrices"), exist_ok=True)
	return os.path.join(NODETYPE_DIR(nodetype), "data_matrices")


def NORMALIZED_DIR(nodetype, percentile):
	os.makedirs(os.path.join(NODETYPE_DIR(nodetype), "normalized", str(percentile)), exist_ok=True)
	return os.path.join(NODETYPE_DIR(nodetype), "normalized", str(percentile))


def AGGREGATE_DIR(nodetype, percentile, configuration_id):
	os.makedirs(os.path.join(os.path.dirname(__file__) + "/../aggregate", nodetype, str(percentile),
							 ".".join(configuration_id)), exist_ok=True)
	return os.path.join(os.path.dirname(__file__) + "/../aggregate", nodetype, str(percentile),
						".".join(configuration_id))


def EVAL_DIR(nodetype, percentile):
	os.makedirs(os.path.join(os.path.dirname(__file__) + "/../eval", nodetype, str(percentile)), exist_ok=True)
	return os.path.join(os.path.dirname(__file__) + "/../eval", nodetype, str(percentile))


def EVAL_PREDICTIONS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(EVAL_DIR(nodetype, percentile), "predictions"), exist_ok=True)
	return os.path.join(EVAL_DIR(nodetype, percentile), "predictions")


def EVAL_EVALUATIONS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(EVAL_DIR(nodetype, percentile), "evaluations"), exist_ok=True)
	return os.path.join(EVAL_DIR(nodetype, percentile), "evaluations")


def EVAL_CONFUSION_DIR(nodetype, percentile):
	os.makedirs(os.path.join(EVAL_DIR(nodetype, percentile), "confusions"), exist_ok=True)
	return os.path.join(EVAL_DIR(nodetype, percentile), "confusions")


def EVAL_GRAPHS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(EVAL_DIR(nodetype, percentile), "graphs"), exist_ok=True)
	return os.path.join(EVAL_DIR(nodetype, percentile), "graphs")


def EVAL_CLUSTER_GRAPHS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(EVAL_GRAPHS_DIR(nodetype, percentile), "cluster"), exist_ok=True)
	return os.path.join(EVAL_GRAPHS_DIR(nodetype, percentile), "cluster")


def EVAL_BOXPLOT_GRAPHS_DIR(nodetype, percentile, integrity):
	os.makedirs(os.path.join(EVAL_GRAPHS_DIR(nodetype, percentile), "boxplot_" + str(integrity)), exist_ok=True)
	return os.path.join(EVAL_GRAPHS_DIR(nodetype, percentile), "boxplot_" + str(integrity))


def EVAL_LINE_GRAPHS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(EVAL_GRAPHS_DIR(nodetype, percentile), "line"), exist_ok=True)
	return os.path.join(EVAL_GRAPHS_DIR(nodetype, percentile), "line")


def CROSS_VALIDATION(nodetype, percentile):
	"""Path to the file containing results of cross-validation

	The file contains clustering scores with and without weights for train and test set in all cross-validation splits.

	Args:
		nodetype (str): To which nodetype this file belongs
		percentile (float): To which percentile this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(EVAL_DIR(nodetype, percentile), "cross_validation.json")


def CROSS_VALIDATION_TABLE(nodetype, percentile):
	"""Path to the file containing aggregated results of cross-validation

	The file aggregates the average change of clustering score for cross validation test set.
	Each combination of methods (clustering, normalization, etc.) is assigned one line.

	Args:
		nodetype (str): To which nodetype this file belongs
		percentile (float): To which percentile this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(EVAL_DIR(nodetype, percentile), str(util.SEED) + "_cross_validation_table.csv")


def PREDICTION_SCORE_TABLE(nodetype, percentile):
	"""Path to the file containing aggregated prediction scores

	The file aggregates prediction scores in and out of the boundary with and without the use of weights.
	Each combination of methods (clustering, normalization, etc.) is assigned one line.

	Args:
		nodetype (str): To which nodetype this file belongs
		percentile (float): To which percentile this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(EVAL_DIR(nodetype, percentile), str(util.SEED) + "_prediction_score_table.csv")


def TRAINING_SCORES(nodetype, percentile):
	"""Path to the file containing the clustering score for each iteration of training

	Args:
		nodetype (str): To which nodetype this file belongs
		percentile (float): To which percentile this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(EVAL_DIR(nodetype, percentile), "training_scores.json")


def STATS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(NODETYPE_DIR(nodetype), "stats", str(percentile)), exist_ok=True)
	return os.path.join(NODETYPE_DIR(nodetype), "stats", str(percentile))


def MOST_SIMILAR(nodetype, percentile):
	"""Path to the file containing the ID and the distance of the closest process for each process

	Args:
		nodetype (str): To which nodetype this file belongs
		percentile (float): To which percentile this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(STATS_DIR(nodetype, percentile), "most_similar.json")


def CLUSTERS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(NODETYPE_DIR(nodetype), "clusters", str(percentile)), exist_ok=True)
	return os.path.join(NODETYPE_DIR(nodetype), "clusters", str(percentile))


def ID_TO_CLUSTER(nodetype, percentile):
	"""Path to the file containing the cluster ID for each process

	Args:
		nodetype (str): To which nodetype this file belongs
		percentile (float): To which percentile this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(CLUSTERS_DIR(nodetype, percentile), "id_to_cluster.json")


def DISTANCE_MATRIX(nodetype, percentile):
	"""Path to the file containing the matrix of distances between each pair of processes

	Args:
		nodetype (str): To which nodetype this file belongs
		percentile (float): To which percentile this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(CLUSTERS_DIR(nodetype, percentile), "distance_matrix.json")


def CLUSTER_CLUSTER_STATS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(CLUSTERS_DIR(nodetype, percentile), "cluster_cluster_stats"), exist_ok=True)
	return os.path.join(CLUSTERS_DIR(nodetype, percentile), "cluster_cluster_stats")


def CLUSTER_CLUSTER_ORIGINALS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(CLUSTERS_DIR(nodetype, percentile), "cluster_cluster_originals"), exist_ok=True)
	return os.path.join(CLUSTERS_DIR(nodetype, percentile), "cluster_cluster_originals")


def PRIMARY_CLUSTER_STATS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(CLUSTERS_DIR(nodetype, percentile), "primary_cluster_stats"), exist_ok=True)
	return os.path.join(CLUSTERS_DIR(nodetype, percentile), "primary_cluster_stats")


def PRIMARY_CLUSTER_ORIGINALS_DIR(nodetype, percentile):
	os.makedirs(os.path.join(CLUSTERS_DIR(nodetype, percentile), "primary_cluster_originals"), exist_ok=True)
	return os.path.join(CLUSTERS_DIR(nodetype, percentile), "primary_cluster_originals")


def REGRESSION_DIR(nodetype, percentile):
	os.makedirs(os.path.join(NODETYPE_DIR(nodetype), "regression_models", str(percentile)), exist_ok=True)
	return os.path.join(NODETYPE_DIR(nodetype), "regression_models", str(percentile))


def POLYNOMIAL_REG_DIR(nodetype, percentile, degree):
	os.makedirs(os.path.join(REGRESSION_DIR(nodetype, percentile), str(degree) + "_degree_polynomial"), exist_ok=True)
	return os.path.join(REGRESSION_DIR(nodetype, percentile), str(degree) + "_degree_polynomial")


def SELECTED_RECORDS_DIR(nodetype):
	os.makedirs(os.path.join(NODETYPE_DIR(nodetype), "selected_records"), exist_ok=True)
	return os.path.join(NODETYPE_DIR(nodetype), "selected_records")


def TRAIN_RECORDS(nodetype):
	"""Path to the file containing the list of process combinations used during training

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(SELECTED_RECORDS_DIR(nodetype), "train_records.json")


def TEST_RECORDS(nodetype):
	"""Path to the file containing the list of process combinations used during cross-validation testing

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(SELECTED_RECORDS_DIR(nodetype), "test_records.json")


def INTEGRITY_PREDICTION_RECORDS(nodetype):
	"""Path to the file containing the list of process combinations used to form predictions during integrity test

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(SELECTED_RECORDS_DIR(nodetype), "robustness_prediction_records.json")


def INTEGRITY_TARGET_RECORDS(nodetype):
	"""Path to the file containing the list of process combinations used as the prediction target during integrity test

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(SELECTED_RECORDS_DIR(nodetype), "robustness_target_records.json")


def PREDICTOR_RECORDS(nodetype):
	"""Path to the file containing the list of process combinations used to formulate prediction

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(SELECTED_RECORDS_DIR(nodetype), "predictor_records.json")


def INFO_DIR(nodetype):
	os.makedirs(os.path.join(NODETYPE_DIR(nodetype), "info"), exist_ok=True)
	return os.path.join(NODETYPE_DIR(nodetype), "info")


def HEADERS(nodetype):
	"""Path to the file containing the list of headers for the data matrices of a given nodetype

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(INFO_DIR(nodetype), "headers.json")


def USER_BOUNDARY(nodetype):
	"""Path to the file containing the list of user specified boundary values for each measured property

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(INFO_DIR(nodetype), "user_boundary.json")


def HEADERS_INFO(nodetype):
	"""Path to the file containing information about the data matrix headers

	The file contains the position of elapsed and scale columns, the number of weighted columns and the number
	of columns with usable data.

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(INFO_DIR(nodetype), "headers_info.json")


def BEST_WEIGHTS(nodetype, percentile):
	"""Path to the file containing the best weights from training

	Args:
		nodetype (str): To which nodetype this file belongs
		percentile (float): To which percentile this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(INFO_DIR(nodetype), "best_weights" + "_" + str(float(percentile)) + ".json")


def GROUNDTRUTH(nodetype):
	"""Path to the file containing IDs of ground truth processes together with their cluster labels

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(INFO_DIR(nodetype), "groundtruth.json")


def REQUESTED_MEASUREMENTS(nodetype):
	"""Path to the file containing process combinations, which the predictor requests to be measured

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(INFO_DIR(nodetype), "requested_measurements.json")


def BOUNDARY(nodetype):
	"""Path to the file containing maximal values used as the boundary for each measured property

	Args:
		nodetype (str): To which nodetype this file belongs

	Returns:
		str: Path to the file
	"""
	return os.path.join(INFO_DIR(nodetype), "boundary.json")


def INPUT_DIR():
	os.makedirs(os.path.dirname(__file__) + "/../input", exist_ok=True)
	return os.path.dirname(__file__) + "/../input"


def INPUT(file):
	return os.path.join(INPUT_DIR(), file)
