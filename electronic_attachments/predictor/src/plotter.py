import collections
import logging
import os

import matplotlib.pyplot as plt
import numpy as np
from sklearn import manifold

import paths
import utilities as util
from record import Record

logger = logging.getLogger("plotter")


class Plotter:
	"""Class used for the visualization of the evaluation results

	Attributes:
		__node (str): On which nodetype is the boundary analyzed
		__perc (float): For which percentile is the boundary analyzed
		__record_originals (dict[str, record.Record]): Mapping of process combination IDs to the original data matrices
		__predictions (dict[str, utilities.PredictionResult]): Mapping of process combination IDs to the prediction
			results
		__arity_ids (dict[int, str]): IDs of combinations of processes mapped to arity number
		__font_size (int): Size of the font used in charts
		__fig (matplotlib.figure.Figure): Figure used to draw the charts
	"""

	def __init__(self, nodetype, percentile):
		self.__node = nodetype
		self.__perc = float(percentile)
		self.__records_originals = collections.OrderedDict()
		self.__predictions = collections.OrderedDict()
		self.__arity_ids = collections.OrderedDict()
		self.__font_size = 25
		self.__fig = None

	def draw_boxplots(self, integrity, target_arities, name):
		"""Draws boxplots of all provided data matrices with the calculated predictions

		Draws charts for each arity:
			1) For each primary process draw chart containing all combinations with the given primary process
			2) For each pair of primary process and secondary process draw chart with combinations, which contain given
				primary and secondary process

		Args:
			integrity (int): For which integrity to draw the boxplots
			target_arities (list of int): For which arities to draw the boxplots
			name (str): Name of the chart

		Returns:
			None
		"""
		logger.info("Drawing boxplots")
		assignment = GraphAssignment()
		assignment.arity = target_arities
		assignment.name = name + "_box.png"
		assignment.title = "Integrity " + str(integrity) + "%"
		assignment.xlabel = "Processes"
		assignment.ylabel = "Milliseconds"

		self.__load_data(assignment, name + "_" + str(integrity) + util.JSON)
		normalized = Record.load_normalized_stats(self.__node, self.__perc)
		for primary in normalized:
			assignment.dir = paths.EVAL_BOXPLOT_GRAPHS_DIR(self.__node, self.__perc, integrity)
			self.__plot_all_boxplot(assignment, primary)
			for second in normalized:
				assignment.dir = os.path.join(
					paths.EVAL_BOXPLOT_GRAPHS_DIR(self.__node, self.__perc, integrity), primary)
				os.makedirs(assignment.dir, exist_ok=True)
				self.__plot_all_boxplot(assignment, primary, second)

	def draw_clusters(self, title):
		"""Visualizes the process clusters

		Loads the distance matrix and applies mutlidimensional scaling.
		Adds each process ID to be drawn as the labels with arrows and adds cluster IDs to be drawn as colors

		Args:
			title (str): Chart title

		Returns:
			None
		"""
		logger.info("Drawing clusters")

		distance_matrix = util.load_json(paths.DISTANCE_MATRIX(self.__node, self.__perc))
		id_to_cluster = util.load_json_OrderedDict(paths.ID_TO_CLUSTER(self.__node, self.__perc))

		labels = []
		colors = []
		pos = self.__multidimensional_scaling(distance_matrix)
		for id in id_to_cluster:
			labels.append(id)
			colors.append(id_to_cluster[id])

		self.__visualize(pos, colors, labels, title)

	def draw_metrics(self, name, title, dir=""):
		"""Visualizes the contents of utilities.Metrics for each integrity level

		Args:
			name (str): Name of the metrics to load
			title (str): Chart title
			dir (str): Subdirectory where to save the charts

		Returns:
			None
		"""
		logger.info("Drawing metrics")
		evals = util.load_eval(self.__node, self.__perc, name)

		for metric in util.to_dict(util.Metrics()):
			assignment = GraphAssignment()
			assignment.name = title + "_" + metric + util.PNG
			assignment.title = title + " ".join(metric.split("_"))
			assignment.xlabel = "Integrity"
			assignment.ylabel = "Percentage"
			assignment.dir = os.path.join(paths.EVAL_LINE_GRAPHS_DIR(self.__node, self.__perc), dir)
			for method in util.PREDICTIONS:
				if method in util.SKIP_METHOD:
					continue
				plot_data, xticks = self.__get_method_metric_data(evals, method, metric)
				self.__plot_line(plot_data, xticks, method)

			self.__write_output(assignment)

	def draw_confusion(self, name, title, dir=""):
		"""Visualizes the confusion matrix for each integrity level

		Args:
			name (str): Name of the confusion to load
			title (str): Chart title
			dir (str): Subdirectory where to save the charts

		Returns:
			None
		"""
		logger.info("Drawing confusion")
		evals = util.load_confusion(self.__node, self.__perc, name)

		sets = util.ConfusionMatrix.get_sets()
		for set in sets:
			assignment = GraphAssignment()
			assignment.name = title + "_confusion_" + set + util.PNG
			assignment.title = title + set + " confusion matrix"
			assignment.xlabel = "Integrity"
			assignment.ylabel = "Percentage"
			assignment.dir = os.path.join(paths.EVAL_LINE_GRAPHS_DIR(self.__node, self.__perc), dir)
			for metric in sets[set]:
				plot_data, xticks = self.__get_confusion_data(evals, metric)
				self.__plot_line(plot_data, xticks, metric)

			self.__write_output(assignment)

	def draw_combined(self, dir=""):
		"""Draws the comparison of average improvement over baseline for each prediction method

		Args:
			dir (str): Subdirectory where to save the charts

		Returns:
			None
		"""
		logger.info("Drawing combined metrics")
		evals = util.load_eval(self.__node, self.__perc, "combined")

		for prefix in ["in", "out"]:
			for method in util.to_dict(util.PredictionResult()):
				assignment = GraphAssignment()
				assignment.name = prefix + " compared" + "_" + method + util.PNG
				assignment.title = method
				assignment.xlabel = "Integrity"
				assignment.ylabel = "Percentage"
				assignment.dir = os.path.join(paths.EVAL_LINE_GRAPHS_DIR(self.__node, self.__perc), dir)
				for metric in [prefix + "_orig_average_improvement", prefix + "_weighted_average_improvement"]:
					plot_data, xticks = self.__get_method_metric_data(evals, method, metric)
					self.__plot_line(plot_data, xticks, metric)

				self.__write_output(assignment)

	def draw_training_scores(self):
		"""Draws the evolution of training scores

		Returns:
			None
		"""
		assignment = GraphAssignment()
		assignment.name = "training" + util.PNG
		assignment.title = "Weights training"
		assignment.xlabel = "Iteration"
		assignment.ylabel = "Value"
		assignment.dir = paths.EVAL_LINE_GRAPHS_DIR(self.__node, self.__perc)

		values = util.load_json_list(paths.TRAINING_SCORES(self.__node, self.__perc))
		self.__fig = plt.figure(1, figsize=(60, 20))
		self.__plot_line(values, [], "clustering_score", 0.25)
		self.__write_output(assignment)

	def draw_weights(self):
		"""Draws the column chart of weights

		Returns:
			None
		"""
		assignment = GraphAssignment()
		assignment.name = "weights" + util.PNG
		assignment.title = "Weights"
		assignment.xlabel = "Property"
		assignment.ylabel = "Value"
		assignment.dir = paths.EVAL_GRAPHS_DIR(self.__node, self.__perc)

		values = util.load_best_weights(self.__node, self.__perc)
		xticks = util.load_json_list(paths.HEADERS(self.__node))[util.original_elapsed_col(self.__node) + 1:]
		scale_position = util.scale_column(self.__node) - util.original_elapsed_col(self.__node) - 1
		xticks = list(np.delete(xticks, scale_position))
		self.__draw_bars(assignment, values, xticks)

	def __draw_bars(self, assignment, values, xticks):
		"""Draws the columns of weights

		Args:
			assignment:
			values:
			xticks:

		Returns:

		"""
		self.__fig = plt.figure(1, figsize=(22, 4))
		plt.bar(np.arange(len(xticks)), values, color='blue')
		plt.xticks(np.arange(len(xticks)), xticks)
		plt.xticks(rotation=90, size=self.__font_size)
		plt.xlim([0, len(xticks)])

		self.__write_output(assignment)

	def __load_data(self, assignment, prediction_file):
		"""Provides data for the plotter

		Loads:
			All of the data matrices
			Names of combination IDs in each arity
			predictions

		Args:
			assignment (GraphAssignment): Details of the drawn chart
			prediction_file (str): Which prediction file to load

		Returns:
			None
		"""
		logger.info("Loading data")
		for arity in assignment.arity:
			files = os.listdir(os.path.join(paths.STATS_DIR(self.__node, self.__perc), str(arity)))
			for file in files:
				record_id = file.replace(util.JSON, "")
				record = Record()
				record.load_data_matrix(os.path.join(paths.DATA_MATRICES_DIR(self.__node), str(arity)),
										file.replace(util.JSON, util.EXTENSION))
				self.__records_originals[record_id] = record

				if arity not in self.__arity_ids:
					self.__arity_ids[arity] = []
				self.__arity_ids[arity].append(record_id)

		for arity in self.__arity_ids:
			self.__arity_ids[arity] = sorted(self.__arity_ids[arity])

		self.__predictions = util.load_json(
			os.path.join(paths.EVAL_PREDICTIONS_DIR(self.__node, self.__perc), prediction_file))

	def __plot_all_boxplot(self, assignment, primary, second=None):
		"""Plots boxplots for all combinations, which contain the primary process (and optionaly the secondary process)
			for a given arity

		Args:
			assignment (GraphAssignment): Details of the drawn chart
			primary (str): ID of the primary process for which to draw the charts
			second (str): ID of the secondary process for which to draw the charts

		Returns:
			None
		"""
		logger.info("Plotting all boxplot")
		xticks = [""]
		originals = []
		predictions = collections.OrderedDict()
		seen_methods = []

		for arity in assignment.arity:
			for record_id in self.__arity_ids[arity]:
				if record_id not in self.__predictions:
					continue
				record = self.__records_originals[record_id]

				if record.primary_id != primary:
					continue

				if second is not None:
					if second not in record.combination_id[1:]:
						continue

				xticks.append(record_id)
				originals.append(record.original[:, util.original_elapsed_col(self.__node)])

				for method in self.__predictions[record_id]:
					if method in util.SKIP_METHOD:
						continue

					if method not in seen_methods:
						predictions[method] = [None]
						seen_methods.append(method)
					predictions[method].append(self.__predictions[record_id][method])

		if len(originals) == 0:
			return
		self.__plot_boxplot(originals, xticks)
		for method in seen_methods:
			self.__plot_dot(predictions[method], xticks, method)

		if second is None:
			name = primary
		else:
			name = primary + "-" + second
		self.__write_output(assignment, name)

	def __plot_ticks(self, data_size, xticks):
		"""Plots the information provided on the x-axis

		Args:
			data_size (int): Number of data points on the x-axis
			xticks (list of str): List of strings to draw on the x-axis

		Returns:
			matplotlib.axes.Axes: Instance of the drawn axes
		"""
		size = data_size / 2
		self.__fig = plt.figure(1, figsize=(size, 10))
		ax = self.__fig.add_subplot(111)

		plt.xticks(np.arange(len(xticks) + 1), xticks)
		plt.xticks(rotation=90, size=self.__font_size)
		plt.yticks(size=self.__font_size)

		return ax

	def __plot_line(self, plot_data, xticks, label, width=1.28):
		"""Adds a line to the chart

		Args:
			plot_data (list of float): Values to be drawn on the line
			xticks (list of str): Strings to be drawn on the x-axis
			label (str): Legend label
			width (float): Width position of the legend

		Returns:
			None
		"""
		ax = self.__plot_ticks(len(plot_data), xticks)
		plt.plot(plot_data, label=label, linewidth=5, color=PLOTTER_COLORS[label])
		ax.legend(loc='upper center', bbox_to_anchor=(width, 1.023),
				  fancybox=True, shadow=True, ncol=1, prop={'size': self.__font_size})

	def __plot_dot(self, plot_data, xticks, label):
		"""Adds a set of dots on the chart

		Args:
			plot_data (list of float): Values to be drawn on the dots
			xticks (list of str): Strings to be drawn on the x-axis
			label (str): Legend label

		Returns:
			None
		"""
		ax = self.__plot_ticks(len(plot_data), xticks)

		plt.plot(plot_data, label=label, linestyle='None', markersize=10, marker='o',
				 color=PLOTTER_COLORS[label])
		ax.legend(loc='upper center', bbox_to_anchor=(1.28, 1.023),
				  fancybox=True, shadow=True, ncol=1, prop={'size': self.__font_size})

	def __plot_boxplot(self, plot_data, xticks):
		"""Adds a set of boxplots to the chart

		Args:
			plot_data (list of list of float): Values to be drawn on the boxplots
			xticks (list of str): Strings to be drawn on the x-axis

		Returns:
			None
		"""
		size = len(plot_data) * ((len(plot_data) + 4) / len(plot_data))
		self.__fig = plt.figure(1, figsize=(size, 20))
		ax = self.__fig.add_subplot(111)
		ax.boxplot(plot_data, whis=[10, 90], showfliers=False)
		ax = plt.gca()
		ax.grid(True)

		plt.xticks(np.arange(len(xticks) + 1), xticks)
		plt.xticks(rotation=90)

	def __multidimensional_scaling(self, distance_matrix):
		"""Executes the multidimensional scaling algorithms to visualize the position of processes for clustering

		Args:
			distance_matrix (numpy.ndarray): A matrix containing the distances between each pair of processes

		Returns:
			numpy.ndarray: Contains the coordinates for each process
		"""
		mds = manifold.MDS(n_components=2, max_iter=3000, eps=1e-9, random_state=0,
						   dissimilarity="precomputed", n_jobs=1)
		pos = mds.fit(distance_matrix).embedding_
		return pos

	def __visualize(self, pos, colors, labels, title):
		"""Draws the clusters with arrows and labels of the process IDs

		Args:
			pos (numpy.ndarray): Coordinates for each process
			colors (list of str): Cluster ID for each process, which will be translated into color
			labels (list of str): Process ID for each process
			title (str): Chart title

		Returns:
			None
		"""
		# Plot the points and color them
		fig = plt.figure(1)
		ax = plt.axes([0., 0., 1., 1.])
		sc = plt.scatter(pos[:, 0], pos[:, 1], c=colors, s=50)

		# Plot the legend
		lp = lambda i: plt.plot([], color=sc.cmap(sc.norm(i)), ms=6,
								label="Cluster {:g}".format(i), ls="", marker="o")[0]
		handles = [lp(int(i)) for i in np.unique(colors)]
		plt.legend(handles=handles)
		# Put the legend under the graph
		ax.legend(loc='upper center', bbox_to_anchor=(1.19, 1.03),
				  fancybox=True, shadow=True, ncol=1, prop={'size': self.__font_size})

		# Plot labels with arrows indicating the combination
		for label, x, y in zip(labels, pos[:, 0], pos[:, 1]):
			plt.annotate(
				label,
				xy=(x, y), xytext=(-20, 20),
				textcoords='offset points',
				arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))

		plt.title(title, fontsize=self.__font_size - 5)
		fig.savefig(os.path.join(paths.EVAL_CLUSTER_GRAPHS_DIR(self.__node, self.__perc), title + util.PNG),
					bbox_inches='tight')

		# flush matplotlib
		plt.gcf().clear()
		plt.clf()
		plt.cla()
		plt.close()

	def __write_output(self, assignment, id=None):
		"""Saves the finished chart

		Args:
			assignment (GraphAssignment): Details of the drawn chart
			id (str): ID of the chart

		Returns:
			None
		"""
		os.makedirs(assignment.dir, exist_ok=True)

		if assignment.title is not None:
			plt.title(assignment.title, fontsize=self.__font_size)

		if assignment.xlabel is not None:
			plt.xlabel(assignment.xlabel, fontsize=self.__font_size)

		if assignment.ylabel is not None:
			plt.ylabel(assignment.ylabel, fontsize=self.__font_size)

		if id is None:
			logger.info("Plotting " + assignment.name)
			self.__fig.savefig(os.path.join(assignment.dir, assignment.name), bbox_inches='tight')
		else:
			logger.info("Plotting " + id + "_" + assignment.name)
			self.__fig.savefig(os.path.join(assignment.dir, id + "_" + assignment.name), bbox_inches='tight')

		# flush matplotlib
		plt.gcf().clear()
		plt.clf()
		plt.cla()
		plt.close()

	def __get_method_metric_data(self, evals, method, metric):
		"""Gets information of a specified prediction method and specified evaluation metric

		Args:
			evals (dict[str, dict[str, dict[str, float]]]): Mapping of evaluation metric names to the metric
			for each integrity level
			method (str): Prediction method name
			metric (str): Metric name

		Returns:
			list of float: Metrics values for each integrity percentage
			list of str: Integrity percentage values
		"""
		plot_data = []
		xticks = []
		for integrity_percentage in evals:
			xticks.append(integrity_percentage)
			plot_data.append(evals[integrity_percentage][method][metric])
		return plot_data, xticks

	def __get_confusion_data(self, confusion, metric):
		"""Gets information of a specified confusion metric

		Args:
			confusion (dict[str, dict[str, float]]): Mapping of confusion metric names to the values for each integrity
				level
			metric (str): Metric name

		Returns:
			list of float: Metrics values for each integrity percentage
			list of str: Integrity percentage values
		"""
		plot_data = []
		xticks = []
		for integrity_percentage in confusion:
			xticks.append(integrity_percentage)
			plot_data.append(confusion[integrity_percentage][metric])
		return plot_data, xticks


class GraphAssignment:
	"""Class used to specify the details of a chart
		arity (list of int): List of used arities
		name (str): Name of the chart
		title (str): Title of the chart
		xlabel (str): Name of the y-axis
		ylabel (str): Name of the x-axis
		dir (str): Subdirectory to save the results
	"""

	def __init__(self):
		self.arity = None
		self.name = None
		self.title = None
		self.xlabel = None
		self.ylabel = None
		self.dir = None


PLOTTER_COLORS = {
	"closest_friend": "r",
	"primary_cluster": "g",
	"cluster_cluster": "b",
	"combined": "c",
	"polynomial_reg": "m",
	"baseline": "y",
	"real_value": "black",
	"clustering_score": "b",
	"in_TP": "r",
	"in_FP": "g",
	"in_FN": "b",
	"in_TN": "m",
	"out_TP": "r",
	"out_FP": "g",
	"out_FN": "b",
	"out_TN": "m",
	"in_orig_average_improvement": "r",
	"out_orig_average_improvement": "r",
	"in_weighted_average_improvement": "g",
	"out_weighted_average_improvement": "g",
}
