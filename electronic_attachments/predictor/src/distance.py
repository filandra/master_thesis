"""
This module contains classes, which implement the calculation of various distance measures

Classes:
	Distance: Base class, which is inherited by classes implementing the distance measures
	Frobenius: Class implementing the Frobenius norm distance
	AveragePairCorrelation: Class implementing the average pair correlation distance
"""

import numpy as np
from scipy.linalg import norm
from scipy.spatial.distance import cdist


class Distance:
	"""Base class, which is inherited by classes implementing the distance measures

	Subclasses of Distance are expected to calculate distance from provided matrices, which represent the processes.
	In the matrices, each row represents different statistical information about process properties (e.g., mean)
	New distance measures can be easily added by inheriting from Distance and overriding provided methods.

	Attributes:
		name (str): name of the distance measure
	"""

	def __init__(self):
		self.name = ""

	def calculate(self, matrix_a, matrix_b):
		"""A virtual method for calculating distance measure from matrices, which represent the processes

		The subclasses are expected to override this method.

		Args:
			matrix_a (numpy.ndarray of float): A matrix, which contains statistical values of process properties
			matrix_b (numpy.ndarray of float): A matrix, which contains statistical values of process properties

		Returns:
			float: The distance value
		"""
		return 0


class Frobenius(Distance):
	"""Class implementing the Frobenius norm distance

	Attributes:
		name (str): name of the distance measure
	"""

	def __init__(self):
		super().__init__()
		self.name = "Frobenious"

	def calculate(self, matrix_a, matrix_b):
		"""Calculates the Frobenius norm distance

		The distance is calculated as the Frobenius norm of the difference between the two matrices.
		The provided matrices contain the mean, median and deviation of process properties

		Args:
			matrix_a (numpy.ndarray of float): A matrix, which contains statistical values of process properties
			matrix_b (numpy.ndarray of float): A matrix, which contains statistical values of process properties

		Returns:
			float: The distance value
		"""
		return norm(matrix_a - matrix_b, 'fro')


class AveragePairCorrelation(Distance):
	"""Class implementing the average pair correlation distance

	Attributes:
		name (str): name of the distance measure
	"""

	def __init__(self):
		super().__init__()
		self.name = "APCorrelation"

	def calculate(self, matrix_a, matrix_b):
		"""Calculates the average pair correlation distance

		First the correlation distance is calculated between each pair of rows from the two matrices.
		These distances are then averaged to get the final result.
		The provided matrices contain the min, max and the percentile of process properties

		Args:
			matrix_a (numpy.ndarray of float): A matrix, which contains statistical values of process properties
			matrix_b (numpy.ndarray of float): A matrix, which contains statistical values of process properties

		Returns:
			float: The distance value
		"""
		distances = np.matrix(cdist(matrix_a, matrix_b, 'correlation'))
		result = np.matrix.mean(distances)

		return result
