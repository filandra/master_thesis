import collections
import logging
import os

import numpy as np
from scipy.optimize import nnls
from sklearn.preprocessing import PolynomialFeatures

import paths
import utilities as util
from record import Record

logger = logging.getLogger("regression_analyser")


class RegressionAnalyser:
	"""Class used to implement the polynomial regression analysis

		Attributes:
			__node (str): On which nodetype is the regression analysis executed
			__perc (float): For which percentile is the regression analysis executed
			__all_records (dict[str, record.Record]): All of the Records used during the regression model calculation
			__single_records (dict[str, record.Record]): Single process Records used during the calculation
			__id_index_mapping (dict[str, int]): Mapping between the process ID and the index in row in the
				first degree version of the model matrix
			__poly (sklearn.preprocessing.PolynomialFeatures): sklearn implementation used to transform linear vector
				into polynomial
			__degree (int): Degree of the polynomial calculated
		"""

	def __init__(self, nodetype, percentile, degree):
		self.__node = nodetype
		self.__perc = float(percentile)
		self.__all_records = None
		self.__single_records = None
		self.__id_index_mapping = None
		self.__poly = PolynomialFeatures(degree=degree)
		self.__degree = degree

	def analyse_regression(self, used_records_path=None):
		"""Loads data and executes the polynomial regression analysis

		Args:
			used_records_path (str): Path to the file with process IDs used during the analysis

		Returns:
			None
		"""
		logger.info("Executing polynomial regression")
		util.clean_dir(paths.REGRESSION_DIR(self.__node, self.__perc))
		self.__load_data(used_records_path)
		self.__calculate_polynomial_reg()

	def __load_data(self, used_records_path=None):
		"""Prepares data for the regression analysis

		Args:
			used_records_path: Path to the file with process IDs used during the analysis

		Returns:
			None
		"""
		logger.info("Loading stats")
		self.__single_records = Record.load_single_stats(self.__node, self.__perc, used_records_path)
		self.__all_records = Record.load_comb_stats(self.__node, self.__perc, used_records_path)
		self.__all_records.update(self.__single_records)
		self.__id_index_mapping = self.build_id_index_mapping(self.__node, self.__perc, self.__single_records)

	def __calculate_polynomial_reg(self):
		"""Calculates the polynomial regression model for each primary process

		For each primary process the first degree polynomial model matrix and model values are calculated.
		The matrix is transformed into the desired polynomial using the sklearn implementation.
		The model matrix corresponds to matrix "A" and model values to vector "b" in the thesis.
		The coefficients (the model) are then calculated using the non-negative least squares algorithm and saved
		to disk.

		Returns:
			None
		"""
		for primary in self.__single_records:
			logger.info("Calculating multiple polynomial regression model for process" + str(primary))
			model_matrix, model_values = self.__prepare_model(primary)
			polynomial_matrix = self.__poly.fit_transform(model_matrix)
			logger.info("Polynomial matrix:")
			logger.info(polynomial_matrix)

			coeff = list(nnls(polynomial_matrix, model_values)[0])
			path = paths.POLYNOMIAL_REG_DIR(self.__node, self.__perc, self.__degree)
			util.save_json(os.path.join(path, primary), coeff)

	@staticmethod
	def build_id_index_mapping(nodetype, percentile, single_records):
		"""Creates the mapping of process ID to he index in a row of the first degree polynomial model matrix

		This mapping is used to create rows of the model matrix from provided combination data matrices.
		The mapping is created simply by iterating the sorted single process IDs.

		Args:
			nodetype (str): On which nodetype is the regression analysis executed
			percentile (float): For which percentile is the regression analysis executed
			single_records (dict[str, record.Record]): Single process Records which will be included in the model matrix

		Returns:
			dict[str, int]: Mapping between the process ID and the index in row in the first degree version of the
				model matrix
		"""
		arity_dirs = util.get_dirs(paths.STATS_DIR(nodetype, percentile))
		arity_dirs.sort()
		id_index_mapping = collections.OrderedDict()

		if not single_records:
			raise Exception("No single process files")
		else:
			single_record_names = sorted([name for name in single_records])

		counter = 0
		for name in single_record_names:
			id_index_mapping[name] = counter
			counter += 1

		logger.info("Built id to index mapping")
		return id_index_mapping

	def __prepare_model(self, primary):
		"""Creates the first degree polynomial model matrix, which is later transformed into the desired polynomial,
			and the vector of the percentile of the execution times with value for each row in the matrix.

		For each Record, if the primary process is the same as the "primary" argument, create a row in the model matrix.

		Args:
			primary (str): ID of the primary process for which we build the model matrix

		Returns:
			numpy.ndarray: The model matrix
			numpy.ndarray: The vector of the percentile of execution time for each row in the model matrix
		"""
		model_matrix = np.array([])
		model_ids = []
		model_values = []

		for id in self.__all_records:
			record = self.__all_records[id]
			if record.primary_id == primary:
				row = self.__get_matrix_vector(record.combination_id)

				if len(model_matrix) != 0:
					model_matrix = np.row_stack((model_matrix, row))
				else:
					model_matrix = row

				model_ids.append(id)
				model_values.append(record.stats.percentile[util.stats_elapsed_col(self.__node)])

		model_matrix = np.array(model_matrix)
		model_values = np.array(model_values)
		logger.info("Built model matrix")
		logger.info(model_ids)
		logger.info(model_values)
		logger.info(model_matrix)

		return model_matrix, model_values

	def __get_matrix_vector(self, combination_id):
		"""Creates one row of the model matrix

		The row is created using the process ID to index mapping. For each process in the combination of process
		increment their respective index in tchtěl he row.

		Args:
			combination_id:

		Returns:
			list of int: A row of the model matrix
		"""
		row = np.zeros(len(self.__single_records))
		# skip the primary id
		for id in combination_id[1:]:
			row[self.__id_index_mapping[id]] += 1

		return row
