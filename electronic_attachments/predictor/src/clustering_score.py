"""
This module contains classes, which implement the algorithms for the calculation of clustering score

Classes:
	ClusteringScore: Base class, which is inherited by classes implementing the scoring algorithms
	AdjustedRand: Class implementing the adjusted rand index scoring algorithm
	VMeasure : Class implementing the V-measure scoring algorithm
	FowlkesMallows: Class implementing the Fowlkes-Mallows scoring algorithm
"""

import sklearn.metrics


class ClusteringScore:
	"""Base class, which is inherited by classes implementing the scoring algorithms

	Subclasses of ClusteringScore are expected to calculate clustering score from real and calculated cluster labels.
	New scoring algorithms can be easily added by inheriting from ClusteringScore and overriding provided methods.

	Attributes:
		name (str): name of the scoring algorithm
	"""

	def __init__(self):
		self.name = ""

	def score(self, labels_true, labels_pred):
		"""A virtual method for calculating clustering score from real and calculated cluster labels

		The subclasses are expected to override this method.

		Args:
			labels_true (list of int): real labeling of processes provided through ground truth
			labels_pred (list of int): calculated labeling of processes

		Returns:
			float: A value between 0 and 1, where 0 is the worst score and 1 is the best score
		"""
		return 0


class AdjustedRand(ClusteringScore):
	"""Class implementing the adjusted rand index scoring algorithm

	Attributes:
		name (str): name of the scoring algorithm
	"""

	def __init__(self):
		super().__init__()
		self.name = "AdjustedRand"

	def score(self, labels_true, labels_pred):
		"""Calculates clustering score using adjusted rand index from real and calculated cluster labels

		Originally the adjusted rand index returns value between -1 and 1.
		This value is recalculated to range between 0 and 1.

		Args:
			labels_true (list of int): real labeling of processes provided through ground truth
			labels_pred (list of int): calculated labeling of processes

		Returns:
			float: A value between 0 and 1, where 0 is the worst score and 1 is the best score
		"""
		# adjusted_rand_score returns values between -1 and 1 - we want only positive values
		return (sklearn.metrics.adjusted_rand_score(labels_true, labels_pred) + 1) / 2


class VMeasure(ClusteringScore):
	"""Class implementing the V-measure scoring algorithm

	Attributes:
		name (str): name of the scoring algorithm
	"""

	def __init__(self):
		super().__init__()
		self.name = "VMeasure"

	def score(self, labels_true, labels_pred):
		"""Calculates v-measure clustering score from real and calculated cluster labels

		Args:
			labels_true (list of int): real labeling of processes provided through ground truth
			labels_pred (list of int): calculated labeling of processes

		Returns:
			float: A value between 0 and 1, where 0 is the worst score and 1 is the best score
		"""
		return sklearn.metrics.v_measure_score(labels_true, labels_pred)


class FowlkesMallows(ClusteringScore):
	"""Class implementing the FowlkesMallows scoring algorithm

	Attributes:
		name (str): name of the scoring algorithm
	"""

	def __init__(self):
		super().__init__()
		self.name = "Fowlkes"

	def score(self, labels_true, labels_pred):
		"""Calculates FowlkesMallows clustering score from real and calculated cluster labels

		Args:
			labels_true (list of int): real labeling of processes provided through ground truth
			labels_pred (list of int): calculated labeling of processes

		Returns:
			float: A value between 0 and 1, where 0 is the worst score and 1 is the best score
		"""
		return sklearn.metrics.fowlkes_mallows_score(labels_true, labels_pred)
