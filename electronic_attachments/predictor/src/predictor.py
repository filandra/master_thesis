"""
This module contains the Predictor class, which is the most important class in the project.
One instance of the Predictor class is intended to produce predictions for a certain nodetype and a certain percentile.
"""

import json
import logging
import os
import shutil

import paths
import utilities as util
from cluster_analyser import ClusterAnalyser
from data_preprocessor import DataPreprocessor
from prediction_calculator import PredictionCalculator
from regression_analyser import RegressionAnalyser
from weights_analyser import WeightsAnalyser

logger = logging.getLogger("predictor")


class Predictor:
	"""Main class of the project, which provides the interface to request predictions, provide data matrices and more.

	One instance of the predictor is intended to calculate predictions for a certain nodetype and a certain percentile.
	Also all of the other methods (such as assignment of headers or deletion of a process) are done in regards to the
	specified nodetype.
	All input files for the Predictor are expected to lie in the "input folder".

	Attributes:
		__node (str): ID of the nodetype, for which the predictions are calculated
		__perc (float): Value of the percentile, for which the predictions are calculated
		__registered_processes (list of str): List of process IDs for which the predictor has a single data matrix
		__data_preprocessor (DataPreprocessor): Data preprocessor used to preprocess the data matrices
		__cluster_analyser (ClusterAnalyser): Cluster analyser used to calculate the clusters
		__regression_analyser (RegressionAnalyser): Regression analyser used to calculate regression models
		__prediction_calculator (PredictionCalculator): Prediction calculator used to formulate the prediction
		__weights_analyser (WeightsAnalyser): Weights analyser used to calculate the property weights

		__clustering_alg (clustering_alg.ClusteringAlg): Implementation of a clustering algorithm
		__clustering_score (clustering_score.ClusteringScore): Implementation of a clustering score
		__distance (distance.Distance): Implementation of a distance measure
		__normalizer (normalizer.Normalizer): Implementation of a normalization algorithm
		__optimizer (optimizer.Optimizer): Implementation of a optimization algorithm used to generate weights
		__boundary_percentage (float): Percentage of the boundary, which a combination of processes can not exceed
	"""

	def __init__(self, nodetype, percentile):
		self.__node = nodetype
		self.__perc = float(percentile)
		self.__registered_processes = []
		self.__data_preprocessor = DataPreprocessor(nodetype, float(percentile))
		self.__cluster_analyser = ClusterAnalyser(nodetype, float(percentile))
		self.__regression_analyser = RegressionAnalyser(nodetype, float(percentile), 3)
		self.__prediction_calculator = PredictionCalculator(nodetype, float(percentile))
		self.__weights_analyser = WeightsAnalyser(nodetype, float(percentile), self)

		self.__clustering_alg = None
		self.__clustering_score = None
		self.__distance = None
		self.__normalizer = None
		self.__optimizer = None
		self.__boundary_percentage = None

		self.__load_registered_processes()

	def predict_combination(self, comb, time_limit, distance_latency=0, evaluation=False):
		"""Predicts the percentile of execution time of the primary process in a specified combination of processes
			and estimates if the

		Args:
			comb (list of str): List of the process IDs in a combination. The first ID in the list is the primary ID
			time_limit (int): The limit, below which the percentile of return trip time has to fit, in milliseconds
			distance_latency (int, optional): The distance latency in milliseconds, default 0
			evaluation (bool, optional): Whether the prediction is intended for evaluation or not, default False

		Returns:
			bool: True if the primary process execution time is estimated to satisfy the time limit, otherwise False
			utilities.PredictionResult: The values of the individual predictions
		"""
		comb = util.sorted_combination(comb)
		if self.__invalid_combination(comb):
			raise Exception("Invalid combination")

		prediction = self.__prediction_calculator.calculate_prediction(comb, evaluation)
		verdict = self.__time_satisfied(time_limit, prediction, distance_latency)

		if verdict == None:
			self.__assign_measurement(comb)

		return verdict, prediction

	def predict(self, assignment, distance_latency=0):
		"""Predicts whether a combination of processes can be collocated on a server

		Each process is assigned a time limit, which is the limit, below which the percentile of return trip time has
		to fit, in milliseconds.
		Prediction of percentile of execution time is calculated for each process. If all processes satisfy their
		time limit, it is estimated that the combination can be collocated.

		Args:
			assignment (dict[str, int]): Mapping between process IDs and time limits for the processes
			distance_latency (int): The distance latency in milliseconds, default 0

		Returns:
			bool: True if all process in the combination satisfy their time limit, False otherwise
		"""
		logger.info(
			"Predicting " + str(assignment) + ", nodetype:" + str(self.__node) + " percentile:" + str(self.__perc))

		verdict = True
		unable_predict = False
		combinations = self.__extract_combinations(assignment)
		for comb in combinations:
			if self.__invalid_combination(comb):
				raise Exception("Invalid combination")

			prediction = self.__prediction_calculator.calculate_prediction(comb)
			passed = self.__time_satisfied(assignment[comb[0]], prediction, distance_latency)

			if passed is None:
				self.__assign_measurement(comb)
				unable_predict = True

			if passed is False:
				verdict = False

		if unable_predict:
			return None
		else:
			return verdict

	def configure(self, clustering_alg, clustering_score, distance, normalizer, optimizer, boundary_percentage):
		"""Configures the predictor with specified algorithms and prepares predictor with the new configuration

		Args:
		__clustering_alg (clustering_alg.ClusteringAlg): Implementation of a clustering algorithm
		__clustering_score (clustering_score.ClusteringScore): Implementation of a clustering score
		__distance (distance.Distance): Implementation of a distance measure
		__normalizer (normalizer.Normalizer): Implementation of a normalization algorithm
		__optimizer (optimizer.Optimizer): Implementation of a optimization algorithm used to generate weights
		__boundary_percentage (float): Percentage of the boundary, which a combination of processes can not exceed

		Returns:
			None
		"""
		self.__clustering_alg = clustering_alg
		self.__clustering_score = clustering_score
		self.__distance = distance
		self.__normalizer = normalizer
		self.__optimizer = optimizer
		self.__boundary_percentage = boundary_percentage

		self.__cluster_analyser.clustering_alg = self.__clustering_alg
		self.__cluster_analyser.clustering_score = self.__clustering_score
		self.__cluster_analyser.distance = self.__distance

		self.__data_preprocessor.normalizer = self.__normalizer
		self.__weights_analyser.optimizer = self.__optimizer

		self.prepare_predictor(preprocess_stats=True)

		self.__prediction_calculator.set_boundary(boundary_percentage)

	def get_configuration_id(self):
		"""Returns an ID of the current configuration of the Predictor

		Used to identify under which configuration the predictions were calculated

		Returns:
			list of str: List of the names of used algorithms
		"""
		id = []
		id.append(self.__clustering_score.name)
		id.append(self.__clustering_alg.name)
		id.append(self.__distance.name)
		id.append(self.__normalizer.name)
		id.append(self.__optimizer.name)
		id.append(str(self.__boundary_percentage))

		return id

	def prepare_predictor(self, preprocess_stats=True, weights=None, used_records_path=None):
		"""Prepares the predictor for calculation of predictions

		Needs to be executed after the provision of new data matrices, after deletion of old data matrices or change of
			weights.
		Is called automatically when configurating the predictor.

		Preprocesses the data matrices, calculates the boundary, clusters and regression models and loads the necessary
		data. The preparation of Predictor can last quite a long time with large numbers of data matrices and processes.
		As a result the preparation should be executed in advance of any prediction calculation.
		
		Args:
			preprocess_stats (bool, optional): Whether to calculate the statistical information from provided data
				matrice. Optional, becuase this step can be skipped during evaluation, default True.
			weights (numpy.ndarray): A vector of weights
			used_records_path: Path to the file with process IDs and combination IDS used during the analysis

		Returns:
			None
		"""
		logger.info("Preprocessing and loading all data")
		if weights is None:
			weights = util.load_best_weights(self.__node, self.__perc)

		if preprocess_stats:
			self.__data_preprocessor.preprocess_stats()

		if used_records_path is None:
			used_records_path = self.__select_used_records()

		self.__data_preprocessor.normalize_stats(used_records_path)
		self.__prediction_calculator.set_boundary()

		self.__cluster_analyser.load_stats(used_records_path)
		self.__cluster_analyser.analyse_clusters(weights)

		self.__regression_analyser.analyse_regression(used_records_path)

		self.__prediction_calculator.load_data(used_records_path)

	def provide_data_matrix(self, data_matrix_path):
		"""Supply the Predictor with new data matrix measured on the nodetype

		Args:
			data_matrix_path (str): Path to the data matrix in the "input" folder

		Returns:
			None
		"""
		logger.info("Providing data matrix for the predictor on nodetype" + str(self.__node))
		shutil.copy(paths.INPUT(data_matrix_path), paths.NODETYPE_DIR(self.__node))

		added_files = self.__data_preprocessor.save_new_files()
		self.__update_requested_measurements(added_files)
		self.__load_registered_processes()

	def calculate_weights(self):
		"""Executes the calculation of weights

		The requirements for weights training are:
			Supply the predictor with ground truth file, which maps process IDs to cluster labels
			Supply the predictor with single data matrices of the ground truth

		This calculation can potentially last for a long time with larger numbers of processes.
		As a result the calculation of weights should be executed before any prediction calculation.

		Returns:
			numpy.ndarray: The vector of weights
		"""
		return self.__weights_analyser.calculate_groundtruth_weights()

	def delete_weights(self):
		"""Deletes the previously calculated weights

		Returns:
			None
		"""
		try:
			os.remove(paths.BEST_WEIGHTS(self.__node, self.__perc))
		except Exception:
			pass

	def assign_headers(self, headers_path):
		"""Method used to provide the headers file for the predictor

		Args:
			headers_path (str): Path to the headers file in the "input" directory

		Returns:
			None
		"""
		logger.info("Assigning headers for nodetype:" + str(self.__node))
		try:
			shutil.copyfile(paths.INPUT(headers_path), paths.HEADERS(self.__node))
		except shutil.SameFileError:
			pass

		headers = util.load_json_list(paths.HEADERS(self.__node))
		elapsed_index = headers.index(util.ELAPSED)
		scaled_index = headers.index(util.SCALE)

		headers_info = {}
		headers_info["elapsed_column"] = elapsed_index
		headers_info["scale_column"] = scaled_index
		headers_info["stats_elapsed_column"] = 0
		# Without the scale column
		headers_info["data_columns_num"] = len(headers) - elapsed_index - 1
		# Without the elapsed column
		headers_info["weights_num"] = headers_info["data_columns_num"] - 1

		util.save_json(paths.HEADERS_INFO(self.__node), headers_info)
		try:
			os.remove(paths.BEST_WEIGHTS(self.__node, self.__perc))
		except Exception:
			pass

	def assign_groundtruth(self, groundtruth_path):
		"""Method used to provide the ground truth file for the predictor

		Args:
			groundtruth_path (str): Path to the ground truth file in the "input" directory

		Returns:
			None
		"""
		logger.info("Assigning groundtruth for nodetype:" + str(self.__node))
		try:
			shutil.copyfile(paths.INPUT(groundtruth_path), paths.GROUNDTRUTH(self.__node))
		except shutil.SameFileError:
			pass

	def assign_user_boundary(self, boundary_path):
		"""Method used to provide the user boundary file for the predictor

		Args:
			boundary_path (str): Path to the user boundary file in the "input" directory

		Returns:
			None
		"""
		logger.info("Assigning user boundary for nodetype:" + str(self.__node))
		try:
			shutil.copyfile(paths.INPUT(boundary_path), paths.USER_BOUNDARY(self.__node))
		except shutil.SameFileError:
			pass

	def delete_nodetype(self):
		"""Method used to delete all information regarding a specified nodetype ID

		Returns:
			None
		"""
		logger.info("Deleting nodetype:" + str(self.__node))
		try:
			shutil.rmtree(paths.NODETYPE_DIR(self.__node))
		except:
			pass

	def delete_process(self, process_id):
		"""Method used to delete all data matrices containing the specified process in a combination

		Args:
			process_id (str): ID of the process to be deleted

		Returns:
			None
		"""
		logger.info("Deleting process " + str(process_id) + ", nodetype:" + str(self.__node))

		if process_id in self.__registered_processes:
			self.__registered_processes.remove(process_id)

		arities = util.list_dir(paths.DATA_MATRICES_DIR(self.__node))
		for arity in arities:
			files = util.list_dir(os.path.join(paths.DATA_MATRICES_DIR(self.__node), arity))
			for file in files:
				if self.__contains_process(file, process_id):
					os.remove(os.path.join(paths.DATA_MATRICES_DIR(self.__node), arity, file))

		self.__remove_requests(process_id)

	def delete_combination(self, comb):
		"""Method used to delete a specific combination

		Args:
			comb (list of str): List of process IDs in the combination, first process ID is the primary

		Returns:
			None
		"""
		logger.info("Deleting combination " + str(comb) + ", nodetype:" + str(self.__node))

		arity = len(comb)
		if arity == 0:
			return

		file_name = util.sorted_name(comb)
		try:
			os.remove(os.path.join(paths.DATA_MATRICES_DIR(self.__node), str(arity), file_name))
		except:
			pass

	def delete_percentile(self):
		"""Deletes data created to form predictions on the percentile

		Returns:
			None
		"""
		logger.info("Deleting percentile:" + str(self.__perc) + ", nodetype:" + str(self.__node))
		self.__delete_percentile_dirs()

	def get_requested_measurements(self):
		"""Returns ID of processes or combinations, which the predictor requests to improve or enable predictions

		Returns:
			list of list of str: List of combinations of IDs
		"""
		logger.info("Listing measurements requested by predictor on nodetype" + str(self.__node))
		requested = util.load_json_list(paths.REQUESTED_MEASUREMENTS(self.__node))
		if requested is []:
			requested = self.__generate_requests()

		return requested

	def get_registered_processes(self):
		"""Returns which processes have single data matrices available on the nodetype

		Returns:
			list of str: List of process IDs for which the predictor has a single data matrix
		"""
		return self.__registered_processes

	def _calculate_cross_validation(self, cross_validation_splits=3):
		"""Executes the cross-validation of weights

		The cross-validation can potentially last for a long time with larger numbers of processes.

		Args:
			cross_validation_splits (int): The number of splits (the k in the k-fold cross validation)

		Returns:
			None
		"""
		self.__weights_analyser.cross_validate(cross_validation_splits)

	def _calculate_clusters(self, weights, used_records_path=None):
		"""Executes the cluster analysis

		Args:
			weights (numpy.ndarray): The vector of weights
			used_records_path (str): Path to the file containing the process IDs used during the calculation

		Returns:
			None
		"""
		self._preload_cluster_data(used_records_path)
		self.__cluster_analyser.analyse_clusters(weights, False)

	def _preload_cluster_data(self, used_records_path=None):
		"""Loads data for cluster analysis

		Args:
			used_records_path (str): Path to the file containing the process IDs used during the calculation

		Returns:
			None
		"""
		logger.info("Preloading data for clustering")
		self.__data_preprocessor.normalize_stats(used_records_path)
		self.__cluster_analyser.load_stats(used_records_path)

	def _get_clusters_score(self, weights):
		"""Calculates the cluster analysis and returns the clustering score

		Args:
			weights (numpy.ndarray): The vector of weights

		Returns:
			float: The clustering score
		"""
		return self.__cluster_analyser.analyse_clusters(weights, True)

	def __load_registered_processes(self):
		"""Loads the names of processes with single data matrix

		Returns:
			None
		"""
		originals = util.list_dir(os.path.join(paths.DATA_MATRICES_DIR(self.__node), util.SINGLE_ARITY))
		self.__registered_processes = [file.replace(util.EXTENSION, "") for file in originals]

	def __select_used_records(self):
		"""Selects the default data matrices (Records) used for prediction calculation

		The default data matrices include all provided data matrices with the exception of those, which contain ground
		truth in the combination.

		Returns:
			str: Path to the file containing the process IDs and process combination IDs
		"""
		groundtruth_files = util.load_groundtruth_files(self.__node)
		selected_files = []
		arities_dir = util.get_dirs(paths.STATS_DIR(self.__node, self.__perc))
		for arity_dir in arities_dir:
			files = os.listdir(arity_dir)
			# Do not use records containing the groundtruth (training) data
			for file in files:
				if util.cointains_groundtruth(file, groundtruth_files):
					continue
				selected_files.append(file)

		with open(paths.PREDICTOR_RECORDS(self.__node), 'w') as file:
			json.dump(selected_files, file, indent=4)

		return paths.PREDICTOR_RECORDS(self.__node)

	def __update_requested_measurements(self, added_files):
		"""Adds new data matrices, which are requested by the predictor to be measured

		Args:
			added_files (list of str): List of the requested IDs

		Returns:
			None
		"""
		requested_measurements = util.load_json_list(paths.REQUESTED_MEASUREMENTS(self.__node))
		for file_name in added_files:
			comb = file_name.replace(util.EXTENSION, "")
			if comb in requested_measurements:
				requested_measurements.remove(comb)

		util.save_json(paths.REQUESTED_MEASUREMENTS(self.__node), requested_measurements)

	def __extract_combinations(self, assignment):
		"""Creates lists of combination IDs from provided set of process for each primary process

		Args:
			assignment (dict[str, int]): Mapping between process IDs and time limits for the processes

		Returns:
			list of list of str: List of lists of combination IDs (first process in the list is the primary one)
		"""
		# Sorted list of processes
		processes = []
		for process in assignment:
			processes.append(process)
		processes.sort(key=util.natural_keys)

		# Make each of the processes primary and the rest secondary
		combinations = []
		for i in range(0, len(processes)):
			comb = processes.copy()
			comb.insert(0, comb.pop(i))
			combinations.append(comb)

		return combinations

	def __time_satisfied(self, time_limit, predictions, distance_latency):
		"""Decides whether the primary process satisfies the required return trip time

		If the result of the combined prediction method is lower than the time limit + two times the distance latency,
		the primary process satisfies the required return trip time.

		Args:
			time_limit (int): The limit, below which the percentile of return trip time has to fit, in milliseconds
			predictions (utilities.PredictionResult): Contains results of the individual prediction methods
			distance_latency (int): The distance latency in milliseconds, default 0

		Returns:
			bool: True if the requirement on return trip time is satisfied
		"""
		predicted_value = predictions.combined
		if predicted_value is None:
			return None
		if predicted_value < time_limit + 2 * distance_latency:
			return True
		else:
			return False

	def __assign_measurement(self, comb):
		"""If the predictor was not able to calculate the prediction, add to requested measurements

		Args:
			comb (list of str): List of process IDs in the combination, first process ID is the primary

		Returns:
			None
		"""
		requested_measurements = util.load_json_list(paths.REQUESTED_MEASUREMENTS(self.__node))
		if comb not in requested_measurements and not self.__already_acquired(comb):
			requested_measurements.append(comb)
			util.save_json(paths.REQUESTED_MEASUREMENTS(self.__node), requested_measurements)

	def __invalid_combination(self, comb):
		"""Checks, whether all of the process in the combination have their single data matrix provided

		Args:
			comb (list of str): List of process IDs in the combination, first process ID is the primary

		Returns:
			bool: True if invalid, False if valid
		"""
		for process in comb:
			if process not in self.__registered_processes:
				return True
		return False

	def __remove_requests(self, process):
		"""Deletes requests containing the process on given nodetype

		Args:
			process (str): Process ID to be deleted

		Returns:
			None
		"""
		requested_measurements = util.load_json_list(paths.REQUESTED_MEASUREMENTS(self.__node))

		for request in requested_measurements:
			if process in request:
				requested_measurements.remove(request)

		util.save_json(paths.REQUESTED_MEASUREMENTS(self.__node), requested_measurements)

	def __contains_process(self, file, process):
		"""Checks, whether the file containing the data matrix contains a process ID in its combination

		Args:
			file (str): Name of the file containing the data matrix
			process (str): The process ID

		Returns:
			bool: True if the file contains the process, False otherwise
		"""
		split = file.replace(util.EXTENSION, "").split("-")
		if process in split:
			return True
		else:
			return False

	def __already_acquired(self, comb):
		"""Checks, whether data matrix of a given combination was already acquired

		Args:
			comb (list of str): List of process IDs in the combination, first process ID is the primary

		Returns:
			bool: True if already acquired, False otherwise
		"""
		arity = len(comb)
		file_name = util.sorted_name(comb)
		if os.path.exists(os.path.join(paths.DATA_MATRICES_DIR(self.__node), str(arity), file_name)):
			return True
		else:
			return False

	def __generate_requests(self):
		"""Generates requested process combinations to be measured

		Right now the implementation is quite simple. The predictor requests random combinations with already known
		arities of processes. Also requests all single proecss ground truth measurements.
		If needed, this method could be really complex in order to select the data matrices with highest probability of
		improving the predictions (e.g., by using the information about clusters).

		Returns:
			list of list of str: List of combinations of IDs
		"""

		requested_measurements = []
		groundtruth = util.load_json_OrderedDict(paths.GROUNDTRUTH(self.__node))
		for process in groundtruth:
			# Avoid duplicates
			if process not in self.__registered_processes:
				requested_measurements.append(process)

		arities = util.list_dir(paths.DATA_MATRICES_DIR(self.__node))
		# Create random combination with given arity
		for arity in arities:
			combination = []
			for i in range(0, arity):
				combination.append(util.get_rand_choice(self.__registered_processes))
			requested_measurements.append(combination)

		util.save_json(paths.REQUESTED_MEASUREMENTS(self.__node), requested_measurements)
		return requested_measurements

	def __delete_percentile_dirs(self):
		"""Clears all directories regarding the specified percentile

		Returns:
			None
		"""
		shutil.rmtree(paths.NORMALIZED_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.STATS_DIR(self.__node, self.__perc), ignore_errors=True)

		shutil.rmtree(paths.EVAL_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.EVAL_PREDICTIONS_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.EVAL_EVALUATIONS_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.EVAL_GRAPHS_DIR(self.__node, self.__perc), ignore_errors=True)

		shutil.rmtree(paths.CLUSTERS_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.CLUSTER_CLUSTER_STATS_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.CLUSTER_CLUSTER_ORIGINALS_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.PRIMARY_CLUSTER_STATS_DIR(self.__node, self.__perc), ignore_errors=True)
		shutil.rmtree(paths.PRIMARY_CLUSTER_ORIGINALS_DIR(self.__node, self.__perc), ignore_errors=True)
