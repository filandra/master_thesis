import collections
import logging
import multiprocessing as mp

import numpy as np

import paths
import utilities as util
from record import Record

logger = logging.getLogger("cluster_analyser")


class ClusterAnalyser:
	"""Class used to execute the cluster analysis and calculate most similar pairs of processes

		Attributes:
			distance (distance.Distance): Distance measure used for cluster analysis
			clustering_alg (clustering_alg.ClusteringAlg): Clustering algorithm used for cluster analysis
			clustering_score (clustering_score.ClusteringScore): Clustering score used to compare calculated and ground
				truth labeling
			__node (str): On which nodetype are the clusters analyzed
			__perc (float): For which percentile are the clusters analyzed
			__normalized_records (dict[str, record.Record]): Mapping of normalized Records to process IDs
			__comb_records (dict[str, record.Record]): Mapping of combination Records to process combination IDs
			__pool (multiprocessing.Pool): Thread pool for parallel calculation of process distances
		"""

	def __init__(self, nodetype, percentile):
		self.distance = None
		self.clustering_alg = None
		self.clustering_score = None
		self.__node = nodetype
		self.__perc = float(percentile)
		self.__normalized_records = {}
		self.__comb_records = {}
		self.__pool = mp.Pool(mp.cpu_count())

	def analyse_clusters(self, weights, training=False):
		"""Executes cluster analysis

		If training weights, the calculation of most similar pairs is omitted in order to achieve shorter execution
		time.

		Args:
			weights (numpy.ndarray of float): Vector of weights, one for each measured property
			training (bool, optional): Whether we are using cluster analysis during training or not, defaults to False

		Returns:
			None
		"""
		logger.info("Calculating clusters")
		util.clean_dir(paths.CLUSTERS_DIR(self.__node, self.__perc))
		if not self.__normalized_records:
			logger.info("Cannot calculate clusters - no normalized records")
			return

		score = self.__calculate_clusters(weights, training)
		if not training:
			self.__calculate_most_similar_pairs(weights)

		return score

	def load_stats(self, used_records_path=None):
		"""Prepares the cluster analyser data

		Args:
			used_records_path (str): Path to the file with process IDs used during the analysis

		Returns:
			None
		"""
		logger.info("Loading stats")
		self.__normalized_records = Record.load_normalized_stats(self.__node, self.__perc, used_records_path)
		self.__comb_records = Record.load_comb_stats(self.__node, self.__perc, used_records_path)

	def __calculate_most_similar_pairs(self, weights):
		"""Calculates the most similar pairs of processes

		First weighted process description is created, which consists of selected statistical values of measured
		properties, for each process with weights applied. Weights are applied by multiplying the statistical value
		with the weight value for a given property and statistic.

		For each process the other most similar (least distance) process is then calculated using the distance measure.
		Results are saved to disk.

		Args:
			weights (numpy.ndarray of float): Vector of weights, one for each measured property

		Returns:
			None
		"""
		logger.info("Calculating most similar pairs")
		most_similar_pairs = collections.OrderedDict()
		weighted_descriptions = self.__get_weighted_descriptions(self.__normalized_records, weights)

		for id in weighted_descriptions:
			closest_id, distance = self.__get_most_similar(id, weighted_descriptions[id], weighted_descriptions)
			most_similar_pairs[id] = util.ClosestId(closest_id, distance)

			util.save_json(paths.MOST_SIMILAR(self.__node, self.__perc), util.encode_dict(most_similar_pairs))

	def __get_most_similar(self, process_id, process_description, weighted_descriptions):
		"""Calculates the most similar (least distant) pair of processes

		Args:
			process_id (str): Process for which the most similar process is found
			process_description (numpy.ndarray of float): A matrix containing normalized and weighted statistical
				information about the measured properties of a process
			weighted_descriptions (dict[str, numpy.ndarray of float]): Mapping of process descriptions to process ID

		Returns:
			str: ID of the most similar process
			float: The distance value of the most similar process
		"""
		closest_id = None
		min_distance = None

		for id in weighted_descriptions:
			# if it is not the same record
			if id != process_id:
				distance = self.distance.calculate(process_description, weighted_descriptions[id])

				if min_distance == None:
					closest_id = id
					min_distance = distance
				else:
					if min_distance > distance:
						closest_id = id
						min_distance = distance

		return closest_id, min_distance

	def __calculate_clusters(self, weights, training):
		"""Calculates clusters with provided weights and calculates primary-cluster and cluster-cluster data if training

		If calculating clusters only for weights training purposes, the primary-cluster and cluster-cluster data does
		not have to be calculated. On the other hand, the clustering score is calculated only for training purposes.

		Args:
			weights (numpy.ndarray of float): Vector of weights, one for each measured property
			training (bool, optional): Whether we are using cluster analysis during training or not, defaults to False

		Returns:
			float: The clustering score
		"""
		process_description = self.__get_weighted_descriptions(self.__normalized_records, weights)
		distance_matrix = self.__calculate_distance_matrix(process_description)
		id_to_cluster = self.__calculate_id_to_cluster(self.__normalized_records, distance_matrix)

		if training:
			score = self.__compare_to_groundtruth(id_to_cluster)
			logger.info("Clustering score: " + str(score))
		else:
			score = 0
			util.save_json(paths.DISTANCE_MATRIX(self.__node, self.__perc), distance_matrix.tolist())
			util.save_json(paths.ID_TO_CLUSTER(self.__node, self.__perc), id_to_cluster)

			if not self.__comb_records:
				logger.info("Cannot calculate cluster interaction - no combination records")
			else:
				self.__calculate_clusters_interaction(self.__comb_records, id_to_cluster, primary=False)
				self.__calculate_clusters_interaction(self.__comb_records, id_to_cluster, primary=True)

		return score

	def __get_weighted_descriptions(self, normalized_records, weights):
		"""Creates process description using normalized single data matrices and weights

		If the APCorrelation distance measure is used, the process description consists of min, max and percentile
		statistics in the form of a matrix. Otherwise mean, median and deviation is used.
		Weights are applied on each of the statistic.

		Args:
			normalized_records (dict[str, record.Record]): Mapping of normalized Records to process IDs
			weights (numpy.ndarray of float): Vector of weights, one for each measured property

		Returns:
			dict[str, numpy.ndarray of float]: Mapping of process descriptions (a matrix) to process IDs
		"""
		weighted_descriptions = collections.OrderedDict()

		if self.distance.name == "APCorrelation":
			# Time information is disregarded
			for id in normalized_records:
				weighted_descriptions[id] = np.array(
					[self.__weight(normalized_records[id].stats.percentile[1:], weights),
					 self.__weight(normalized_records[id].stats.max[1:], weights),
					 self.__weight(normalized_records[id].stats.min[1:], weights)])
		else:
			# Time information is disregarded
			for id in normalized_records:
				weighted_descriptions[id] = np.array(
					[self.__weight(normalized_records[id].stats.mean[1:], weights),
					 self.__weight(normalized_records[id].stats.median[1:], weights),
					 self.__weight(normalized_records[id].stats.deviation[1:], weights)])

		return weighted_descriptions

	def __calculate_distance_matrix(self, weighted_descriptions):
		"""Calculates distance for each pair of processes

		Thread pool is used to calculate the distances between processes in parallel. The results are then formed into
		a matrix.

		Args:
			weighted_descriptions (dict[str, numpy.ndarray of float]): Mapping of process descriptions to process IDs

		Returns:
			numpy.ndarray of float: A matrix containing distance between each pair of processes
		"""
		inputs = []
		i = 0
		j = 0
		for id_a in weighted_descriptions:
			for id_b in weighted_descriptions:
				if j > i:
					break
				inputs.append((weighted_descriptions[id_a], weighted_descriptions[id_b]))
				j += 1
			i += 1
			j = 0

		distances = self.__pool.starmap(self.distance.calculate, inputs)

		process_num = len(weighted_descriptions)
		distance_matrix = np.zeros((process_num, process_num))
		counter = 0
		for i in range(0, process_num):
			for j in range(0, process_num):
				if j > i:
					break
				distance_matrix[i, j] = distances[counter]
				distance_matrix[j, i] = distances[counter]
				counter += 1

		return distance_matrix

	def __calculate_id_to_cluster(self, normalized_records, distance_matrix):
		"""Assigns cluster number to process IDs

		Args:
			normalized_records (dict[str, record.Record]): Mapping of Records to process IDs
			distance_matrix (numpy.ndarray of float): A matrix containing distance between each pair of processes

		Returns:
			dict[str, int]: Mapping of the cluster number to the process ID
		"""
		id_to_cluster = collections.OrderedDict()
		clusters_list = self.clustering_alg.cluster(distance_matrix)

		i = 0
		for id in normalized_records:
			id_to_cluster[id] = str(clusters_list[i])
			i += 1

		return id_to_cluster

	def __calculate_clusters_interaction(self, comb_records, id_to_cluster, primary):
		"""Calculates the primary-cluster or cluster-cluster data

		Calculates the interaction between a primary process and the secondary clusters, or between the primary cluster
		and the secondary clusters.
		For each combination of processes the primary-cluster or cluster-cluster ID is generated.
		For example the A-B-C combination can have A-0-1 primary cluster ID and 1-0-1 cluster-cluster ID.
		Relative slowdown with the same cluster ID is aggregated and statistics are calculated from the aggregate.
		The results are saved to disk.

		Args:
			comb_records (dict[str, record.Record]): Mapping of Records to processes combination IDs
			id_to_cluster (dict[str, int]): Mapping of the cluster number to the process ID
			primary (bool): True if the primary-cluster data is calculated, False if the cluster-cluster data is
				calculated

		Returns:
			None
		"""
		interaction_aggregate = collections.OrderedDict()
		for id in comb_records:
			record = comb_records[id]
			cluster_id = Record.get_cluster_id(record.combination_id, id_to_cluster, primary)

			if cluster_id not in interaction_aggregate:
				interaction_aggregate[cluster_id] = []
			val = record.stats.slowdown_rel[util.stats_elapsed_col(self.__node)]
			interaction_aggregate[cluster_id].append(val)

		for cluster_id in interaction_aggregate:
			id_split = cluster_id.split("-")

			cluster_record = Record()
			cluster_record.primary_id = id_split[0]
			cluster_record.combination_id = id_split
			cluster_record.original = interaction_aggregate[cluster_id]
			cluster_record.stats = self.__calculate_cluster_stats(interaction_aggregate[cluster_id])

			if primary:
				cluster_record.save_stats(paths.PRIMARY_CLUSTER_STATS_DIR(self.__node, self.__perc))
				cluster_record.save_original(paths.PRIMARY_CLUSTER_ORIGINALS_DIR(self.__node, self.__perc))
			else:
				cluster_record.save_stats(paths.CLUSTER_CLUSTER_STATS_DIR(self.__node, self.__perc))
				cluster_record.save_original(paths.CLUSTER_CLUSTER_ORIGINALS_DIR(self.__node, self.__perc))

	def __calculate_cluster_stats(self, interaction_aggregate):
		"""Statistical information is extracted from the aggregate of slowdowns

		Args:
			interaction_aggregate (list of float): List of relative slowdown values with the same primary-cluster ID or
				cluster-cluster ID

		Returns:
			utilities.Stats: Statistical values extracted from the aggregate
		"""
		stats = util.Stats()

		stats.mean = np.mean(interaction_aggregate, axis=0)
		stats.median = np.median(interaction_aggregate, axis=0)
		stats.deviation = np.std(interaction_aggregate, axis=0)
		stats.percentile = np.percentile(interaction_aggregate, self.__perc, axis=0)
		stats.min = np.min(interaction_aggregate, axis=0)
		stats.max = np.max(interaction_aggregate, axis=0)

		return stats

	def __compare_to_groundtruth(self, id_to_cluster):
		"""Returns clustering score for the calculated cluster labeling

		The ground truth labeling is extracted for the same set of processes and the clustering score is calculated
		between the ground truth labeling and calculated labeling.


		Args:
			id_to_cluster (dict[str, int]): Mapping of the cluster number to the process ID

		Returns:
			None
		"""
		groundtruth = util.load_json_OrderedDict(paths.GROUNDTRUTH(self.__node))

		labels_pred = [id_to_cluster[id] for id in id_to_cluster]
		labels_true = [groundtruth[id] for id in id_to_cluster]

		return self.clustering_score.score(labels_true, labels_pred)

	def __weight(self, vector, weights):
		"""Applies weights to a vector

		The weights and the vector are required to have the same dimensions

		Args:
			vector (numpy.ndarray of float): 1D vector
			weights (numpy.ndarray of float): 1D vector

		Returns:
			numpy.ndarray of float: 1D vector
		"""
		v = np.array(vector)
		w = np.array(weights)

		return v * w
