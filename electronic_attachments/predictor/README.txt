Predictor
=========
The predictor of percentile of execution time created for the thesis.
We chose Python to implement the project because of the vast amount of available libraries, which can be used to implement many of the functionalities.  
The used libraries include: matplotlib, numpy, scipy and sklearn.
As a result, Python 3.5 (or higher) and the specified libraries are needed in order to use the Predictor.

The directory "data" contains all files used during the prediction calculation and weights training.
The directory "data/nodetype_1/data_matrices" contains data matrices used in the chapter Evaluation.
The directory "input" contains files which can be provided to the Predictor
The directory "src" contains the source code of the Predictor together with usage example.
The directory "eval" contains the result of the latest test
The directory "aggregate" aggregates eval directories for each test

The documentation can be generated from provided docstrings. The documentation is not pre-generated due to the restrictions on file extensions, when submiting electronic attachements to SIS.

Setup:
======
When setting up the Predictor, the user needs to do five things:  
1) Import the Predictor class from predictor.py 
2) Create an instance of Predictor for a specific node type and percentile  
3) Supply the Predictor with data matrices, headers for the data matrices, ground truth specification and user boundary  
4) Configure the Predictor using chosen algorithms
5) Preprocess the data and prepare the Predictor for predictions  

An example of this setup is provided in the usage example, together with the explanation of used methods and parameters.  
After setting up the Predictor, the prediction requests can be issued.


The predictor project also consists of a predictor evaluator. To set up the Evaluator, the user needs to:
1) Import the Evaluator class from evaluator.py  
2) Create TestSetup, which configures the tests executed by the Evaluator  
3) Create TestPlan, which configures which combination of algorithms should be evaluated during the tests
4) Create an instance of the Evaluator for a specific node type and percentile using the TestSetup and TestPlan
5) Setup the Predictor in the Evaluator
6) Run the tests

An example of the evaluator setup is provided in the usage example. The example generates all charts and tables shown in the thesis.  
The results can be found in the "eval" or "aggregate" (aggregates results of "eval" for different combinations of algorithms) directories.

To run the usage example, simply execute "python usage_example.py" or a similar alternative of such a command.  
In case the user wanted to create other tests, the usage_example.py contains instructions on how to create new scenarios.



Input file formats:
===================
Examples of the data matrix, headers file the user boundary file and ground truth specification is provided in the "input" directory.


Data matrix:
------------
As already described, the Predictor needs to be provided with data matrices. Each row in the data matrix represents one run of a process and the columns describe various properties of the process (such as execution time, number of instructions executed, etc.). The expected format of the data matrix is a standard .csv format with the character ";" as a separator.

The name of the data matrix indicates which combination of processes has been measured. The naming convention is explained in the thesis (Section 3.3). The naming of data matrices can be also explained as a regexp: [A-Za-z]*[0-9]*(-[A-Za-z]*[0-9]*)*

During the preprocessing, some of the measured properties need to be scaled by the length of execution time in a given run. Consequently, each data matrix has to provide a column with the measured length of execution time, and it is also necessary to specify which properties are intended to be scaled. As a result, the Predictor has to be provided with a headers file.


Headers file:
-------------
The headers file provides information about the positiion of column with the measured length of execution time and the informatoin about which columns should be scaled. The headers file is a standard .json file, which contains a list of string, for example:

[run, elapsed, instructions, disk_sectors, scale, noise]

The contents of the file include: 
1) First, the headers file contains IDs of columns containing metadata, which are not used during the calculation
2)  The end of metadata columns is denoted by the "elapsed" column, which contains the execution time of the process in a given run. This column needs to be present in each data matrix.
3)  IDs of columns, which need to be scaled, are present between the "elapsed" and "scale" identifier.
4)  The "scale" identifier, which is the only identifier, which does not correspond to a column in the data matrix. This identifier only serves to separate the scalable and non-scalable data matrix properties.
5)  IDs of columns, which are not supposed to be scaled, are present after the "scale" identifier.


User boundary file:
-------------------
The format of the user boundary file is very similar to the format of the headers file described above. The user boundary file is a standard .json file, which contains a list of float, for example:

[0, 0, 5000000, 1000, 0, 16]

For each ID in the headers file a corresponding value of the user-specified maximal value is provided. For the sake of consistency, values for metadata, elapsed time and the scale identifier are also provided, however, these are ignored during the calculation.


Ground truth file:
------------------
In the ground truth file, the user specifies which ground truth processes belong to which clusters. This file is a standard .json file, which contains a dictionary mapping the ground truth processes to the clusters, for example:

{"C50": "1", "D50": "2"}
