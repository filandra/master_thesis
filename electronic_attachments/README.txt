Latency aware deployment in the edge-cloud environment
======================================================


Electronic attachments
-----------------------

The attachments consist of:

predictor: The predictor project, which consists of the source codes, data matrices, user documentation (README.txt) and a usage example.

ULS: Source code of the universal load simulator used to generate the ground truth processes.

process_source_codes: Source codes and JARs of the processes measured in data matrices.
