# Master Thesis
My master thesis utilizes similarity analysis, cluster analysis, regression analysis and data feature weight approximation using supervised learning. The result is a predictor of an application performance in the edge-cloud environment.

Implemented using Python and Java.  
The thesis was awarded with the highest possible grade.

Overview:  
Edge-cloud computing is a new computing paradigm, which decentralizes computing to nodes that are geographically closer to client (compared the the traditional centralized cloud environments). Thanks to this decentralization, it is possible to achieve smaller communication latencies. However, for latency-sensitive applications (like augmented reality or coordinated movement of autonomous devices) the dislocation to edge-nodes is not enough as the end-to-end response is heavily influenced by the computation on the nodes. Thus in addition to cutting the communication latencies, it is necessary to provide guarantees on computation time for the workload process in the edge-cloud.
This calls for an additional layer on top of the edge-cloud, which manages deployment and redeployment of services in edge-cloud based on specification of timing requirements of individual services running the cloud. The goal of this thesis is to propose such a layer, which, given the knowledge of services' performance and resource consumption, predicts service's response time and finds optimal deployment (with regards to the application's real-time requirements on communication latencies).
