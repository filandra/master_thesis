%Done
\chapter{Cloud and edge-cloud computing explained}
\label{Edge}
\smallskip
In this chapter, we explore and compare cloud and edge-cloud computing. Then we examine the relation of both paradigms to latency-sensitive applications and explain in detail what problem our thesis focuses on. 

\section{Cloud computing}
\smallskip
Cloud computing started gaining prominence since its inception around 2005\cite{History}. It has been identified as the next computing paradigm in the evolution from mainframes to PCs, networked computing, the internet and grid computing, with effects as profound as the move from mainframes to PCs\cite{Importance}. 

\smallskip
The US National Institute of Standards and Technology (NIST) defines cloud computing as\cite{NIST}: \textit{Cloud computing is a model for enabling ubiquitous, convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction.}

\smallskip
Cloud computing systems are usually implemented as centralized data centers, in which the cloud provider manages servers, which host the customers' activity. To support all of the required features, a range of other existing technologies are combined, such as parallel computing, distributed computing, virtualization, containerization, orchestration and many more. As a result, we can think about the cloud system as a collection of hardware and software running in a data center that enables cloud computing\cite{Thinking}.

\smallskip
A customer connects to the cloud system, usually through the Internet, and is provided with various cloud services. One of the implications of the NIST definition is, that the cloud computing paradigm transforms computing power into a service with certain key properties, for which a customer pays according to use. This is in contrast with computing power being hardware, for which the customer pays an upfront cost. Computing power as a service is not a new idea (e.g., renting a server), thus the properties which these services are required to satisfy, in order to be classified as cloud services, is what makes cloud computing novel and appealing\cite{Research}:

\begin{itemize}[noitemsep]
\item On-demand self-service: A customer can acquire server time, storage, or other computing capabilities automatically without the need of other human intervention.

\item Broad network access: The customer accesses the cloud services over the network through standard mechanisms using a client platform (mobile, laptop, etc.).

\item Resource pooling: The provider pools the computing resources to serve multiple customers. Different physical and virtual resources are dynamically assigned and reassigned according to customer demand.

\item Location independence: The customer has no knowledge of the exact location of the provided resources (the infrastructure is transparent to him). However, the customer might be able to specify the broad location in which he wishes to acquire the service (e.g., western Europe).

\item Rapid elasticity: The acquired services can be automatically provisioned or released in order to scale according to demand.

\item Measured service: Resource usage (such as storage, bandwidth use, etc.) is monitored, controlled and reported in order to better inform the customer and the provider and to enable the optimization of resource use.
\end{itemize}

\smallskip
The services provided to the customer are typically divided by the level of responsibility, which the provider adopts, into three categories\cite{Cloud}:

\begin{itemize}[noitemsep]
\item Software-as-a-Service (SaaS): Customers of this cloud service only make use of an application hosted by the cloud provider, which can be accessed using clients such as a web browser or dedicated application. The customer does not manage the application or the infrastructure (such as servers, operating systems, storage, etc.). Thus the service, for which the customer pays, is the ability to use a given software application. Examples of this model include DropBox, Slack, Microsoft Office 365 and Google Apps for Work.

\item Platform-as-a-Service (PaaS): Customers of this cloud service can release their applications, developed using programming languages, libraries and tools supported by the cloud provider in a hosting environment. Users can again access these applications through various clients. Similarly to SaaS, the customer does not have control over the underlying infrastructure, but the customer has control over his deployed applications. Thus the service, for which the customer pays, is the ability to develop and deploy applications using a platform managed by the cloud provider. Examples of this model include Microsoft Azure and Google App Engine.

\item Infrastructure-as-a-Service (IaaS): The cloud provider manages the processing, storage, networking, power, security, virtualization and other aspects of the infrastructure, which the customer uses to run arbitrary software. The customer has limited or no ability to manage underlying physical infrastructure. Thus the service, for which the customer pays, is the ability to use remote hardware managed by the cloud provider. Examples of this model include Amazon Web Services’ EC2 and S3.
\end{itemize}

\smallskip
Cloud computing systems are also divided into different categories depending on who owns and who uses them\cite{NIST}:
\begin{itemize}[noitemsep]
\item Private cloud: The cloud system is created for and used by a single organization. This organization, some third party, or a combination of them own the system.
\item Community cloud: The cloud system is created for and used by a specific community (e.g., academic community). An organization from this community, some third party, or a combination own the system.
\item Public cloud: The cloud system is created for general use by the public. It is owned by a business, academic, or government organization or a combination of those.
\item Hybrid cloud: This cloud system is a composition of more than one distinct cloud system (public, community or private), which are bound together by a technology that enables the portability of data and applications.
\end{itemize}

\subsubsection{Advantages and disadvantages}
\smallskip
Key advantages of cloud computing consist of scalability, the gradual payment model, cost-efficiency and ease of use.

\smallskip
Key disadvantages of cloud computing consist of issues with downtime, security, privacy, limited control over infrastructure, flexibility and vendor lock-in. Vendor lock-in is the problem of transferring activity from one cloud provider to another. It can be divided into risks of transfer of data, applications, infrastructure and human resource knowledge risk. In regards to latency-sensitive applications, the main problem of cloud computing is the significant distance latency between the user and the data center and the lack of guarantees on the performance of an application. 

\smallskip
It is clear that without sufficient planning and carefully executed development, the use of a cloud computing system can become problematic. However, as we can see by the wide adoption of this computing paradigm, the positives outweigh the negatives significantly.

\section{Edge-cloud computing}
\smallskip
One can think of edge-cloud computing as an extension of the idea of cloud computing explored in the previous section. Multiple definitions of edge-cloud computing exist, such as: 

\begin{itemize}[noitemsep]
\item \textit{Edge-cloud computing is where compute resources, ranging from credit-card-size computers to micro data centers, are placed closer to information-generation sources, to reduce network latency and bandwidth usage generally associated with cloud computing.}\cite{Microsoft}
\item \textit{Edge-cloud computing consists of any computing and network resources along the path between data sources and cloud data centers.}\cite{Edge}
\end{itemize}

\smallskip
In general, the main idea behind edge-cloud computing is to decentralize the computing power from a few large data centers into many smaller computational units (micro data centers (MDCs), cloudlets, devices, etc.) closer to the users (the data generators). This is done in order to reduce the latency between the user and the data center and to distribute the network usage into multiple locations. 

\smallskip
When compared, edge-cloud computing could be, in the broad sense, almost identical to cloud computing, with the only difference being that the data centers are decentralized. Technically, the two systems would differ significantly, however, the main concepts of cloud computing still apply, such as the properties of cloud services, the concepts of SaaS, PaaS and IaaS and the differentiation of cloud systems into the public cloud, private cloud, etc.

\smallskip
There are also other architectures of edge-cloud computing systems possible, such as edge-cloud being a layer, in which only partial computation occurs between the user and the centralized data center, such as in Figure \ref{ThreeLayer}. Thus cloud computing and edge-cloud computing do not have to be exclusive but can complement each other. However, in this thesis, we only consider the edge-cloud systems with two layers -- the MDCs and the users. For our purposes, the three-layer design does not make much difference.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{../img/cloud.png}
  \caption{Cloud computing compared to three-layer edge-cloud computing\cite{Edge}.}
  \label{ThreeLayer}
\end{figure}

\subsubsection{Importance of edge-cloud computing for the future}
\smallskip
One of the main reasons behind the advent of edge-cloud computing is the change of users from being only data consumers into being both data consumers and data producers\cite{Edge}. The amount of data created by people and machines on the edge of the network is steadily increasing, and with that comes increased demand in data processing and other related computationally demanding tasks.

\smallskip
There is a strong incentive to use cloud computing to process the data in order to save the battery life of the devices generating this data and increase the processing speed. However, to process all this data in centralized data centers would create a heavy load on the network infrastructure. Also, latency-sensitive applications would experience problems due to the vast distances between the user and the data center. As a result, edge-cloud is needed to satisfy this increased demand in computing.

\smallskip
Examples of promising edge-cloud use cases include IoT data processing, implementation of smart home/city, augmented reality and self-driving cars/drones\cite{Edge} as visualized in Figure \ref{Drone}.

\begin{figure}
  \centering
    \includegraphics[width=0.9\textwidth]{../img/drone.png}
  \caption{Ilustration of a farming drone equipped with sensors and simple computer using an edge-cloud self-driving software.}
  \label{Drone}
\end{figure}

\subsubsection{Advantages and disadvantages}
\smallskip
Key advantages of edge-cloud computing consist of decentralization, low latency (due to the smaller distance between the user and the MDC) performance improvement and battery saving due to computation off-loading. Also, the same advantages of cloud computing apply to edge-cloud computing. Several demonstrations of advantages of computation offloading to the edge-cloud exist, such as:

\begin{itemize}[noitemsep]
\item Reducing 20 times the running time and energy consumption of tested applications\cite{TwentyTimes}.
\item Reduction of the response time of a face recognition application from 900 to 169 ms by moving the computation from cloud to the edge-cloud\cite{FaceRecognition}.
\item Reduction of response time by 80-200ms and reduction of energy consumption by 30\%-40\% of wearable cognitive assistance\cite{Cognitive}.
\end{itemize}

\smallskip
Key disadvantages of edge-cloud computing consist of problems with privacy, security, availability, cost and consistency. In regards to latency-sensitive applications, the main problem of edge-cloud computing is the lack of guarantees on the performance of an application. Also, edge-cloud computing suffers from the same disadvantages as cloud computing.


\section{Guaranteeing the performance of latency-sensitive applications}
\label{Motivation}
\smallskip
In the 5G Automotive Vision white paper\cite{5G}, the 5G-PPP (initiative between the European Commission and European ICT industry) introduces a multitude of technologies focused on integrating the automotive and the telecom industry. These technologies include mostly latency-sensitive applications such as cooperative collision avoidance of cars, augmented reality windows, utilization of sensors distributed across the city for self-driving, traffic synchronization and many more.

\smallskip
Importantly, the relocation of complex tasks from computing units in vehicles and IoT to a remote server is argued to be possible and recommended in 5G infrastructures. In order to use this remote computing, it is necessary to provide low latency and high reliability. 

\smallskip
To implement this remote computing, cloud computing is insufficient due to having extensive and variable distance latency (time needed to transfer a request from the user to the data center).

\smallskip
As a result of using decentralized data centers located closer to the user, the edge-cloud significantly reduces the distance latency. However, the base architecture of an edge-cloud system does not provide a guarantee on round trip time (RTT). RTT is the time needed to get a response for a request issued by a user. RTT consists of two times the distance latency (to and from the server) and the time needed to calculate the response. 

\smallskip
The issue is that developers are expected to have soft real-time requirements on the RTT for their latency-sensitive applications. For example, developers can demand, that the RTT for their self-driving or augmented reality application to be less than 70ms, otherwise the applications would be unusable (e.g., self-driving car would not react fast enough).

\smallskip
Imagine an edge-cloud provider having an MDC composed of several servers, which covers a single city with distance latency guaranteed to be less than 20 ms, and the service provided is in the form of hosting latency-sensitive applications. If the provider allocated applications on servers using a trivial strategy (e.g., randomly), it is very likely, that the applications would slow each other down to the point of exceeding the required RTT, making the applications unusable.

\smallskip
The focus of our thesis is to solve this problem and provide a soft guarantee on the RTT for applications running in the edge-cloud. Soft guarantee on the RTT means that a request issued to the edge-cloud system will return in a specified amount of time with a specified probability (e.g., a request to apply a filter on an image will return in 30ms with 90\% probability).

\smallskip
It is important to note that we do not focus on providing full real-time guarantees (every request finishes in a specified amount of time). The reason being, that such guarantees are the domain of real-time programming, which comes at a very high price of forcing developers to a low-level programming language, limited choice of libraries and the use of a relatively exotic programming model of periodic non-blocking real-time tasks.