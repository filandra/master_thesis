%Done
\chapter{Predicting a percentile of process execution time}
\label{Approach}
\smallskip
In this chapter, we first provide an overview of the predictor's architecture. Then we focus on related theory needed to understand the design of the predictor, such as similarity, cluster analysis, weights and regression analysis. After that, we describe the individual prediction methods. The implementation details are explained in Chapter \ref{Implementation}.

\section{Predictor architecture overview}
\smallskip
As described in Section \ref{WhyPredictor}, we need a predictor, which predicts a given percentile of the elapsed execution time of a process when this process is collocated alongside other processes. 

\smallskip
As mentioned in Section \ref{Knowledge}, the predictor needs to be general enough to work on multiple hardware and software configurations of nodes. The predictor can only utilize data matrices, which describe the behavior of a process or a combination of processes on a given type of a node. Consequently, the predictor does not use any specific information about the nodes (such as the type of CPU used) or the processes (such as which library does the process use). The form of data matrices is heavily influenced by the configuration of a node -- two data matrices of the same process measured on two different nodes can have very different measured properties due to different hardware.

\smallskip
A trivial prediction solution is to run the combination of processes on a test-node and measure the slowdown for each process, thus creating a database of combinations with known slowdowns. Then, before assigning the combination to an edge-cloud node, a simple database lookup would suffice to decide, if the slowdown is acceptable or not. As explained in Section \ref{Number}, the measurement of a large number of combinations is challenging, which makes this solution acceptable only with a small number of different processes and different node types.

\smallskip
In Section \ref{Knowledge}, we also mentioned the advantage of having a general predictor. The main advantage is that this predictor should be able to work on new and unexpected hardware configurations of nodes, predicting unexpected processes with new and unexpected properties in the data matrix. This sounds very similar to the use cases for the latest deep learning models. The reason why we did not use deep learning for our predictor lies in the amount of information available to us. It is usually necessary to utilize data points in the numbers of millions, however, as we will see in Chapter \ref{Evaluation}, the number of combinations we were able to obtain is in the thousands from less than twenty processes. Thus we consider the risk of unsuccessful deep learning due to limited data to be too high.

As a result, we decided to use a more traditional approach to design the predictor. The core of our approach consists of cluster analysis, weights training, regression analysis, and boundary detection. The prediction process is visualized in Figure \ref{Schema}.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{../img/process_schema.png}
  \caption{Schema of the prediction process.}
  \label{Schema}
\end{figure}

\subsubsection{Data acquisition}
\smallskip
In the first phase, it is necessary to obtain data matrices, which will be provided to the predictor. We utilized an existing measurement software that ran on a small server farm. This step is executed a long time before any actual prediction is calculated due to the time required for measurement.

\subsubsection{Predictor: Data preprocessing}
\smallskip
After we finish the data acquisition, we need to preprocess the data matrices into a more suitable form. The result of preprocessing is scaled and normalized statistical information about the measured properties of processes, such as mean, median, deviation, etc. Further explanation is provided in Section \ref{Preprocess}. This step is done in advance in order to reduce the prediction calculation time.

\subsubsection{Predictor: Data analysis}
\smallskip
The preprocessed data matrices are analysed in order to uncover:

\begin{itemize}[noitemsep]
\item Similarity between processes (Section \ref{Similarity})
\item Relationships among the processes using cluster analysis (Section \ref{Cluster})
\item Usefulnes of various measured properties using weights (Section \ref{Weights})
\item Trends in slowdown of processes using regression analysis (Section \ref{Regression}) 
\item Limits on node resource usage using a boundary system (Section \ref{Boundary}).
\end{itemize}

The approaches to data analysis are evaluated in Chapter \ref{Evaluation}. This step is also done in advance to reduce the prediction calculation time.

\subsubsection{Predictor: Prediction}
\smallskip
Finally, we calculate a multitude of predictions for a given combination of processes by using the results of all previous phases. This step is designed to be computationally simple in order to achieve fast calculation times and is described in Section \ref{PredictionMethods}. The individual prediction methods are evaluated in Chapter \ref{Evaluation}. The user of the predictor is provided with a prediction from the most accurate method that was able to predict a given combination with the provided data.

\section{Similarity between processes}
\label{Similarity}
\smallskip
It is important to define the concept of similarity between processes first,  as this concept is used multiple times in our solution. The whole meaning of similarity depends heavily on the description of processes and the way this information is interpreted. For example, we could say that two processes are similar if they have similar lengths of execution time. This is, of course, not suitable for our purposes.

\smallskip
We are interested in the similarity of behavior and resource usage of the processes. Imagine we had two processes, which write some data on the disk and a third process, which only calculates some mathematical functions. In our sense of similarity, the two processes using disk are more similar to each other than to the third process.

\smallskip
In our case, the description of processes is in the form of single data matrices. Combination data matrices are not used as we only want to know the information about processes running alone without the interference of other processes.

\smallskip
It is possible to devise many ways of interpreting the information in single data matrices in order to identify the similarity of behavior of processes. We chose a few approaches, which we describe in Section \ref{SimilarityImpl} and compare in Chapter \ref{Evaluation}. For now, the key thing to understand is that similarity between processes means similarity in their behavior and resource usage.

\section{Cluster analysis}
\label{Cluster}
\smallskip
Cluster analysis is one of the key concepts of our solution. The general idea of clustering is to find relationships between objects by placing similar objects in groups. The objects in said groups should be more similar to each other than to objects in other groups. In our case, the objects are individual processes, which can run in the edge-cloud system.

\smallskip
To provide a motivational example, imagine we had a group of processes and data matrices of all pairs of processes (all double data matrices). If we ran a cluster analysis, the resulting clusters could, for example, correlate with the process resource usage, dividing them into CPU-heavy, memory-heavy and disk-heavy clusters. We hypothesize that similarly behaving processes should influence other processes in a similar manner. Thus the influence between members of two clusters should be similar (e.g., disk-heavy processes slow other disk-heavy processes down by a certain percentage of execution time but are neutral to CPU-heavy ones). Now imagine that we were provided with a new process with only a single data matrix measured. Just by placing this process into its respective cluster, we can predict the interaction between this process and others. As a result, the knowledge of groups of similar processes offers additional information, which can be used to uncover new relationships between processes and predict their interaction.

\smallskip
It is important to note that the clusters could also group processes by other means than just resource usage. Imagine we were provided with six CPU-heavy facial recognition processes and the clustering algorithm divided them into two clusters -- three versions of one process and three versions of some other process. Thus even though the resource usage is very similar, the versions of the processes are much more similar to each other, which divides the set of processes into two clusters.

\smallskip
As a result of cluster analysis, each process is assigned a cluster (denoted by a number), into which it belongs. The utilization of information obtained by cluster analysis is explored in greater detail in Section \ref{PrimaryCluster} and \ref{ClusterCluster}.

\smallskip
Cluster analysis is not a single algorithm but a problem to be solved, for which there are many methods available. The choice of a particular method influences the result of cluster analysis heavily. The explanation of the methods we used is provided in Section \ref{ClusterImpl}.

\section{Weights}
\label{Weights}
\smallskip
The concept of weights relates directly to the concepts of similarity between processes and cluster analysis. The data matrix, which is provided as the description of the processes, only provides information about properties of the process (e.g., the number of instructions executed etc.). However, it would be useful to have information indicating which of these properties are actually useful in order to determine the similarity of behavior and to analyze clusters.

\smallskip
To illustrate this issue on an example, suppose that an error occurred during the measurement of data matrices, causing some properties to have random values. We measured CPU-heavy processes C1, C2 and disk-heavy processes D1, D2. Without the ability to determine which properties are useful and which are useless, these erroneous properties would negatively impact the calculation of similarity between processes and consequently negatively impact the cluster analysis.

\smallskip
As a result, weights indicating the usefulness of various properties for similarity calculation are introduced. Weights are in the form of a vector, where each value corresponds to one property in the data matrix. The values in this vector range between 0 and 1 and indicate the importance of a given property for the similarity calculation, as illustrated in Figure \ref{WeightsFigure}. 

\smallskip
We hypothesize that with these weights, we can improve the similarity calculation, reduce noise in the data matrices, and consequently improve the cluster analysis. They also solve one of the challenges we are faced with, namely the issue of having different data matrices on different types of nodes. As we can not be sure that the measured properties of processes are correct or useful, we need a system of determining the importance of these properties.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{../img/weights.png}
  \caption{Illustration of the improvement of similarity calculation and cluster analysis caused by the introduction of weights.}
  \label{WeightsFigure}
\end{figure}

\smallskip
It is important to highlight that weights can differ on different nodes (due to possibly different data matrices between the nodes) in their length and value. Thus weights are node-specific and their value depends on available processes.

\smallskip
Similarly to cluster analysis, there is no single algorithm used to find such weights, but many approaches can be valid. The implementation of the approach we chose is described in Section \ref{WeightsImpl}.

\section{Regression analysis}
\label{Regression}
\smallskip
Regression analysis is another key concept of our solution and is used to find trends in the slowdown of processes using only the information about the length of execution time in single and combination data matrices.

\smallskip
To provide a motivational example, imagine we only had a single process A in the edge-cloud system. We then measured A running alone (90th percentile of the execution time equal to 10 sec), A-A (15 sec) and A-A-A-A (25 sec). Using this information, we can express a prediction of the percentile of the execution time of processes A as:

\begin{equation}
A_{percentile} = t_0 + t_1secondary
\end{equation}

\smallskip
This form of prediction is a case of linear regression prediction, where the percentile of the execution time of process A ($A_{percentile}$) is the \textit{dependent variable} and the number of secondary processes A (\textit{secondary}) is the \textit{independet variable}. In the model (equation), the dependent variable is a linear combination of the coefficients $t_0$ and $t_1$. The most logical estimation of coefficient $t_0$ is the percentile of execution time of the process A running alone (10 sec) and 5 sec for $t_1$. Using this formula, the prediction of percentile of execution time for process A in the combination A-A-A is 20 sec.

\smallskip
This concept can be extended for multiple processes into \textit{multiple linear regression} with multiple independent variables. For example if we had processes A and B, the regression models would be:

\begin{equation}
A_{percentile} = x_0 + x_1A + x_2B
\end{equation}

\begin{equation}
B_{percentile} = y_0 + y_1A + y_2B
\end{equation}

\smallskip
Where $A$ and $B$ are the numbers of secondary processes A and B respectively and $x_0$, $x_1$, $x_2$, $y_0$, $y_1$ and $y_2$ are the coefficients, which need to be calculated.

\subsection{Polynomial regression analysis}
\smallskip
Polynomial regression analysis is a special case of multiple linear regression, where the model is created as the nth degree polynomial of the independent variables. We can consider the previous examples to be a first-degree polynomial regression. Thus the second-degree polynomial model of the example with single process A is:

\begin{equation}
A_{percentile} = t_0 + t_1secondary + t_2secondary^{2}
\end{equation}

\smallskip
As a result, instead of approximating the percentile of execution time of a process using a line, we can now use a curve as illustrated in Figure \ref{Models}. Similarly, the second-degree polynomial model of the example with processes A and B for $A_{percentile}$ is:

\begin{figure}
  \centering
  \includegraphics[width=0.60\textwidth]{../img/models.png}
  \caption{Ilustration of of first and second degree polynomial regression models.}
  \label{Models}
\end{figure}

\begin{equation}
A_{percentile} = x_0 + x_1A + x_2B + x_3A^{2} + x_4B^{2} + x_5(A \times B)
\end{equation}

\smallskip
For $B_{percentile}$, the model is similar, just with different coefficients. In our solution, we use the polynomial regression to predict the percentile of the execution time of processes. The challenge is how to estimate the values of the coefficients ($x_0$, $x_1$, etc.) using the information about the execution time in combination data matrices, which is explained in Section \ref{RegImpl}. The degree of the polynomial is decided experimentally in Section \ref{PredictionDepth}.

\section{Prediction methods}
\label{PredictionMethods}
\smallskip
We chose to implement a multitude of prediction methods. We hypothesize that individual methods can perform better or worse in certain conditions and thus it is suitable to implement and compare more of them in order to find the one which suits us best. We also hypothesize that various prediction methods can complement each other -- for example, when the best performing method does not have suitable data for given prediction, the second-best performing method can step in and provide a prediction.

\subsection{Closest friend prediction}
\label{ClosestFriend}
\subsubsection{Hypothesis}
\smallskip
Closest friend prediction is the simplest method, which only utilizes the information about the similarity of processes. Imagine we want to predict the 90th percentile of the A-B-C combination. We lack the data matrix for this combination, however, we already measured A-B1-C or A-B-C1, where B1 and C1 are previous versions of their respective processes. Intuitively, the data matrices of old versions could serve as suitable predictions for the A-B-C combination, as their behavior should be quite similar. 

\smallskip
If we extend this idea, we hypothesize that any combination can be predicted by a data matrix of some other combination, if the secondary processes in this alternative combination are the most similar to the original ones. This concept is illustrated in Figure \ref{ClosestFigure}.

\begin{figure}
  \centering
  \includegraphics[width=0.65\textwidth]{../img/closest_friend.png}
  \caption{Illustration of the closest friend prediction method.}
  \label{ClosestFigure}
\end{figure}

\subsubsection{Procedure}
\smallskip
During the prediction phase:
\begin{enumerate}[noitemsep]
\item Prepare alternative combinations by exchanging processes in the provided combination with their most similar counterpart. For example, alternative combinations for A-B-C (if B is most similar to X and C is most similar to Y) are A-B-C, A-C-X, A-B-Y and A-X-Y.
\item Sort the alternative combinations by their distance from the original combination. The distance of an alternative combination is measured as a sum of distances between the original and alternative processes in the combination.
\item Iterate through the sorted alternative combinations and perform a database lookup for the data matrix. If found, return the requested percentile of elapsed execution time.
\end{enumerate}

\subsection{Primary-cluster prediction}
\label{PrimaryCluster}
\subsubsection{Hypothesis}
\smallskip
Imagine that due to the lack of data matrices, the closest friend prediction could not be calculated. We thus need to access more information, from which to calculate the prediction, by going upwards in abstraction -- instead of examining how a combination of processes influence a process, we examine how a combination of clusters on average influences a process. As a result, the primary-cluster prediction method should be less accurate than the closest-friend prediction, however, due to the larger amount of available data, it can complete a prediction more often.

\smallskip
We hypothesize that the influence of processes from secondary clusters on the primary process can be analyzed as the average percentual change of percentile of the execution time of the primary process. We call it the primary-cluster slowdown and denote it similarly to the data matrix naming convention, except secondary processes are replaced by cluster numbers.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{../img/primary_cluster.png}
  \caption{Illustration of the primary-cluster prediction method.}
  \label{PrimaryFigure}
\end{figure}

\smallskip
To give a simple example (illustrated in Figure \ref{PrimaryFigure}), consider that processes A belongs to cluster 0, processes B1, B2, B3, and X belong to cluster 1 and processes C1, C2, C3 and Y belong to cluster 2. We have data matrices A-B1-C1, A-B2-C2 and we want to predict A-B3-C3. B3 is most similar to X and C3 to Y, which blocks the closest friend prediction. When using the primary-cluster prediction, we calculate that percentile of A is 10\% slower in A-B1-C1 and percentile of A is 20\% slower in A-B2-C2. Then the A-1-2 primary-cluster slowdown is 15\%. The prediction of A-B3-C3 is then calculated as the percentile of the execution time of single A extended by primary-cluster slowdown (15\%).

\subsubsection{Procedure}
\begin{enumerate}[noitemsep]
\item During the analysis phase:
\begin{enumerate}[noitemsep]
\item Analyse clusters of processes and assign each process a cluster into which it belongs.
\item Calculate primary-cluster slowdown for all available combinations of primary processes and clusters. 
\end{enumerate}
\item During the prediction phase:
\begin{enumerate}[noitemsep]
\item Identify into which clusters do the secondary processes in the requested combination belong. In other words, transform the combination from A-B-C into A-0-1, for example.
\item Perform a database lookup into the previously calculated primary-cluster slowdowns. If the requested slowdown was calculated, return the percentile of execution time of the primary process running alone extended by the primary-cluster slowdown.
\end{enumerate}
\end{enumerate}

\subsection{Cluster-cluster prediction}
\label{ClusterCluster}
\subsubsection{Hypothesis}
\smallskip
The cluster-cluster prediction method is a direct extension of the thought process behind the primary-cluster prediction. If we lack the data even for primary-cluster prediction, we can again go a step upwards in the abstraction and focus on the interaction between the cluster, into which the primary process belongs, and the clusters, into which the secondary processes belong. The cluster-cluster prediction method should be less accurate than the primary-cluster prediction, however, due to the larger amount of available data, it can complete a prediction more often.

\smallskip
We hypothesize that this interaction between primary and secondary clusters can be analyzed as an average percentual change of percentile of the execution time of processes from the primary cluster when collocated with processes from the secondary clusters. We call it cluster-cluster slowdown and denote it similarly to the primary-cluster notation, except the primary process is also replaced by cluster number. The prediction is then calculated by adding the cluster-cluster slowdown to the original percentile of the execution time of the primary process in the predicted combination.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{../img/cluster_cluster.png}
  \caption{Illustration of the cluster-cluster prediction method.}
  \label{ClusterFigure}
\end{figure}

\smallskip
To give a simple example (illustrated in Figure \ref{ClusterFigure}), consider that processes A1, A2 and A3 belong to cluster 0, processes B1 and B2 belong to cluster 1 and processes C1 and C2 belong to cluster 2. We have data matrices A1-B1-C1, A2-B2-C2 and we want to predict A3-B1-C2. When using the cluster-cluster prediction, we calculate that the percentile of A1 is 10\% slower in A1-B1-C1 and the percentile of A2 is 20\% slower in A2-B2-C2. Then the 0-1-2 cluster-cluster slowdown is 15\%. The prediction of A3-B1-C2 is then calculated as the percentile of the execution time of single A3 extended by cluster-cluster slowdown (15\%).

\subsubsection{Procedure}
\begin{enumerate}[noitemsep]
\item During the analysis phase:
\begin{enumerate}[noitemsep]
\item Analyse clusters of processes and assign each process a cluster into which it belongs.
\item Calculate cluster-cluster slowdown for all available combinations of primary and secondary clusters.
\end{enumerate}
\item During the prediction phase:
\begin{enumerate}[noitemsep]
\item Identify into which clusters do the processes in the requested combination belong. In other words, transform the combination from A-B-C into 2-0-1, for example.
\item Perform a database lookup into the previously calculated cluster-cluster slowdowns. If the requested slowdown was calculated, return the percentile of execution time of the primary process running alone extended by the cluster-cluster slowdown.
\end{enumerate}
\end{enumerate}

\subsection{Polynomial regression prediction}
\smallskip
To calculate the polynomial regression prediction, we build a regression model for each one of the processes during the analysis phase. Each model is a polynomial of a certain degree, which consists of independent variables and coefficients. The coefficients are calculated during the analysis, therefore, to calculate the prediction of the percentile of the execution time of a process, we only need to fill the independent variables.

\smallskip
During the prediction phase, the values of independent variables are calculated from the numbers of distinct secondary processes in the predicted combination of processes, as explained in Section \ref{Regression}. 

\subsubsection{Procedure}
\begin{enumerate}[noitemsep]
\item During the analysis phase:
\begin{enumerate}[noitemsep]
\item For each process:
\begin{enumerate}[noitemsep]
\item Gather all data matrices with the specified primary process
\item Calculate the regression model
\end{enumerate}
\end{enumerate}

\item During the prediction phase:
\begin{enumerate}[noitemsep]
\item Calculate the values of the independet variables from the predicted combination
\item To form the prediction, apply the model using the independent variables
\end{enumerate}
\end{enumerate}

\section{Operational boundary}
\label{Boundary}
\smallskip
When deploying processes on the nodes, we want to collocate as many processes as possible, as long as the RTT satisfies soft real-time requirements. This is done in order to reduce the number of nodes needed. However, if a combination of processes exceeds the computational capacity of a computer, their behavior can become highly unpredictable. There are many ways in which a set of processes can exceed the capacity of a computer: they can overload the CPU, require too much memory, use too much network bandwidth, etc. 

\smallskip
To give an example, imagine we collocated five processes, which write 50 MB per second on disk, while the capacity of the computer is to write 200 MB per second on the disk. We would exceed the capacity by 50 MB per second, which would result in a slowdown in the execution time of the processes.

\smallskip
As a result, a problem arises -- how can we be confident that the predictor is reliable when predicting a combination. It is reasonable to assume that a predictor probably can not predict a combination that exceeds the computational capacity of a computer by 500\% and that such a combination probably exceeds the RTT requirements anyway. There are, of course, exceptions, such as that we measured in advance exactly this extreme combination, and now the predictor can predict it with 100\% accuracy -- this situation, however, should be very rare.

\smallskip
As a result, we need to decide if a given combination of processes can be reliably predicted. There are two solutions to this problem:
\begin{enumerate}[noitemsep]
\item Dynamic solution: Design a special estimator, which would for any combination of processes evaluate the capability of the predictor to predict this combination and estimate confidence in this prediction.
\item Static solution: Detect that a combination exceeds an operational boundary (e.g., 200\% of computer resources) and mark the prediction as potentially unreliable.
\end{enumerate} 

\smallskip
We chose to implement the static solution and leave the dynamic solution as future work. By setting the operational boundary, the user is given greater control over the predictor and can avoid many problematic situations. The design of the boundary detection system is explained in Section \ref{BoundaryImpl}. In Chapter \ref{Evaluation}, we examine the accuracy of the predictor, both with and without the use of boundary detection, in order to evaluate its effects.