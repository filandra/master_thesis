%Done
\chapter{Implementation}
\label{Implementation}
In this chapter, we focus in greater detail on the implementation of certain aspects of the solution, as described in Chapter \ref{Approach}. During the implementation, we focused on finding more than one way to solve a problem (e.g., more than one normalization function, similarity function, clustering algorithm, etc.). The reason being, that we do not know in advance which combination of methods is most suitable for us, and as a result, we want to test out multiple combinations experimentally in Chapter \ref{Evaluation}.

\section{Data preprocessing}
\label{Preprocess}
\smallskip
The purpose of the preprocessing is to extract useful statistical information from the provided data matrices for similarity search between applications and to determine the percentile of the slowdown of a primary process in a combination of processes. The results of preprocessing are stored in order to reduce the calculation time of prediction.

\smallskip
It is important to note that preprocessing differs for single data matrices and combination data matrices. This is due to the fact that some properties can be cumulatively influenced by secondary processes and thus do not offer useful information. Unfortunately, the only reliably usable information in the combination data matrix is the length of run time. Thus only single data matrices are scaled and normalized.

\subsection{Scaling}
\smallskip
Initially, each row in the data matrix provides exact values of various properties of a process (e.g., number of instructions in a calculation). It is necessary to scale some of these values by their respective run time in a given row in order to distinguish resource consumption of long-running processes from short-running processes. As a result, a portion of the properties in a row will be expressed per millisecond (e.g., instructions per millisecond), which is used later for similarity search between processes. The user can configure which properties should be scaled.

\smallskip
To give an example, without scaling a process M1, which calculates matrix addition, and a second process M2, which calculates the same matrix addition two times, would seem like processes with very different behavior without property scaling (M2 would have the values of scaling properties double). However, with scaling, we can see that those two processes behave essentially the same, just the second process runs longer. 

\smallskip
As explained in Section \ref{Behaviour}, each process calculation on the node will be immediately repeated after completion. If we imagine two scenarios, M1-M1 and M1-M2, the influence of the secondary process on the primary process should be essentially the same in both cases (just two parallel matrix additions collocated). Scaling is thus necessary in order to calculate the similarity of behavior between processes.

\subsection{Statistics}
\smallskip
With the properties scaled, we can extract statistical information from the data matrix. For the single data matrices, the statistics are calculated for each property and consist of:
\begin{itemize}[noitemsep]
\item Mean
\item Median
\item Standard deviation
\item Percentile
\item Max
\item Min
\end{itemize}

\smallskip
The result is again a matrix as each statistic is calculated for every property.

\smallskip
For the combination data matrices we only analyze the execution time property and calculate the relative slowdown -- the difference between the percentile of the execution time of primary process running alone and in combination, measured as the percentage of the primary percentile execution time:

\begin{equation}
\frac{percentile(combination\_time) - percentile(primary\_time)}{percentile(primary\_time)}
\end{equation}

\subsection{Normalization}
\smallskip
The final step in the preprocessing phase is to normalize the data, which transforms the values of a property from its original range into values between 0 and 1. This is needed as the values of different properties can be in very different ranges (some properties are in single digits, other in millions). Without normalization, this would distort the similarity search between processes greatly, as the properties with large ranges would have a bigger impact. Different normalization methods can have different influence on the overall prediction method. We chose to implement the two most popular methods and compare them in Chapter \ref{Evaluation}.

\smallskip
Thus the result of data preprocessing is a set of preprocessed data matrices (one for each process), which consists of normalized statistical information (illustrated in Table \ref{tab:PreprocessedDataMatrix}).

\begin{table}[!htbp]
\centering
\large
\resizebox{\columnwidth}{!}{%
\begin{tabular}{|l|l|l|l|l|l|}
\hline
\rowcolor[HTML]{FFFFFF} 
{\color[HTML]{000000} } & {\color[HTML]{000000} \textbf{instructions}} & {\color[HTML]{000000} \textbf{cache-misses}} & {\color[HTML]{000000} \textbf{read\_sectory}} & {\color[HTML]{000000} \textbf{written\_sectors}} & {\color[HTML]{000000} \textbf{io\_time}} \\ \hline
\rowcolor[HTML]{FFFFFF} 
\cellcolor[HTML]{FFFFFF}{\color[HTML]{000000} \textbf{mean}} & 0.8 & 0.47 & 0 & 1 & 1 \\ \hline
\rowcolor[HTML]{FFFFFF} 
\cellcolor[HTML]{FFFFFF}{\color[HTML]{000000} \textbf{median}} & 0.75 & 0.48 & 0 & 1 & 1 \\ \hline
\rowcolor[HTML]{FFFFFF} 
\cellcolor[HTML]{FFFFFF}{\color[HTML]{000000} \textbf{deviation}} & 0.02 & 0.01 & 0 & 0 & 0.02 \\ \hline
\rowcolor[HTML]{FFFFFF} 
\cellcolor[HTML]{FFFFFF}{\color[HTML]{000000} \textbf{...}} & ... & ... & ... & ... & ... \\ \hline
\end{tabular}
}
\caption{An example of a preprocessed single data matrix.}
\label{tab:PreprocessedDataMatrix}
\end{table}

\subsubsection{Min-max normalization}
\smallskip
For every calculated statistic and every property we find the $max$ and $min$ value among all processes. The normalized value of the property in a particular statistic of a given process $X$ is then calculated as:

\begin{equation}
normalized\_value_{s,p}(X) = \frac{original\_value_{s,p}(X)-min_{s,p}}{max_{s,p}-min_{s,p}}
\end{equation}

\smallskip
Where $s$ represents a statistic and $p$ represents a measured property.

\subsubsection{Z-score normalization}
\smallskip
For every calculated statistic and every property we find mean $\mu$ and standard deviation $\delta$ among all processes. The normalized value of the property in a particular statistic of a given process $X$ is then calculated as:

\begin{equation}
normalized\_value_{s,p}(X) = \frac{original\_value_{s,p}(X)-\mu_{s,p}}{\delta_{s,p}}
\end{equation}

\smallskip
Where $s$ represents a statistic and $p$ represents a measured property.

\section{Calculating similarity between processes}
\label{SimilarityImpl}
\smallskip
As explained in Section \ref{Similarity}, the meaning of similarity between two processes depends on the description of said processes and on the interpretation of the provided description. In our case, we form the description of processes from preprocessed single data matrices. Combination data matrices are not used as we only want to know the information about processes running alone without the interference of other processes. 

\smallskip
We implemented two distance measures, which is just an inverse of similarity (clustering algorithms rely on distance instead of similarity), and both of these measures use only a subset of the vectors available in preprocessed single data matrices to describe a process. This is done in order to reduce the calculation time.

\subsubsection{Application of weights}
\smallskip
During similarity calculation, we can also utilize weights, which indicate the importance of various properties. The application of weights is straightforward -- we execute elementwise multiplication between each vector used to describe a process and the weights. Because the values in weights range between 0 and 1, this will cause some properties to contribute less or more towards the final similarity value.

\subsection{Average pair correlation distance}
\smallskip
To calculate the average pair correlation distance, we describe the process using a set of vectors -- the max, min and percentile vector from the preprocessed single data matrix. The correlation distance is then calculated between every pair of vectors from the two sets and the results are averaged.

\begin{equation}
corr\_dist(u,v) = \frac{(u-\bar{u})\cdot(v-\bar{v})}{||u-\bar{u}||_2 \times ||v-\bar{v}||_2}
\end{equation}

\smallskip
Where $||u||_2$ is the euclidean norm of the vector, $\bar{u}$ is the mean of elements of the vector and $u \cdot v$ is the dot product of the vectors. The final average pair correlation distance is then calculated as:

\begin{equation}
APCorrelation(U,V) = \frac{\sum\limits_{u \in U}{\sum\limits_{v \in V}{corr\_dist(u,v)}}}{|U| \times |V|}
\end{equation}

\smallskip
Where $U$ and $V$ are the two sets of vectors describing the two processes, between which we calculate the average pair correlation distance.

\subsection{Frobenius distance}
\smallskip
To calculate the Frobenius distance, we describe the process by a matrix consisting of the mean, median and standard deviation vectors taken from the preprocessed single data matrix. The Frobenius norm is then calculated from the difference of the matrices of the two processes, for which we calculate the Frobenius distance.

\begin{equation}
Frob\_dist(X,Y) = ||X-Y||_F = \sqrt{\sum^{m}_{i=1}{\sum^{n}_{j=1}{(X_{i,j}-Y_{i,j})^2}}}
\end{equation}

\smallskip
Where $X$ and $Y$ are the matrices representing the processes.

\section{Calculating clusters}
\label{ClusterImpl}
\smallskip
In Section \ref{Cluster}, we introduced the idea of cluster analysis, and in Sections \ref{PrimaryCluster} and \ref{ClusterCluster} we explained how the information of clusters of processes can be used to predict the execution time of processes. In this section, we describe the chosen clustering algorithms%.

\subsubsection{Requirements}
\smallskip
We are interested in \textit{hard clustering}, which means that the relationship between the object and the cluster is binary (the object either belongs or does not belong into the cluster). This is in contrast with \textit{fuzzy clustering}, where objects belong to all clusters to a certain degree, which is not suitable for us.

\smallskip
Additionally, we require \textit{strict partitioning clustering}, which means that each object belongs to exactly one cluster. Thus we do not allow outliers or overlapping of clusters. The reason is that overlapping and outliers would bring too much complexity to the prediction method. However, the exploration of these concepts could be suitable for future work.

\smallskip
The last requirement is the automatic detection of the number of clusters in the dataset. Consequently, we exclude algorithms such as k-means, where the user has to specify the number of clusters to detect. The reason being that the number of clusters is very hard to estimate in advance in our situation, and various sets of processes can produce vastly different numbers of clusters.

\subsubsection{Selected algorithms}
\smallskip
To be confident in the correctness of used clustering algorithms, we chose to use the implementation provided by Python scikit library\cite{scikit}.

\smallskip
The overview of all clustering algorithms provided by scikit using a toy dataset can be seen in Figure \ref{ClusteringComparison}. In this overview, we can see that different clustering methods have very different outcomes. It is thus very important to choose a suitable algorithm for our purposes.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{../img/clustering_comparison.png}
  \caption{Comparison clustering algorithms provided by scikit using a toy dataset\cite{scikit}.}
  \label{ClusteringComparison}
\end{figure}

\smallskip
Imagine that the distances between data points in the toy dataset in Figure \ref{ClusteringComparison} are distances in the behavior of processes. We hypothesize that the clustering provided by Affinity propagation and mean-shift is most suitable if we want to cluster processes by the similarity of their behavior. The rest of the algorithms provided by scikit do not automatically detect the number of clusters, and the way they form clusters does not fit our needs.

\smallskip
For example, in the first row, we do not want to detect the outer and inner circle, but we want to split both circles into partitions. The same applies to the second row, where we do not want to detect two separate moons. Similarly, in the last row, we would like to split the square into four parts (in this case, the mean-shift does not cluster ideally).

\smallskip
To calculate the clusters, we provide the clustering algorithms with a distance matrix calculated using preprocessed data matrices and our similarity measures. A distance matrix is a matrix containing the information about the distance between each pair of processes.

\subsection{Affinity propagation}
\smallskip
Affinity propagation\cite{Affinity} is based on finding exemplars around which clusters are then formed. This is done through a technique called message passing, during which information is exchanged between data points until convergence. This information represents the ability of a data point to be the exemplar of other data points. The exemplars are iteratively chosen until they remain unchanged for some number of iterations, or a maximum number of iterations was reached.

\smallskip
The time complexity is $O(N^2T)$, where N is the number of processes and T is the number of iterations until convergence. The memory complexity is $O(N^2)$.

\subsection{Mean-shift}
\smallskip
Mean-shift\cite{MeanShift} is a centroid based algorithm, which updates candidates to be the mean of the points in a region. We can understand this algorithm by thinking of our data points to be represented as a probability density function, where higher density regions will correspond to the regions with more points. In clustering, we need to find clusters of points, i.e., the regions with higher density in our probability density function. This is done by iteratively shifting the centroids towards higher density regions. The time complexity is also $O(N^2T)$.

\section{Weights training}
\label{WeightsImpl}
\smallskip
As introduced in Section \ref{Weights}, we need weights in order to determine the importance of properties measured in data matrices, in regards to the similarity of process behavior and cluster analysis.

\smallskip
To illustrate the idea behind the solution of training the weights, imagine we had processes C1, C2 and C3 as CPU-heavy processes and D1, D2 and D3 as disk heavy processes. We ran a cluster analysis and the result was three clusters [C1, D1], [C2, D2] and [C3, D3]. The reason being that in the data matrices one of the measured properties has random values. Thus the calculation of similarity of processes is negatively impacted, and consequently, also the clustering is negatively impacted.

\smallskip
We want to tell our training model, that the correct clustering is [C1, C2, C3] and [D1, D2, D3]. With this information, the training model can adjust the weights and try again, while hopefully lowering the importance of the erroneous property. In the end, we should end up with weights, which adjust the importance of properties in such a way, that the resulting clustering is correct.

\smallskip
In this scenario, the processes C[1,3] and D[1,3] are the training dataset. The correct clustering is the ground truth, which is assigned by a human. Thus we compare the results of cluster analysis on the training dataset against ground truth labeling. This way, we can determine if the weights improve the calculation of similarity of process behavior. Using the labeling in the ground truth dataset, we essentially determine, what does a similarity of process behavior mean to us, and the weights then shift the calculation of similarity in order to satisfy it. This method of weights training can be viewed as a case of supervised learning.

\smallskip
It is important to note that this approach is heavily dependent on the quality of the training data. If we provided ground truth, which was wrongly labeled, the weights would train to distort the similarity search. The ground truth, which we used in our solution, is described in Section \ref{ULS}.

\smallskip
Thus the process of training the weights is:
\begin{enumerate}[noitemsep]
\item Create a training dataset from the ground truth.
\item For some number of iterations:
\begin{enumerate}[noitemsep]
\item Use the latest weights and execute cluster analysis on the dataset.
\item Compare the result of cluster analysis to the ground truth labeling and assign it a score.
\item Based on the score, generate new weights with the goal of maximizing the score.
\end{enumerate}
\end{enumerate}

\smallskip
The score measures how well did the cluster analysis perform with the weights. The implementation of scoring is explained in Section \ref{ClusterScoring}. Thus the problem of weights training is transformed into an optimization problem with the goal of maximizing score through weights. Such an optimization problem has many common solutions, the algorithm which we implemented is described in Section \ref{SimAnneal}.

\subsection{Simulated annealing}
\label{SimAnneal}
\smallskip
Simmulated annealing is a method used to find global optimum of a function. In our situation, the function $f: R^{n} -> R$ is the cluster analysis combined with scoring method, which takes the weights of length $n$ as argument and produces single number (the score). In order to explain how simulated annealing works, let us compare it to hill climbing. Let us define:

\smallskip
\begin{itemize}[noitemsep]
\item $f: R^{n} -> R$ function for which we want to find global maxima
\item $x, y \in R^{n}$ vectors
\item $G: R^{n} -> R^{n}$ weights generation function
\end{itemize}

\smallskip
The algorithm for hill climbing (when searching for maxima) goes as follows:

\smallskip
\begin{enumerate}[noitemsep]
\item $x= empty \textunderscore weights$
\item \textit{For some number of iterations:}
\begin{enumerate}[noitemsep]
\item $y=G(x)$
\item If $f(y) > f(x)$ then $x=y$
\end{enumerate}
\end{enumerate} 

\smallskip
Note that the weights generation function produces new weights based on the previously best-performing ones. The disadvantage of hill climbing is that there is a chance of weights getting stuck in local maxima. In simulated annealing, we sometimes allow the acceptance of weights with a worse score, instead of always accepting weights with a better score. The hypothesis is that this will allow us to jump out of local maxima and find the global one, or at least get closer to it. 

\smallskip
For this purpose, we define \textit{acceptance criterion}, which determines if weights, which result in a worse score, will be used as the basis for next weights generation. The acceptance criterion changes during the algorithms run, which is called \textit{cooling schedule}. A cooling schedule is specified by the initial $temperature$, $decrement function$ for lowering the value of temperature, and the final value of temperature at which the annealing stops\cite{Annealing}. With high temperature, the acceptance criterion accepts worse weights relatively frequently, but as time goes on and the temperature cools down, the acceptance criterion behaves more and more like hill climbing.

\smallskip
The idea is that we jump out of local maxima at the begining and towards the end we climb the global maxima (or value close to the global maxima). Let us define:

\begin{itemize}[noitemsep]
\item $f: R^{n} -> R$ function for which we want to find global maxima
\item $x, y \in R^{n}$ vectors
\item $G: R^{n} -> R^{n}$ weights generation function
\item $P: (R,R,R) -> <0,1>$ acceptance criterion
\item $T$ temperature
\item $C$ decrement function
\end{itemize}

\smallskip
The algorithm for simulated annealing goes as follows:
\begin{enumerate}[noitemsep]
\item $x= empty \textunderscore weights$
\item While T$>$1:
\begin{enumerate}[noitemsep]
\item $y=G(x)$
\item $x=y$ with the probability of $P(f(x), f(y), T))$
\item $T=C(T)$
\end{enumerate}
\end{enumerate} 

\smallskip
When implementing this algorithm, we need to solve mainly three different issues, which are specific to the kind of function for which we want to find global optima: how to implement weights generation function, acceptance criterion, and decrement function.

\smallskip
The weights generation function tak is defined as:
\begin{itemize}[noitemsep]
\item $G(weights)= weights + rand(-1,1)*[\dots, 0, dist, 0, \dots]$
\end{itemize}

\smallskip
Where $rand(...)$ is a function which randomly chooses one of the provided numbers, $dist = 1/3$ and the position of $dist$ in the vector $[\dots, 0, dist, 0, \dots]$ is also random. The values of a particular weight can not get below 0 or exceed 1. Thus weights range between zero, low, medium and high importance.

\smallskip
The acceptance criterion is implemented as :

\begin{equation}
P(f(x), f(y), T) = \begin{cases} 1 & \text{if } f(x)=<f(y) \\
                      exp \bigg( {{f(y)-f(x)}\over{T}} \bigg)    & \text{if } f(x)>f(y)  %
        \end{cases} 
\end{equation} 
        
\smallskip
The decrement function is implemented as $C=T*cooling$, where $cooling$ is a constant (usually 0.99) and starting temperature $T$ is set to 100000. The temperature at which simulated annealing stops is $T=1$.

\smallskip
Inspiration for these functions was drawn from\cite{Annealing}.

\section{Scoring clusters}
\label{ClusterScoring}
\smallskip
As explained in Section \ref{WeightsImpl}, we need to assign a score to a clustering when compared to a ground truth dataset. The choice of a scoring function influences the training of weights greatly. It is thus important to try multiple functions and choose the best one experimentally.

\smallskip
To give an example, imagine we had a scoring function, that would return a score of 1, if the number of clusters matched between the calculated and ground truth clustering, and otherwise gave a score of 0. The trained weights would then try to push the clustering algorithm into detecting a certain amount of clusters while disregarding any requirements for the similarity of behavior of the processes. Thus the final clustering when using weights trained with such scoring would have the correct number of clusters, however, the contents of the clusters could be completely wrong.

\smallskip
As a result, we need to use such scoring functions, that can project the similarity of behavior into the score. As already mentioned, the ground truth dataset is labeled by the behavior of the processes. Thus the scoring function has to take this labeling into account in an intelligent manner.

\smallskip
However, such a scoring function can not be as trivial as counting the number of errors in labeling. The evaluation metric should not take the absolute values of the cluster labels into account but rather if this clustering defines separations of the data similar to the ground truth\cite{scikit}.

\subsubsection{Selected scoring functions}
\smallskip
To be confident in the correctness of used scoring functions, we again chose to use the implementation provided by Python scikit library\cite{scikit}.

\smallskip
We selected two scoring functions, namely Fowlkes-Mallows score and V-measure. Both functions range between 0 and 1, where 1 is perfect clustering and 0 completely wrong clustering.

\subsection{Fowlkes-Mallows score}
The Fowlkes-Mallows score is calculated as the geometric mean of the pairwise precision and recall\cite{Fowlkess}.

\begin{equation}
FM = \frac{TP}{\sqrt{(TP+FP) \times (TP+FN)}}
\end{equation}

Where $TP$ is the number of pairs of processes, which belong to the same cluster in both detected and ground truth clusters (true positives). $FP$ is the number of pairs of processes, which belong to the same cluster in the ground truth, but not into the same detected cluster (false positives). $FN$ is the number of pairs of processes, which belong to the same detected cluster, but not into the same cluster in the ground truth (false negatives).

\subsection{V-measure}
V-measure is calculated as the harmonic mean of homogeneity and completeness of the clustering\cite{VMeasure}

\begin{equation}
v = 2 \times \frac{h \times c}{h + c}
\end{equation}

Homogeneity and completeness scores are given by:

\begin{minipage}[t]{.45\textwidth}
\begin{equation}
h = 1-\frac{H(C|K)}{H(C)}
\end{equation}
\end{minipage}
\begin{minipage}[t]{.45\textwidth}
\begin{equation}
  c = 1-\frac{H(K|C)}{H(K)}
\end{equation}
\end{minipage}

Where $C$ are the detected clusters, $K$ the ground truth clusters, $H(C|K)$ is the conditional entropy of the ground truth and detected clusters, and $H(C)$ is the entropy of the detected clusters.

\begin{equation}
H(C|K) = - \sum^{|C|}_{c=1}{\sum^{|K|}_{k=1}{\frac{n_{c,k}}{n} \times \log \left(\frac{n_{c,k}}{n_k}\right)}}
\end{equation}

\begin{equation}
H(C) = - \sum^{|C|}_{c=1}{\frac{n_c}{n} \times \log\left(\frac{n_c}{n}\right)}
\end{equation}

Where $n$ is the number of processes, $n_c$ and $n_k$ the number of processes in cluster $c$ and ground truth cluster $k$, and $n_{c,k}$ number of processes from $k$ assigned to $c$. $H(K|C)$ and $H(C)$ is defined symmetricaly. 

\section{Calculating polynomial regression models}
\label{RegImpl}
\smallskip
As explained in section \ref{Regression}, we need to calculate the regression model for each process. More specifically, we need to calculate the coefficients of the polynomial of independent variables. The model for a process X is expressed in Equation \ref{Equation}.

\begin{equation}
\label{Equation}
X_{percentile} = x_0 + x_1Y + x_2Z + x_3Y^{2} + x_4Z^{2} + x_5(Y \times Z) \dots
\end{equation}

\smallskip
Where $Y$ and $Z$ are the number of some secondary processes Y and Z and $X_{percentile}$ is the percentile of execution time of process X. Our goal is to calculate the coefficients $x_0$, $x_1$, etc. The number of the coefficients depends on the number of distinct processes, which can be collocated with process X, and the degree of the polynomial. To calculate the coefficients, we need to first gather all data matrices with the primary process X. The value of the percentile of execution time in each data matrix can be expressed as an equation:

\begin{equation}
X_{percentile1} = x_0 + x_1Y_1 + x_2Z_1 + x_3Y_1^{2} + x_4Z_1^{2} + x_5(Y_1 \times Z_1) \dots
\end{equation}

\smallskip
Where the values of $X_{percentile1}$, $Y_1$ and $Z_1$ are known and the coefficients are unknown. Similarly, we would have equations for $X_{percentile2}$, $X_{percentile3}$, etc. depending on the number of measured combination data matrices. We can transform this set of equations into two vectors and a matrix.

\begin{minipage}[t]{.45\textwidth}
\begin{align}
    x &= \begin{bmatrix}
           x_{0} \\
           x_{1} \\
           \vdots \\
           x_{n}
         \end{bmatrix}
\end{align}
\end{minipage}
\begin{minipage}[t]{.45\textwidth}
\begin{align}
    A &= \begin{bmatrix}
    Y_{1}       & Z_{1} & Y_1^{2} & \dots \\
    Y_{2}       & Z_{2} & Y_2^{2} & \dots \\
    \hdotsfor{4} \\
    Y_{m}       & Z_{m} & Y_m^{2} & \dots
\end{bmatrix}
\end{align}
\end{minipage}

\begin{align}
    b &= \begin{bmatrix}
           X_{percentile1} & X_{percentile2} & \dots &  X_{percentile(m)}
         \end{bmatrix}
\end{align}

\smallskip
Where the matrix A has dimensions $m \times n$. The vector $b$ holds the values from the right side of the equations -- percentiles of execution time of the process X when collocated with various combinations of processes. The matrix $A$ holds the values from the right side of the equations (the polynomial values of the numbers of secondary processes) and the vector $x$ holds values of the coefficients. Our goal is to find:

\begin{equation}
argmin_x ||Ax - b||_2
\end{equation}

\smallskip
Where $||_2$ is the euclidean norm. In other words, we want to find vector $x$, which after multiplying the matrix A, will produce a vector of execution times, which has the smallest distance from the original vector of execution times $b$.

\smallskip
To calculate the coefficients, we chose the method of non-negative least squares (NNLS). Standard least squares minimizes the sum of squares of the residuals (the difference between measured and calculated percentile of execution time) for each equation. The problem is that standard least squares can yield negative coefficients, and consequently, such coefficients could predict a negative slowdown in percentile of execution time. Because negative slowdown does not make sense in our case, we opted for NNLS, which is a constrained version of least squares, where negative coefficients are forbidden.

\smallskip
For the calculation of NNLS, we used the solution provided by sklearn\cite{NNLS}. After having the coefficients (the regression model) calculated for a given process, the prediction of a given combination of processes is formed simply by assigning the numbers of the secondary processes into the model.

\smallskip
The polynomial regression prediction can, in theory, predict any combination of processes. However, we consider the prediction to be unsucessful if the result is extremely high -- in our case, higher than twenty times the percentile of the execution time of the process running alone.

\section{Ensuring operational boundary}
\label{BoundaryImpl}
\smallskip
As explained in Section \ref{Boundary}, in our solution, we want to detect the situation in which a combination of processes exceeds a user-specified computational capacity of a computer (e.g., 200\% of the capacity).

\smallskip
In order to do so, we first define what is the maximal value of a property: \textit{Maximal value of a measured property on a given computer is a value, which is impossible to exceed cumulatively on the computer with any number of running processes.} To use the example of a number of written bytes per second on disk, the maximal value of this property could be 100 MB per second due to the hardware performance.

\smallskip
It is important to note that processes could have upper limits on the usage of various resources -- for example, a process could be forbidden to allocate more than 2 GB of memory by the OS. The maximal value of a property is not the highest value, which a process can reach, but the highest value, which a computer can reach.

\smallskip
These maximal values then represent the 100\% capacity of the computer in regards to the measured properties. We determine if a combination of processes exceeds a specified percentage of the capacity of the computer as follows:

\begin{enumerate}[noitemsep]
\item For each property:
\begin{enumerate}[noitemsep]
\item Sum the average value of that property from all processes in the combination
\item If the sum is lower than the specified percentage of maximal value of the property, this property does not exceed the boundary
\end{enumerate}
\item The combination of processes does not exceed the boundary if each property lies below the boundary
\end{enumerate}

\smallskip
As a result, in order to establish the operational boundaries, we need to approximate the maximal values of properties in the data matrix. It is important to note that these maximal values vary between types of nodes due to the difference in measured properties, as explained in Section \ref{Knowledge}.

\subsection{Approximating maximal values of properties}
\smallskip
There are multiple possible approaches to approximate the maximal values, and they can be split into two categories -- approaches, which use the information about the hardware and software (configuration) of the computer, and those, which do not use this information. In our solution, we use the second category.

\smallskip
Let us first explain why we did not choose to primarily use the information about configuration of the computer. Imagine we knew details about the CPU, such as clock rate, architecture, etc. We could, in theory, estimate the maximal number of instructions, which a multi-threaded process can use in a unit of time. However, such an estimate would be unrealistic, as the performance of a computer depends on the combination of all hardware and software components. To estimate all properties, which indicate a resource usage, would require a very complex model and understanding of the computer. This would soon become unrealistic with large numbers of properties and unexpected components on new node types.

\smallskip
The approach we chose uses only information about the processes, which means data matrices with various measured properties. We hypothesize that we can approximate the maximal values of these properties through measurement of the performance of extreme processes (benchmarks) on the computer. As a backup option, the user can also supply the predictor directly with the information about maximal values of various properties to deal with limitations on resource usage by the OS and other problems with measurement. The primary way of approximating the maximal value of these properties should, however, be automatic.

\smallskip
The predictor comes with a set of benchmarks. When a user wants to predict on a new type of node, the predictor requests the measurements of provided benchmarks on this node. These benchmarks aim to use all of the capacity of various resources on the computer. As a result, we are provided with data matrices, which have various properties measured, hopefully, close to their maximal values.

\smallskip
Additionally, if a process supplied by the user exceeds the benchmarks in some property, the maximal value of this property will be estimated by this process instead of the benchmarks. Thus even if the benchmarks are not designed ideally, we can expect that the processes running in the edge-cloud system will eventually supply us with information about the maximal values of various properties. The probability that one process from the large numbers of processes running in the edge-cloud system will come close to the maximal value is expected to be reasonably high. On top of that, if the supplied benchmarks do not focus on some kind of property (e.g., usage of graphic cards), the user processes will, which makes this approach future-proof.

\smallskip
The description of the benchmarks which we used during the evaluation is provided in Section \ref{ULS}.

\subsubsection{Excluding properties}
\smallskip
Some properties can be excluding in the sense, that they exploit the same resource. For example, if we had separate properties for reads and writes on the disk, these two properties would exclude each other -- two processes, one reading and the other writing 60 MB per second to/from disk would exceed the capacity of 100 MB per second disk.

\smallskip
To reflect this behaviour, it is needed to introduce new property into the data matrix aggregating excluding properties into one, which is the responsibility of the provider of the data.

\subsubsection{User provided approximation}
\smallskip
The usage of some properties can be restricted by the OS, such as the maximal number of allocated bytes by a process. As a result, the approximation of this property using benchmarks would become impossible. For example, a process could maximally allocate 4 GB of data, but the capacity of the computer is 32 GB.

\smallskip
As a result, it is permitted that the user can provide their approximation of maximal values of properties. The user can also specify which properties should be used to detect the boundary. For example, if one of the measured properties was non-limiting, the limit of such property can be set to infinite.