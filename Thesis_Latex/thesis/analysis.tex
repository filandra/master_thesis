%Done
\chapter{Situation analysis}
\label{Analysis}
\smallskip
In this chapter, we first propose an edge-cloud layer to provide soft guarantees on the RTT. Then we examine the way we expect the processes to run in the edge-cloud nodes. In the end, we focus on the form and measurement of the provided data, which influences the solution to the problem.

\section{Proposed layer}
\label{WhyPredictor}
As highlighted in Chapter \ref{Introduction} and further explained in Section \ref{Motivation}, the focus of our thesis is to provide soft-guarantee on the RTT for applications in the edge-cloud system.

\smallskip
Two factors are contributing to RTT in an edge-cloud system -- the distance between the user and the MDC and the amount of slowdown caused by interference between processes. In order to minimize the latency caused by distance, edge-cloud MDCs are moved closer to the user. As a result, only the minimization of process interference needs to be solved.

\smallskip
It is expected that the developers use standard high-level programming languages (such as Java or Python) and that the cloud provider uses standard cloud technologies (such as Docker and Kubernetes). Therefore the minimization of interference between processes is achieved by deployment of suitable combinations of processes on individual servers, not by the use of low-level real-time programming languages or real-time programming models.

\smallskip
As a result, the cloud provider faces a dilemma: how to deploy processes to the servers available in a given MDC in such a way that would achieve:
\begin{itemize}[noitemsep]
\item Minimal interference between the collocated processes 
\item Satisfaction of the soft real-time requirements
\item Minimal number of servers used (maximize the resource utilization of servers)
\end{itemize}

\smallskip
To solve this problem, we propose a new layer for the edge-cloud composed of a predictor of the execution time of processes and a solver, as visualized in Figure \ref{Arch}. The edge-cloud architecture would then consist of:
\begin{itemize}[noitemsep]
\item Central datacenter -- decides which processes should run in which MDC
\item Predictor and solver -- accept deployment request of a set of processes to an MDC and finds suitable combinations of processes to collocate on the individual servers
\item MDCs -- run combinations of processes on its servers
\end{itemize}

\begin{figure}	
  \centering
  \includegraphics[width=0.75\textwidth]{../img/architecture.png}
  \caption{Ilustration of the edge-cloud architecture with the proposed layer.}
  \label{Arch}
\end{figure}

\subsection{Finding optimal deployment using predictor}
\smallskip
The problem of deployment and redeployment can be solved relatively easily using a predictor of execution time. The solver iterates through a set of process combinations and requests a prediction for each combination. The predictor issues a prediction stating the predicted performance and whether the combination satisfies the soft real-time requirements or not. The solver can then select a deployment with the fewest used servers or with combinations of processes with the best performance.

\smallskip
A trivial solver would iterate through all combinations of processes and use the predictor to decide if a given combination satisfies the soft real-time requirements. A non-trivial solver design could be, for example, in the form of formulating and solving the optimal deployment as a constraint satisfaction problem. 

\smallskip
However, a more complex solver can only decrease the number of combinations to be predicted (thus lower the time needed to find the deployment). To provide the soft real-time guarantees, the quality of the predictor is the decisive factor. Thus our thesis focuses primarily on the predictor and not on the solver.

\smallskip
To estimate if a set of collocated processes satisfy the soft real-time requirements on the RTT, we need to predict the percentile of the execution time of each of the processes. The reason being, that if a process is required to have, for example, execution time shorter than 30 ms in 90\% of calculations, we can express the same requirement as having the 90th percentile of execution time shorter than 30 ms.

\smallskip
As a result, the construction of a sufficiently accurate predictor of the percentile of the execution time is needed to satisfy the goals of this thesis. Section \ref{Knowledge} describes the data provided to the predictor, the design of the predictor is described in Chapter \ref{Approach}, and the implementation details are explained in Chapter \ref{Implementation}.

\section{Process and node behavior}
\label{Behaviour}
\smallskip
The expected behavior of processes can be summarised as follows:
\begin{itemize}[noitemsep]
\item The processes run in isolated containers
\item The containers can be moved between nodes in the same MDC
\item The processes receive requests from client applications on the user's devices or from other processes
\item The processes can communicate with other processes running in the same MDC (processes can depend on other processes)
\item The processes can not communicate with processes running in other MDCs
\end{itemize}

\smallskip
Even though the processes are isolated in containers, we expect them to share the CPU. Thus instead of each process acquiring one CPU core for itself, the computational load is distributed among all cores equally. The same principle also applies to memory, disk and other shared resources. Even if the node had multiple disks or memory cards, we still regard them as one resource. This is important as we do not have to deal with the issue of assigning applications on different CPU cores, disks or memory cards.

\subsubsection{Rate of requests}
\label{Rate}
\smallskip
The expected rate of requests is 100\% -- when a process finishes the current calculation, it starts a new calculation immediately. The reason is, that when using the edge-cloud system, a new clone of a process would be spawned only after the previous one can not handle the request rate anymore. Thus at any time, there would be maximally one clone of a process without a 100\% request rate. It is preferable to still treat it as a 100\% request rate process during the prediction as the request rate can climb unexpectedly.

\subsubsection{Process dependency}
\smallskip
The processes running in a given MDC can depend on other processes running in the same MDC, for example, a face recognition application (depender) can use a database (dependee). In this situation, we expect the dependee process to run with an acceptable slowdown. If this condition cannot be guaranteed, then both the depender and the dependee can not be assigned as the performance of the depender can be unpredictably affected.

We regard the communication latency between processes in the same MDC to be trivial. On the other hand, we expect that processes do not communicate across MDCs, as the communication latency would be automatically too high (if the communication latency would also be trivial, these two MDCs could be merged into one).

\subsubsection{Distance latency}
\smallskip
The communication latency caused by the distance between the user and the MDC does not necessarily have to be trivial. The information about the expected latency (probably the highest possible in regards to the served area by given MDC) can be provided and accounted for in order to provide the soft guarantee on RTT.

\section{Provided knowledge}
\label{Knowledge}
\smallskip
Data provided to implement the predictor is in the form of a data matrix describing the behavior of a process when running alone or alongside a combination of other processes. In this matrix, columns describe various properties (such as execution time, number of instructions executed, number of written blocks on disk, etc.) and rows represent runs of the application (a run represents one calculation of a request). This data matrix allows us to examine the behavior over multiple runs, which can be used to infer statistical information and also reveals low probability behavior, such as the writing of disk buffers. An example is provided in Table \ref{tab:Example}.

\begin{table}[!htbp]
\centering
\large
\resizebox{\columnwidth}{!}{%
\begin{tabular}{|l|l|l|l|l|l|l|l|}
\hline
\rowcolor[HTML]{FFFFFF} 
\textbf{run} & \textbf{elapsed} & \textbf{instructions} & \textbf{cache-misses} & \textbf{read\_sectors} & \textbf{written\_sectors} & \textbf{io\_time} \\ \hline
\rowcolor[HTML]{FFFFFF} 
1 & 5506 & 42787936272 & 58142200 & 0 & 8976 & 170 \\ \hline
\rowcolor[HTML]{FFFFFF} 
2 & 5467 & 42788015318 & 58033451 & 0 & 7432 & 158 \\ \hline
\rowcolor[HTML]{FFFFFF} 
3 & 5496 & 42787914647 & 58113190 & 0 & 6608 & 94 \\ \hline
\rowcolor[HTML]{FFFFFF} 
4 & 5545 & 42787790092 & 58147487 & 0 & 4688 & 66 \\ \hline
\rowcolor[HTML]{FFFFFF} 
5 & 5470 & 42787883516 & 58149216 & 0 & 5680 & 114 \\ \hline
\rowcolor[HTML]{FFFFFF} 
6 & 5515 & 42787801186 & 58045890 & 0 & 6592 & 125 \\ \hline
\end{tabular}
}
\caption{An example of possible data matrix of a process.}
\label{tab:Example}
\end{table}

\subsection{Data matrix naming convention}
\smallskip
Let us consider we had processes A, B and C at our disposal. The data matrix of a process allocated alone is called \textit{single data matrix} and is denoted only by the ID of the process (e.g., data matrix A). The data matrix of a combination of processes is called \textit{combination data matrix} and is denoted by the IDs of processes separated by a dash.
 
\smallskip
For example, B-A-C means a measurement of a situation, in which three processes were collocated on a given node. In the combination, we distinguish the primary process and secondary processes. The primary process is the first ID in the combination (B), the secondary processes are the rest (A and C). The primary process is the process, for which we measure the data matrix and secondary processes are influencing the primary process. Thus the result of measuring the B-A-C combination is a data matrix with the information about process B (execution time, written sectors, etc.), which is influenced by the secondary processes A and C. For example, the execution time could be a second longer when compared to the measurement of single B.

\smallskip
It is important to note that some of the properties (e.g., number of written bytes on disk) can be cumulatively influenced by the secondary processes. Imagine that process A writes 1000 bytes per 3.5 seconds on disk. Thus even thought process B does not write anything on disk, the B-A-C data matrix will have the property influenced. This is unfortunately how the data is provided to us, as apparently, this behavior is unavoidable for some properties. This concept is illustrated in Figure \ref{Combination}.

\begin{figure}	
  \centering
  \includegraphics[width=0.5\textwidth]{../img/comb.png}
  \caption{Ilustration of cumulative influence on a property.}
  \label{Combination}
\end{figure}

\subsection{Lack of node or process-specific information}
\smallskip
At first glance, it seems like we do not get any information about the nodes on which the processes run, however, that is not entirely true. The data matrix of a given process is measured on a particular type of node (hardware and software configuration) and is only used for prediction on the same type of node. This way, the properties of the node are reflected on the data matrices about the process (e.g., in the length of execution time).

\smallskip
We are also not provided with any specific data about the process, such as if the process uses a particular library, uses sequence write on disk, etc. The reason is that such information provided by the developer is unreliable.

\smallskip
The lack of more specific information about the node or the processes is on purpose. The predictor needs to function on multiple types of nodes and it is undesirable to structure the predictor around the knowledge of, for example, particular pieces of hardware.

\smallskip
It is important to note that the data matrix of a process can vary from node to node. The reason is, that some hardware and OS configurations do not allow the measurement of certain properties (e.g., the information about CPU cache), or provide the measurement of additional properties.

\smallskip
The lack of specific information about the nodes and processes and the varying data matrix of a process on different nodes can enhance the difficulty of designing the predictor greatly. However, when the predictor is constructed with these limitations, it becomes much more flexible and also future-proof. If, for example, new measurable properties became available on a new server architecture running a new type of application, the predictor should be automatically able to take advantage of such a data matrix and provide prediction, even if this situation is entirely unaccounted for.

\subsection{How is the data matrix measured}
\smallskip
Measured processes are run in containers with a 100\% request rate (their calculation is repeated after each completion), which simulates how would these processes run in the edge-cloud node. For each run of the primary process, properties are measured into a row containing various information about the use of CPU, memory, disk, etc. One difference is that after a certain number of runs, the computer is restarted in order to minimize the effects of different computer initializations (e.g., loading of libraries).

\smallskip
The measurements of a data matrix last for several hours (in our case between one and two hours). The length of measurement must be sufficiently long in order to acquire a statistically significant number of runs. We expect that dependencies are run alone on separate computers in the same MDC during the measurement in order to guarantee acceptable slowdown.

\subsubsection{Number of measurements}
\label{Number}
\smallskip
Due to the number of possible combinations of processes and length of the measurement, it is expected that only a small percentage of all combinations will be measured for a given edge-cloud system. The number of data matrices in a given arity of processes (combination of five processe has an arity of five) is calculated as:

\begin{equation}
DM_a = \binom{(n+a-2)!}{a-1} \times n
\end{equation}

\smallskip
Where $DM_a$ is the number of data matrices in a given arity $a$ and $n$ is the number of available processes. The number is calculated as the combination with repetition of secondary processes, times the number of possible primary processes. This is due to the fact, that for each combination of secondary processes we can choose one primary process.

\smallskip
Imagine we had 17 different processes and we could collocate up to 5 processes. The number of quintuples alone already amounts to $(\binom{20}{4} \times 17) = 82 365$. If a single measurement lasts around 1 hour (which is our case), we would need at least 82 365 hours (3 431 days) of machine time to measure them all.

\smallskip
This puts another limitation on the design of the predictor, as we can expect only a small percentage of all combinations to be measured.

\section{Overview of the whole process}
\smallskip
The process of providing a soft real-time guarantee (as illustrated in Figure \ref{ProcessOverview}) starts with a developer submitting their process to the cloud provider for evaluation. The cloud provider then measures necessary data matrices and provides them to the predictor. After that, the developer can request the deployment of their process to the edge-cloud. The cloud provider then uses the deployment solver to find a suitable deployment of all processes on the MDC. The deployment solver repeatedly uses the predictor to estimate the percentile of the execution time of collocated processes in order to find a deployment, which satisfies the soft real-time requirements of developers. Finally, the cloud provider deploys the processes according to the suitable deployment plan.

\smallskip
It is important to note that the predictor is expected to be periodically updated with new data matrices in order to improve the precision of predictions. The cloud provider should also monitor the real performance of deployed processes in order to confirm the predicted execution times or request a redeployment.

\begin{figure}	
  \centering
  \includegraphics[width=\textwidth]{../img/overview.png}
  \caption{Overview of providing soft real-time guaranties on process execution time.}
  \label{ProcessOverview}
\end{figure}